﻿using System;
using System.Text;
using I2.Loc;
using RTLTMPro;
using TMPro;
using UnityEngine;

public class PauseDialogAnimationHelper : MonoBehaviour
{
    public GameObject darkPanel;
    
    public RTLTextMeshPro englishScore;
    public RTLTextMeshPro farsiScore;

    private void OnEnable()
    {
        StringBuilder stringBuilder = new StringBuilder();
        if (LocalizationManager.CurrentLanguage == LocalizationManager.GetAllLanguages()[0])
        {
            englishScore.gameObject.SetActive(true);
            farsiScore.gameObject.SetActive(false);

            stringBuilder.Append("Score ").Append(ArcadeBlockManager.Instance.GameLevel.ToString());
            englishScore.text = stringBuilder.ToString();
        }
        else
        {
            englishScore.gameObject.SetActive(false);
            farsiScore.gameObject.SetActive(true);
            
            stringBuilder.Append("امتیاز ").Append(ArcadeBlockManager.Instance.GameLevel.ToString());
            farsiScore.text = stringBuilder.ToString();
        }
    }

    public void Disable()
    {
        gameObject.SetActive(false);
        darkPanel.SetActive(false);
    }
}
