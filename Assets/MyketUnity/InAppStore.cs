﻿using System.Collections.Generic;
using System;
using System.Text;
using _02_Scripts.Helper_Classes.Models;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(StoreHandler))]
public class InAppStore : MonoBehaviour, IBillingListener
{
	public Product[] skus;
	public BazaarProducts bazaarProducts = BazaarProducts.GetDefaults();

	private StoreHandler _storeHandler;

	public static InAppStore GetInstance;
	void Awake ()
	{
		if (GetInstance == null)
		{
			_storeHandler = GetComponent<StoreHandler>();
			_storeHandler.SetUpBillingService (this);
			
			GetInstance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	public void OnBillingServiceSetupFinished ()
	{
		_storeHandler.UpdatePurchasesAndDetails (skus);
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.AppendLine("InAppStore.OnBillingServiceSetupFinished() -> skus.count = " + skus.Length);
		foreach (Product sku in skus)
		{
			stringBuilder.AppendLine(sku.productId);
		}

		stringBuilder.AppendLine("---....---....---...--------------------...../////-");
		print(stringBuilder.ToString());
	}
	
	public void OnPurchasesUpdated (List<Purchase> purchases)
	{

	}

	public void OnPurchasesAndDetailsUpdated (List<Purchase> purchases, List<ProductDetail> skus)
	{
		print("InAppStore.SetProducts(List<BazaarSkuInfo> skus) -> skus.count = " + skus.Count);

/*		bazaarProducts.productOne = new BazaarProduct(skus[0].productId,
			skus[0].title, skus[0].price, skus[0].type, skus[0].description);
        
		bazaarProducts.productTwo = new BazaarProduct(
			skus[1].productId,
			skus[1].title,
			skus[1].price,
			skus[1].type, 
			skus[1].description);

		bazaarProducts.productThree = new BazaarProduct(
			skus[2].productId,
			skus[2].title,
			skus[2].price,
			skus[2].type, 
			skus[2].description);

		bazaarProducts.productFour = new BazaarProduct(
			skus[3].productId,
			skus[3].title,
			skus[3].price,
			skus[3].type, 
			skus[3].description);

		bazaarProducts.productFive = new BazaarProduct(
			skus[4].productId,
			skus[4].title,
			skus[4].price,
			skus[4].type, 
			skus[4].description);

		bazaarProducts.productSix = new BazaarProduct(
			skus[5].productId,
			skus[5].title,
			skus[5].price,
			skus[5].type, 
			skus[5].description);*/

		SetProducts();

		print("InAppStore.SetProducts(List<BazaarSkuInfo> skus) -> bazaarProducts.ToString() = " + bazaarProducts.ToString());
        
		PlayerPrefs.SetString("Bazaar Products",bazaarProducts.ToString());
		PlayerPrefs.Save();
	}

	public void OnUserCancelPurchase (string errorMessage)
	{

	}

	public void OnPurchaseFinished (Purchase purchase)
	{
		print("InAppStore.OnPurchaseFinished (Purchase purchase) -> " + purchase.productId);
		
		BazaarProduct purchasedProduct = new BazaarProduct();
		if (bazaarProducts.productOne.ProductId.Equals(purchase.productId))
		{
			purchasedProduct = bazaarProducts.productOne;
		} else if (bazaarProducts.productTwo.ProductId.Equals(purchase.productId))
		{
			purchasedProduct = bazaarProducts.productTwo;
		} else if (bazaarProducts.productThree.ProductId.Equals(purchase.productId))
		{
			purchasedProduct = bazaarProducts.productThree;
		} else if (bazaarProducts.productFour.ProductId.Equals(purchase.productId))
		{ 
			purchasedProduct = bazaarProducts.productFour;
		} else if (bazaarProducts.productFive.ProductId.Equals(purchase.productId))
		{ 
			purchasedProduct = bazaarProducts.productFive;
		} else if (bazaarProducts.productSix.ProductId.Equals(purchase.productId))
		{
			purchasedProduct = bazaarProducts.productSix;
		}
        
		// in product filed we put coin number that we should give
		int coinNumber = Int32.Parse(purchasedProduct.Description);
		CoinManager.GetInstance.IncreaseCoins(coinNumber);
		
		_purchaseDone?.Invoke();

		_storeHandler.ConsumePurchase(purchase.productId, purchase.purchaseToken);
		
		Debug.Log("BazaarIABManager.PurchaseDone -> Error: user bought a product that has no corresponding in app");
	}

	public void OnConsumeFinished (String productId, String purchaseToken)
	{
		print("InAppStore.OnConsumeFinished (String productId, String purchaseToken) -> productId = " + productId);
	}

	public void OnError (Int32 errorCode, String errorMessage)
	{
		print("InAppStore.Error -> " + errorMessage);
		
		switch (errorCode) {
		case 1: // error illegal State
			break;

		case 2: // error billing setup fail
			break;

		case 3: // error empty public key
			break;

		case 4: // error update purchases
/*****************************************************/
			SetProducts();
/**********************************************************/
			break;
		case 5: // error consume

			break;
		case 6: // error start activity

			break;
		case 7: // error purchase

			break;
		}

	}
	
	private Action _purchaseDone;
	public void purchaseProduct (int productIndex, Action onSuccess)
	{
		_purchaseDone = onSuccess;
		Product product = skus [productIndex];
		if (product.type == Product.ProductType.Subscription) {
			GetComponent<StoreHandler> ().BuyProduct (product.productId, "subs");
		} else if (product.type == Product.ProductType.InApp) {
			GetComponent<StoreHandler> ().BuyProduct (product.productId, "inapp");
		}
	}

	public void SetProducts()
	{
		PlayerPrefs.SetString("Bazaar Products", bazaarProducts.ToString());
		PlayerPrefs.Save();
		/*string temp = PlayerPrefs.GetString("Bazaar Products");
        
		if(!temp.Equals(""))
			bazaarProducts = JsonUtility.FromJson<BazaarProducts>(temp);
        
		print("InAppStore.SetProducts() -> PlayerPrefs.GetString(Bazaar Products) = " + temp);
		print("InAppStore.SetProducts() -> PbazaarProducts = " + bazaarProducts);*/
	}
}
