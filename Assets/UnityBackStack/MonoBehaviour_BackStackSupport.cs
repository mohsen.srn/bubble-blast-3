﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityBackStack;
using UnityEngine;

namespace UnityBackStack
{
    [RequireComponent(typeof(ComponentBackHelper))]
    public abstract class MonoBehaviour_BackStackSupport : MonoBehaviour
    {
        public abstract void BackCalled();
    }
}
