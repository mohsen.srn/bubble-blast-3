using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityBackStack
{
    public class BackStackManager : MonoBehaviour
    {
        /******************************* Singleton ***************/
        public static BackStackManager GetInstance;
        private void Awake()
        {
            if (GetInstance == null)
            {
                GetInstance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                // back to home
                ResetBackStack();
                Destroy(gameObject);
            }
        }
    /**************************************************************/
        private readonly Stack<MonoBehaviour_BackStackSupport> _stack = new Stack<MonoBehaviour_BackStackSupport>();

        public void Push2BackStack(MonoBehaviour_BackStackSupport goComponent)
        {
            _stack.Push(goComponent);
        }

        public void RemoveFromBackStack()
        {
            if(_stack.Count > 0)
                _stack.Pop().BackCalled();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                RemoveFromBackStack();
            }
        }

        public void ResetBackStack()
        {
            _stack.Clear();
        }
    }
}
