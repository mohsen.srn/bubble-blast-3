using System;
using System.Collections;
using UnityEngine;

namespace UnityBackStack
{
    [RequireComponent(typeof(MonoBehaviour_BackStackSupport))]
    public class ComponentBackHelper : MonoBehaviour
    {
        private void OnEnable()
        {
            StartCoroutine(PushToStackWithDelay());
        }

        IEnumerator PushToStackWithDelay()
        {
            yield return new WaitForSeconds(0.2f);
            BackStackManager.GetInstance.Push2BackStack(GetComponent<MonoBehaviour_BackStackSupport>());
        }
    }
}
