%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Crab Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
  - m_Path: Body/Right eye stand
    m_Weight: 1
  - m_Path: Body/Right eye stand/Eye open
    m_Weight: 1
  - m_Path: Body/Right eye stand/Eye open/Eye ball
    m_Weight: 1
  - m_Path: Body/Right eye stand/Eye open/eye open front
    m_Weight: 1
  - m_Path: Body/Right eye stand/Eye open/Moje right
    m_Weight: 1
  - m_Path: Body/Left eye stand
    m_Weight: 1
  - m_Path: Body/Left eye stand/Eye open
    m_Weight: 1
  - m_Path: Body/Left eye stand/Eye open/Eye ball
    m_Weight: 1
  - m_Path: Body/Left eye stand/Eye open/eye open front
    m_Weight: 1
  - m_Path: Body/Left eye stand/Eye open/Moje left
    m_Weight: 1
  - m_Path: Body/Right hand
    m_Weight: 1
  - m_Path: Body/Right hand/Crab_Chang right big
    m_Weight: 1
  - m_Path: Body/Right hand/Crab_Chang right small
    m_Weight: 1
  - m_Path: Body/Left hand
    m_Weight: 1
  - m_Path: Body/Left hand/Crab_Chang left big
    m_Weight: 1
  - m_Path: Body/Left hand/Crab_Chang left small
    m_Weight: 1
  - m_Path: Body/Mouth
    m_Weight: 1
  - m_Path: Rigging
    m_Weight: 1
  - m_Path: Rigging/Waist
    m_Weight: 1
  - m_Path: Rigging/Waist/Body
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1/mouth bone 2
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1/mouth bone 2/mouth bone 3
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1 (1)
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1 (1)/mouth bone 2
    m_Weight: 1
  - m_Path: Rigging/Waist/Body/mouth/mouth bone 1 (1)/mouth bone 2/mouth bone 3
    m_Weight: 1
  - m_Path: Rigging/Waist/right eye stand lower
    m_Weight: 1
  - m_Path: Rigging/Waist/right eye stand lower/right eye stand upper
    m_Weight: 1
  - m_Path: Rigging/Waist/right eye stand lower/right eye stand upper/right eye
    m_Weight: 1
  - m_Path: Rigging/Waist/right eye stand lower/right eye stand upper/eye ball
    m_Weight: 1
  - m_Path: Rigging/Waist/left eye stand lower
    m_Weight: 1
  - m_Path: Rigging/Waist/left eye stand lower/left eye stand upper
    m_Weight: 1
  - m_Path: Rigging/Waist/left eye stand lower/left eye stand upper/left eye
    m_Weight: 1
  - m_Path: Rigging/Waist/left eye stand lower/left eye stand upper/eye ball
    m_Weight: 1
  - m_Path: Rigging/Waist/right hand upper
    m_Weight: 1
  - m_Path: Rigging/Waist/right hand upper/right hand lower
    m_Weight: 1
  - m_Path: Rigging/Waist/right hand upper/right hand lower/right grip big
    m_Weight: 1
  - m_Path: Rigging/Waist/right hand upper/right hand lower/right grip big/right grip
      small
    m_Weight: 1
  - m_Path: Rigging/Waist/left hand upper
    m_Weight: 1
  - m_Path: Rigging/Waist/left hand upper/right hand lower
    m_Weight: 1
  - m_Path: Rigging/Waist/left hand upper/right hand lower/right grip big
    m_Weight: 1
  - m_Path: Rigging/Waist/left hand upper/right hand lower/right grip big/right grip
      small
    m_Weight: 1
  - m_Path: IKs
    m_Weight: 1
  - m_Path: IKs/Body Ik CCD
    m_Weight: 1
  - m_Path: IKs/eyes
    m_Weight: 1
  - m_Path: IKs/eyes/right eye Ik CCD
    m_Weight: 1
  - m_Path: IKs/eyes/left eye Ik CCD
    m_Weight: 1
  - m_Path: IKs/eye balls
    m_Weight: 1
  - m_Path: IKs/eye balls/eye ball left Ik CCD
    m_Weight: 1
  - m_Path: IKs/eye balls/eye right Ik CCD
    m_Weight: 1
  - m_Path: IKs/right hand Ik CCD
    m_Weight: 1
  - m_Path: IKs/left hand Ik CCD
    m_Weight: 1
  - m_Path: IKs/mouth right Ik CCD
    m_Weight: 1
  - m_Path: IKs/mouth left Ik CCD
    m_Weight: 1
