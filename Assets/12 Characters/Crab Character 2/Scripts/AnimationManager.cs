﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class AnimationManager : MonoBehaviour
{
    public List<string> animTrigers;
    public int Min, Max;
    private Animator _animator;
    
    void Start()
    {
        _animator = GetComponent<Animator>();
        StartCoroutine(ShuffleAnimation());
    }

    IEnumerator ShuffleAnimation()
    {
        while(true)
        {
            float randSec = Random.Range(Min, Max);
            
            yield return new WaitForSeconds(randSec);

            _animator.SetTrigger(animTrigers[Random.Range(0, animTrigers.Count)]);
        }
    }

    public bool _flag = false;
    private void OnMouseDown()
    {
        /*float ranNumber = Random.Range(0, 10);
        if (ranNumber < 5)
        {
            _animator.SetTrigger("scratching stomach");
        }
        else
        {
            _animator.SetTrigger("bored");
        }*/

        _animator.SetTrigger(_flag ? "scratching stomach" : "bored");

        _flag = !_flag;
    }
}
