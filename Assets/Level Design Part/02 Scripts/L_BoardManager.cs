﻿using System.Collections.Generic;
using _02_Scripts;
using _02_Scripts.ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class L_BoardManager : MonoBehaviour
{
    public GameObject Row;
    public string HomeSceneName;
    public GameStages _GameStages;
    public List<L_Row> rows;
    public Text RowNumberTxt;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Insert))
        {
            NextRow();
        }
    }

    public void NextRow()
    {
        foreach (var _row in rows)
        {
            var localPosition = _row.gameObject.transform.localPosition;
            localPosition = new Vector2(localPosition.x, localPosition.y - 1);
            _row.gameObject.transform.localPosition = localPosition;
        }
    
        GameObject row = Instantiate(Row, transform);
        
        // we add rows to board list so that we can keep state  
        rows.Add(row.GetComponent<L_Row>());

        RowNumberTxt.text = rows.Count + "";
    }

    public void GoToHomeScene()
    {
        SceneManager.LoadScene(HomeSceneName);
    }

    // this method is for edit stage. i do not use this for now
    public void LoadLastStage()
    {
        int ballNumbers =_GameStages.stages[0].initBalls;
        for (int i = _GameStages.stages[0].stageBoard.Count - 1; i >= 0; i--)
        {
            foreach (var _row in rows)
            {
                var localPosition = _row.gameObject.transform.localPosition;
                localPosition = new Vector2(localPosition.x, localPosition.y - 1);
                _row.gameObject.transform.localPosition = localPosition;
            }
            var row = Instantiate(Row, transform);
            // we add rows to board list so that we can keep state  
            int j = -1;
            foreach (var obstacle in _GameStages.stages[0].stageBoard[i].stageObstacles)
            {
                j++;
                // ReSharper disable once IdentifierTypo
                switch (obstacle.type)
                {
                    case StageObstacle.ObstacleType.p:
                        row.GetComponent<L_Row>().ItemHolders[j].MakeBlock(ObstacleTypeV2.p, obstacle.resistance);
                        break;
                    case StageObstacle.ObstacleType.O:
                        row.GetComponent<L_Row>().ItemHolders[j].MakeBlock(ObstacleTypeV2.O, obstacle.resistance);
                        break;
                    case StageObstacle.ObstacleType.LJ:
                        row.GetComponent<L_Row>().ItemHolders[j].MakeBlock(ObstacleTypeV2.LJ, obstacle.resistance);
                        break;
                    case StageObstacle.ObstacleType.BRK:
                        row.GetComponent<L_Row>().ItemHolders[j].MakeBlock(ObstacleTypeV2.BRK, obstacle.resistance);
                        break;
                    // ReSharper disable once RedundantCaseLabel
                    case StageObstacle.ObstacleType.__:
                    default: // i put default here to ensure all of obstcl will initials
                        row.GetComponent<L_Row>().ItemHolders[j].MakeBlock(ObstacleTypeV2.__, obstacle.resistance);
                        break;
                }
            }
            rows.Add(row.GetComponent<L_Row>());
        }
    }
}
