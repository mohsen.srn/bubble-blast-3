﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using _02_Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class L_StageManager : MonoBehaviour
{
    public List<List<L_Obstacle>> Stage = new List<List<L_Obstacle>>();
    public TMP_InputField InputFieldCollumn;
    public Text boardRowNumbers;
    private int _numberOfRowItems;
    public int NumberOfRowItems // columns
    {
        get => _numberOfRowItems;
        set
        {
            _numberOfRowItems = Int32.Parse(InputFieldCollumn.text);
            SecondCameraManager.Instance.UpdateCameraAndBoard(_numberOfRowItems);
            boardRowNumbers.text = UtilityClass.GetRowNumberFromColumnNumber_Stage(_numberOfRowItems) + "";
        }
    }

    private void Start()
    {
        ItemHolder.OnItemChange += UpdateStageArray;
        _numberOfRowItems = 5;
        InputFieldCollumn.text = _numberOfRowItems + "";
        
//        ItemHolder.OnItemChange += Log;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.A))
        {
            ChangeColumnNumber(+1);
        } else if (Input.GetKeyUp(KeyCode.S))
        {
            ChangeColumnNumber(-1);
        }
    }

    public void UpdateStageArray(int row, int item, L_Obstacle type)
    {
        if (Stage.Count >= row + 1)
        {
            if (Stage[row].Count >= item + 1)
            {
                Stage[row][item] = type;
            }
            else
            {
                Stage[row].Add(type);
            }
        }
        else
        {
            AddNewRow();
            UpdateStageArray(row, item, type);
        }
    }

    public void AddNewRow()
    {
        Stage.Add(AddNewItems());
    }

    private List<L_Obstacle> AddNewItems()
    {
        List<L_Obstacle> items = new List<L_Obstacle>();
        for (int i = 0; i < NumberOfRowItems; i++)
        {
            items.Add(new L_Obstacle());
        }
        return items;
    }

    private void ChangeColumnNumber(int changeAmount)
    {
        _numberOfRowItems += changeAmount;
        InputFieldCollumn.text = _numberOfRowItems + "";
        boardRowNumbers.text = UtilityClass.GetRowNumberFromColumnNumber_Stage(_numberOfRowItems) + "";
    }
    
    public void Log(int a, int b, L_Obstacle t)
    {
        StringBuilder stringBuilder = new StringBuilder();
        foreach (var s in Stage)
        {
            stringBuilder.Append(" / ");
            foreach (var i in s)
            {
                stringBuilder.Append(" " + i.typeV2 + " ");
            }
        }
        print(stringBuilder);
    }
}
