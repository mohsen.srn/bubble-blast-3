﻿using System.Collections.Generic;
using UnityEngine;

public class L_Row : MonoBehaviour
{
    public int Index;
    public GameObject ItemHolder;
    public List<ItemHolder> ItemHolders;
    private L_StageManager _stageManager;
    void Start()
    {
        _stageManager = FindObjectOfType<L_StageManager>();
//        CreateRow();
        CreateRow(_stageManager.NumberOfRowItems);
    }

    private void CreateRow()
    {

    }

    private void CreateRow(int itemCount)
    {
        _stageManager.AddNewRow();
        Index = _stageManager.Stage.Count - 1;
        for (int i = 0; i < itemCount; i++)
        {
            GameObject g = Instantiate(ItemHolder, transform);
            g.transform.localPosition = 
                new Vector3(i - (itemCount - 1)/2.0f ,0f,0f);
            ItemHolder itemHolder = g.GetComponent<ItemHolder>();
            itemHolder.ItemIndex = i;
            itemHolder.lRow = this;
            ItemHolders.Add(itemHolder);
        }
    }
}
