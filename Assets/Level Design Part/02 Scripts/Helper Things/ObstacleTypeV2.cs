namespace _02_Scripts
{
    public enum ObstacleTypeV2
    {
        LJ, BOXBMB, ROWBMB,
        __,
        O,
        p,
        BRK,
        Plus,
        Minus,
        COLBMB,
        LeftTopBrick, RightTopBrick, RightDownBrick, LeftDownBrick,
        LeftTopBlock, RightTopBlock, RightDownBlock, LeftDownBlock,
        Sponge
    }
}
