﻿using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts;
using UnityEngine;

[Serializable]
public class L_Obstacle
{
    public ObstacleTypeV2 typeV2;
    public int Resistance;

    public L_Obstacle()
    {
        typeV2 = ObstacleTypeV2.__;
        Resistance = 0;
    }
}
