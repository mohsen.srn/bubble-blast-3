﻿using System;
using _02_Scripts;
using RTLTMPro;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InitBall : MonoBehaviour
{
    public static int initBall = 1;
    public TextMeshPro initBallText;
    public TMP_InputField InputField;

    private void Start()
    {
        initBallText.text = initBall + "";
    }

    private void OnMouseUpAsButton()
    { 
        if (Item.DraggedTypeV2 == ObstacleTypeV2.Plus)
        {
            int a = Int32.Parse(InputField.text);
            initBall += a;
            initBallText.text = initBall+"";
        }
        else if (Item.DraggedTypeV2 == ObstacleTypeV2.Minus)
        {
            int a = Int32.Parse(InputField.text);
            initBall -= a;
            initBallText.text = initBall + "";
        }

    }
}
