﻿using System;
using UnityEngine;

public class SecondCameraManager : MonoBehaviour
{
    private static SecondCameraManager _instance;
    public static SecondCameraManager Instance => _instance;

    public Camera secondCamera;
    public GameObject boardManager;
    private int columnCount;
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if(_instance != this)
        {
            Destroy(this);
        }
    }

    public void UpdateCameraAndBoard(int columnNumber)
    {
        columnCount = Mathf.Max(columnCount, columnNumber);
        if (secondCamera != null)
        {
//            secondCamera.aspect = 7f / 10;
            secondCamera.orthographicSize = columnCount / secondCamera.aspect / 2;
        }
        boardManager.transform.position = 
            new Vector3(secondCamera.transform.position.x,
                secondCamera.orthographicSize-0.5f);
    }
}
