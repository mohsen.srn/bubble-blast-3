﻿using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts;
using TMPro;
using UnityEngine;

public class ItemHolder : MonoBehaviour
{
    public Sprite Bubble, BoxBomb, RowBomb, Adder, Pit, Brick, Empty, ColBomb, HalfBrick, HalfBlock, Sponge;
    private SpriteRenderer _spriteRenderer;
    public ObstacleTypeV2 typeV2;
    private TMP_InputField _inputField;

    private int _resistance;
    private L_StageManager _stageManager;

    public int Resistance
    {
        get => _resistance;
        set
        {
            _resistance = value;
            if (_resistance > 0)
            {
                ResisText.text = _resistance + "";
                if (_resistance == 1)
                {
                    ResisText.text = "";
                }
            }
            else
            {
                _resistance = 0;
                ResisText.text = "";
            }
        }
    }

    public TextMeshPro ResisText;
    public int ItemIndex;
    public L_Row lRow;

    public delegate void ItemActions(int rowIndex, int itemIndex, L_Obstacle type);

    public static event ItemActions OnItemChange;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _inputField = FindObjectOfType<TMP_InputField>();
        _stageManager = FindObjectOfType<L_StageManager>();
    }

    private void OnMouseUpAsButton()
    {
        if (Item.DraggedTypeV2 != ObstacleTypeV2.Plus && Item.DraggedTypeV2 != ObstacleTypeV2.Minus)
        {
            typeV2 = Item.DraggedTypeV2;
        }

        switch (Item.DraggedTypeV2)
        {
            case ObstacleTypeV2.LJ:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Bubble;
                Resistance = Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.BRK:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Brick;
                Resistance = 1;
                break;
            case ObstacleTypeV2.O:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Adder;
                Resistance = Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.p:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Pit;
                Resistance = Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.__:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Empty;
                Resistance = 0;
                break;
            case ObstacleTypeV2.Plus:
                Resistance += Int32.Parse(_inputField.text);
//                    typeV2 = ObstacleTypeV2.__;
                break;

            case ObstacleTypeV2.Minus:
            {
                Resistance -= Int32.Parse(_inputField.text);
                if (Resistance <= 0)
                {
                    _spriteRenderer.sprite = Empty;
                    typeV2 = ObstacleTypeV2.__;
                }
            }
                break;
            case ObstacleTypeV2.BOXBMB:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = BoxBomb;
                Resistance = Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.ROWBMB:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = RowBomb;
                Resistance = Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.COLBMB:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = ColBomb;
                Resistance = Int32.Parse(_inputField.text);
                break;
            /************************************************************************************/
            case ObstacleTypeV2.LeftDownBrick:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBrick;
                Resistance = 1;
                transform.rotation = Quaternion.AngleAxis(180f, new Vector3(0f, 0f, 1f));

                break;
            case ObstacleTypeV2.LeftTopBrick:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBrick;
                Resistance = 1;
                transform.rotation = Quaternion.AngleAxis(90f, new Vector3(0f, 0f, 1f));

                break;
            case ObstacleTypeV2.RightTopBrick:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBrick;
                Resistance = 1;
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);

                break;
            case ObstacleTypeV2.RightDownBrick:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBrick;
                Resistance = 1;
                transform.rotation = Quaternion.AngleAxis(270f, new Vector3(0f, 0f, 1f));

                break;
            /****************************************************************************/
            case ObstacleTypeV2.RightDownBlock:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBlock;
                Resistance = Int32.Parse(_inputField.text);
                transform.rotation = Quaternion.AngleAxis(270f, new Vector3(0f, 0f, 1f));
                ResisText.transform.Rotate(new Vector3(0f, 0f,1f), -270f);

                break;
            case ObstacleTypeV2.RightTopBlock:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBlock;
                Resistance = Int32.Parse(_inputField.text);
                transform.rotation = Quaternion.AngleAxis(0f, new Vector3(0f, 0f, 1f));

                break;
            case ObstacleTypeV2.LeftDownBlock:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);                
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBlock;
                Resistance = Int32.Parse(_inputField.text);
                transform.rotation = Quaternion.AngleAxis(180f, new Vector3(0f, 0f, 1f));
                ResisText.transform.Rotate(new Vector3(0f, 0f,1f), -180f);
                break;
            case ObstacleTypeV2.LeftTopBlock:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = HalfBlock;
                Resistance = Int32.Parse(_inputField.text);
                transform.rotation = Quaternion.AngleAxis(90f, new Vector3(0f, 0f, 1f));
                ResisText.transform.Rotate(new Vector3(0f, 0f,1f), -90f);

                break;
            case ObstacleTypeV2.Sponge:
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                ResisText.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _spriteRenderer.sprite = Sponge;
                Resistance = Int32.Parse(_inputField.text);
                break;
        }

        L_Obstacle lObstacle = new L_Obstacle {typeV2 = typeV2, Resistance = Resistance};
        OnItemChange?.Invoke(lRow.Index, ItemIndex, lObstacle);
    }

    private static bool isDragging;

    private void OnMouseDrag()
    {
        isDragging = true;

        if (Item.DraggedTypeV2 != ObstacleTypeV2.Plus && Item.DraggedTypeV2 != ObstacleTypeV2.Minus)
        {
            OnMouseUpAsButton();
        }
    }

    private void OnMouseEnter()
    {
        if (isDragging)
        {
            OnMouseUpAsButton();
        }
    }

    private void OnMouseUp()
    {
        isDragging = false;
    }

    // note: this method is for edit purpose. i do not need this for now
    public void MakeBlock(ObstacleTypeV2 t, int resis)
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        switch (t)
        {
            case ObstacleTypeV2.LJ:
                _spriteRenderer.sprite = Bubble;
                Resistance = resis;
                break;
            case ObstacleTypeV2.BRK:
                _spriteRenderer.sprite = Brick;
                break;
            case ObstacleTypeV2.O:
                _spriteRenderer.sprite = Adder;
                Resistance = resis;
                break;
            case ObstacleTypeV2.p:
                _spriteRenderer.sprite = Pit;
                Resistance = resis;
                break;
            case ObstacleTypeV2.__:
                _spriteRenderer.sprite = Empty;
                Resistance = 0;
                break;
            case ObstacleTypeV2.Plus:
                Resistance += Int32.Parse(_inputField.text);
                break;
            case ObstacleTypeV2.Minus:
            {
                Resistance -= Int32.Parse(_inputField.text);
                if (Resistance <= 0)
                {
                    _spriteRenderer.sprite = Empty;
                    typeV2 = ObstacleTypeV2.__;
                }
            }
                break;
        }

        L_Obstacle lObstacle = new L_Obstacle {typeV2 = typeV2, Resistance = Resistance};

        _stageManager.UpdateStageArray(lRow.Index, ItemIndex, lObstacle);
//        OnItemChange?.Invoke(lRow.Index, ItemIndex, lObstacle);
    }
}