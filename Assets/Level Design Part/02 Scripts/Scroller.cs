﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour
{
    public Transform board;
    public float index = 0.1f;
    private void OnMouseDrag()
    {
        board.position = new Vector2(board.position.x, board.position.y + index);
    }
}
