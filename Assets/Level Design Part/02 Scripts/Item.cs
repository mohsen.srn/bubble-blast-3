﻿using _02_Scripts;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public ObstacleTypeV2 typeV2;
    public static ObstacleTypeV2 DraggedTypeV2;
    public Text Status;
    private void OnMouseDown()
    {
        DraggedTypeV2 = typeV2;
        Status.text = typeV2.ToString();
    }
}
