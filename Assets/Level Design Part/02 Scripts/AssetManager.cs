﻿using System.Collections.Generic;
using _02_Scripts;
using _02_Scripts._02_Stage.Data;
using _02_Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

public class AssetManager : MonoBehaviour
{
    public string path;
    public int sequence;
    private bool _isFix;
    public GameStages StageCollection;
    private L_StageManager _StageManager;
    private StageGameData _data;

    private void Start()
    {
        _data = FindObjectOfType<StageGameData>();
        _StageManager = FindObjectOfType<L_StageManager>();
    }

    public void CreateAsset()
    {
//        path = "Assets/05 Stages"; //AssetDatabase.GetAssetPath(Selection.activeObject);
        Stage stage = ScriptableObject.CreateInstance<Stage>();
    
        List<StageRow> stageBoard = new List<StageRow>();

        foreach (var row in _StageManager.Stage)
        {
            StageRow stageRow = new StageRow();
            foreach (var item in row)
            {
                StageObstacle.ObstacleType t = StageObstacle.ObstacleType.__;
                switch (item.typeV2)
                {
                    case ObstacleTypeV2.__:
                        t = StageObstacle.ObstacleType.__;
                        break;
                    case ObstacleTypeV2.p:
                        t = StageObstacle.ObstacleType.p;
                        break;
                    case ObstacleTypeV2.O:
                        t = StageObstacle.ObstacleType.O;
                        break;
                    case ObstacleTypeV2.LJ:
                        t = StageObstacle.ObstacleType.LJ;
                        break;
                    case ObstacleTypeV2.BRK:
                        t = StageObstacle.ObstacleType.BRK;
                        break;
                    case ObstacleTypeV2.BOXBMB:
                        t = StageObstacle.ObstacleType.BOXBMB;
                        break;
                    case ObstacleTypeV2.ROWBMB:
                        t = StageObstacle.ObstacleType.ROWBMB;
                        break;
                    case ObstacleTypeV2.COLBMB:
                        t = StageObstacle.ObstacleType.COLBMB;
                        break;
                    case ObstacleTypeV2.LeftDownBrick:
                        t = StageObstacle.ObstacleType.LDBr;
                        break;
                    case ObstacleTypeV2.LeftTopBrick:
                        t = StageObstacle.ObstacleType.LTBr;
                        break;
                    case ObstacleTypeV2.RightDownBrick:
                        t = StageObstacle.ObstacleType.RDBr;
                        break;
                    case ObstacleTypeV2.RightTopBrick:
                        t = StageObstacle.ObstacleType.RTBr;
                        break;
                    case ObstacleTypeV2.LeftDownBlock:
                        t = StageObstacle.ObstacleType.LDBl;
                        break;
                    case ObstacleTypeV2.LeftTopBlock:
                        t = StageObstacle.ObstacleType.LTBl;
                        break;
                    case ObstacleTypeV2.RightTopBlock:
                        t = StageObstacle.ObstacleType.RTBl;
                        break;
                    case ObstacleTypeV2.RightDownBlock:
                        t = StageObstacle.ObstacleType.RDBl;
                        break;
                    case ObstacleTypeV2.Sponge:
                        t = StageObstacle.ObstacleType.SPNG;
                        break;
                }
                stageRow.stageObstacles.Add(new StageObstacle(t, item.Resistance));
            }
            
            stageBoard.Add(stageRow);
        }
        
        stageBoard.Reverse();

        stage.stageBoard = stageBoard;
        stage.col = _StageManager.NumberOfRowItems;
        stage.boardRows = UtilityClass.GetRowNumberFromColumnNumber_Stage(stage.col);
        stage.gameRows = _StageManager.Stage.Count;
        stage.initBalls = InitBall.initBall;
        stage.IsFix = _isFix;
        
        CreateAsset(stage, "Stage " + sequence++, path);
        StageCollection.stages.Insert(0, stage);
        
        _data.SetFileNameToSaveAndLoad();
    }

    public static void CreateAsset<T>(T asset, string name, string path) where T : ScriptableObject
    {
#if UNITY_EDITOR
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");
        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
#endif
    }

    public void IsFix()
    {
        _isFix = !_isFix;
    }
}
