﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;
using System.IO;

namespace CheeseBurgames
{
    [InitializeOnLoad]
    public static class AutoScene
    {
        private const string VERSION = "1.0.0";

        private static string ProjectPath {
            get {
                string assetsPath = Application.dataPath;
                return Directory.GetParent(assetsPath).FullName;
            }
        }

        private static string PrefsKey {
            get {
                string projectPath = ProjectPath;
                return projectPath + ".AutoScene";
            }
        }

        static AutoScene() {
            Action updateAction = () => {
                UpdatePlayModeStartScene(EditorPrefs.GetString(PrefsKey, "none"));
            };

            EditorBuildSettings.sceneListChanged += updateAction;
            updateAction();
        }

        private static void UpdatePlayModeStartScene(string _value) {
            SceneAsset sceneAsset = null;

            if (_value == "auto") {
                foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
                    if (scene.enabled) {
                        sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                        break;
                    }
                }
            }
            else if (_value != "none") {
                sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(_value);
            }

            EditorSceneManager.playModeStartScene = sceneAsset;
        }

        [PreferenceItem("AutoScene")]
        public static void AutoScenePreferences() {
            string prefsValue = EditorPrefs.GetString(PrefsKey, "none");

            EditorGUILayout.LabelField("Version", VERSION, EditorStyles.boldLabel);
            EditorGUILayout.Space();

            // Build scene list
            string[] sceneGuids = AssetDatabase.FindAssets("t:Scene");
            string[] scenePathes = new string[sceneGuids.Length];
            for (int i = 0; i < sceneGuids.Length; i++) {
                scenePathes[i] = AssetDatabase.GUIDToAssetPath(sceneGuids[i]);
            }
            Array.Sort(scenePathes, string.Compare);

            // Finding selected index
            int selectedIndex = 0;
            if (prefsValue == "auto") {
                selectedIndex = 1;
            }
            else {
                int arrayIndex = Array.IndexOf(scenePathes, prefsValue);
                if (arrayIndex >= 0) {
                    selectedIndex = arrayIndex + 2;
                }
            }

            string[] menuEntries = new string[scenePathes.Length + 2];
            menuEntries[0] = "None";
            menuEntries[1] = "Auto";
            Array.Copy(scenePathes, 0, menuEntries, 2, scenePathes.Length);

            EditorGUI.BeginChangeCheck();
            selectedIndex = EditorGUILayout.Popup("Scene to load on Play", selectedIndex, menuEntries);
            if (EditorGUI.EndChangeCheck()) {
                if (selectedIndex == 0) {
                    prefsValue = "none";
                }
                else if (selectedIndex == 1) {
                    prefsValue = "auto";
                }
                else {
                    prefsValue = menuEntries[selectedIndex];
                }

                EditorPrefs.SetString(PrefsKey, prefsValue);
                UpdatePlayModeStartScene(prefsValue);
            }

            string helpBoxMessage;
            if (selectedIndex == 0) {
                helpBoxMessage = "The scenes currently loaded in the editor will be maintained when entering Play mode.\n\nThis is the default Unity behaviour.";
            }
            else if (selectedIndex == 1) {
                helpBoxMessage = "The first enabled scene in the Build Settings box will be loaded when entering Play mode. If no such scene exists, falls back to 'None'.";
            }
            else {
                helpBoxMessage = string.Format("The scene '{0}' will be loaded when entring Play mode. If the scene does not exist anymore, falls back to 'None'.", prefsValue);
            }
            EditorGUILayout.Space();
            EditorGUILayout.HelpBox(helpBoxMessage, MessageType.Info, true);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Made with ❤️ by Cheese Burgames");
        }
    }
}