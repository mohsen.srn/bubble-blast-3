﻿using System;
using System.Collections;
using _02_Scripts;
using UnityEngine;
using UnityEngine.UI;

public class JamBallsBtn : MonoBehaviour
{
    private Transform _transform;
    private Animator _animator;

    private Button _button;
    public Transform groundTransform;
    private void Start()
    {
        _transform = transform;
        _animator = GetComponent<Animator>();
        _button = GetComponent<Button>();
        
        BallManager.OnBallStart += ShowBtn;
        BallManager.OnBallStop += HideBtn;
        
        HideBtn();
    }

    private void OnDestroy()
    {
        BallManager.OnBallStart -= ShowBtn;
        BallManager.OnBallStop -= HideBtn;
    }

    private void ShowBtn()
    {
        _transform.position = new Vector3(0f, groundTransform.position.y);
        try
        {
            _animator.SetTrigger("in");
        }
        catch
        {
        }

        _button.enabled = true;
        gameObject.SetActive(true);
    }

    private void HideBtn()
    {
        if (_button.isActiveAndEnabled)
        {
            _button.enabled = false;
            gameObject.SetActive(false);
            try
            {
                _animator.SetTrigger("out");
            }
            catch
            {
            }
        }
    }
    
    public void Jam()
    {
        BallManager.GetInstance.CallBallsGetToGather();
        HideBtn();
    }
}
