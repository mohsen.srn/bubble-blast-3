﻿using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public GameObject lostPointPrefab;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 12)
        {
            // pit hit ground
            Destroy(other.gameObject);
        }
        else if (other.gameObject.layer == 15)
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.layer == 10)
        {
            /*GameObject go = Instantiate(lostPointPrefab,
                other.gameObject.transform.position,
                Quaternion.identity);
            
            LostingPoint lp = go.GetComponent<LostingPoint>();
            lp.Point = other.gameObject.GetComponent<Block>().Resistance;*/
        }
    }

    private void OnMouseDown()
    {
        ControllerBallManager.Instance.OnMouseDown();
    }

    private void OnMouseDrag()
    {
        ControllerBallManager.Instance.OnMouseDrag();
    }

    private void OnMouseUp()
    {
        ControllerBallManager.Instance.OnMouseUp();
    }
}