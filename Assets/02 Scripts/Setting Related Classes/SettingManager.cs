﻿using UnityEngine;

public class SettingManager : MonoBehaviour
{
    public static SettingManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            
            CheckBazaar();
            CheckMyket();
            
            DontDestroyOnLoad(this);
        } else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private readonly string _soundPref = "Is Sound Enable";
    private readonly string _musicPref = "Is Music Enable";

    private const int Enable = 1;
    private const int Disable = 0;

    public delegate void SettingActions();
    public static SettingActions OnSoundSettingChange;
    public static SettingActions OnMusicSettingChange;
    
    void Start()
    {
        if (!PlayerPrefs.HasKey(_soundPref))
        {
            PlayerPrefs.SetInt(_soundPref, Enable);
        }
        if (!PlayerPrefs.HasKey(_musicPref))
        {
            PlayerPrefs.SetInt(_musicPref, Enable);
        }
    }

    public bool IsMusicEnabled()
    {
        int result = PlayerPrefs.GetInt(_musicPref, Enable);
        return result == Enable;
    }
    public bool IsSoundEnabled()
    {
        int result = PlayerPrefs.GetInt(_soundPref, Enable);
        return result == Enable;
    }

    public void SetMusic(bool m)
    {
        PlayerPrefs.SetInt(_musicPref, m?Enable:Disable);
        OnMusicSettingChange?.Invoke();
    }
    public void SetSound(bool m)
    {
        PlayerPrefs.SetInt(_soundPref, m?Enable:Disable);
        OnSoundSettingChange?.Invoke();
    }

    private bool _isBazaarInstalled = false;
    private bool _isMyketInstalled = false;

    public bool IsBazaarInstalled()
    {
        return _isBazaarInstalled;
    }

    public bool IsMyketInstalled()
    {
        return _isMyketInstalled;
    }

    /// <summary>
    /// check if bazaar is installed to players phone or not.
    /// game uses this information to recognize iranian players among others
    /// </summary>
    private string _bazaarPackageName = "com.farsitel.bazaar";

    private string _myketPackageName = "ir.mservices.market";
    private void CheckBazaar()
    {
        _isBazaarInstalled = UtilityClass.IsAppInstalled(_bazaarPackageName);
    }

    private void CheckMyket()
    {
        _isMyketInstalled = UtilityClass.IsAppInstalled(_myketPackageName);
    }
}
