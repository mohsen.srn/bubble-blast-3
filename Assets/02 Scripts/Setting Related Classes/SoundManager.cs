﻿using _02_Scripts._02_Stage;
using _02_Scripts.Obstacles;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] BubbleCollideClips;
    public AudioSource BubbleCollide;

    public AudioClip[] BubblePopClips;
    public AudioSource BubblePop;

    public AudioClip BrickCollideClip, BrickBreakClip;
    public AudioSource BrickCollide;
    
    public AudioSource BallAdderCollide;
    
    public AudioClip[] TakeStarClips;
    public AudioSource TakeStar;
    
    public AudioClip[] BallReachGroundClips;
    public AudioSource BallReachGround;

    public AudioSource DecreasedPoint;

    public AudioClip WinClip, LostClip;
    public AudioSource GameResult;

    private static SoundManager _instance;
    public static SoundManager Instance => _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        } else if (_instance != this)
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        if (SettingManager.Instance.IsSoundEnabled())
        {
            EnableSounds();
        } else DisableSounds();

        SettingManager.OnSoundSettingChange = SettingManager.OnSoundSettingChange += () =>
        {
            if (FindObjectOfType<SettingManager>().IsSoundEnabled())
            {
                EnableSounds();
            }
            else DisableSounds();
        };
    }

    private void EnableSounds()
    {
        Block.OnCollide += PlayBubbleCollideSound;
        Block.OnBlast += PlayBubblePopSound;
        EmbeddedBrick.OnCollide += PlayBrickCollide;
        EmbeddedBrick.OnBreak += PlayBrickBreak;
        BallPlus.OnCollide += PlayBallAdderCollideSound;
        ProgressBarManager.OnTakeStar += PlayTakeStarSound;
        WinManager.OnPunish += DecreasedPoints;
        StageManager.OnLost += LostGameSound;
        StageManager.OnWin += WinGameSound;
    }

    private void DisableSounds()
    {
        Block.OnCollide -= PlayBubbleCollideSound;
        Block.OnBlast -= PlayBubblePopSound;
        EmbeddedBrick.OnCollide -= PlayBrickCollide;
        EmbeddedBrick.OnBreak -= PlayBrickBreak;
        BallPlus.OnCollide -= PlayBallAdderCollideSound;
        ProgressBarManager.OnTakeStar -= PlayTakeStarSound;
        WinManager.OnPunish -= DecreasedPoints;
        StageManager.OnLost -= LostGameSound;
        StageManager.OnWin -= WinGameSound;
    }

    private bool _isBubbleSilence;
    private void PlayBubbleCollideSound(int a)
    {
        int r = UtilityClass.MakeRandNumber(0, BubbleCollideClips.Length);
        if (BubbleCollide != null && !_isBubbleSilence)
        {
            BubbleCollide.PlayOneShot(BubbleCollideClips[r]);
            _isBubbleSilence = true;
            Invoke("UnSilenceBubble", 0.1f);
        }
    }

    private void UnSilenceBubble()
    {
        _isBubbleSilence = false;
    }
    
    private void PlayBubblePopSound(int a)
    {
        if (BubblePop!=null)
        {
            BubblePop.PlayOneShot(BubblePopClips[UtilityClass.MakeRandNumber(0, BubblePopClips.Length)]);
        }
    }

    private bool _isBrickSilence;
    private void PlayBrickCollide(int a)
    {
        if (BrickCollide != null && !_isBrickSilence)
        {
            BrickCollide.PlayOneShot(BrickCollideClip);
            _isBrickSilence = true;
            Invoke("UnSilenceBrick", 0.2f);
        }
    }

    private void UnSilenceBrick()
    {
        _isBrickSilence = false;
    }

    private void PlayBrickBreak(int a)
    {
        if (BrickCollide != null && BrickBreakClip != null)
        {
            BrickCollide.PlayOneShot(BrickBreakClip);
        }
    }

    private void PlayBallAdderCollideSound(int a)
    {
        if (BallAdderCollide != null)
        {
            BallAdderCollide.PlayOneShot(BallAdderCollide.clip);
        }
    }

    private void PlayTakeStarSound(int index)
    {
        if (TakeStar != null)
        {
            TakeStar.PlayOneShot(TakeStarClips[index-1]);
        }
    }

    private void PlayBallReachGround()
    {
        if (BallReachGround != null)
        {
            BallReachGround.PlayOneShot(BallReachGroundClips[UtilityClass.MakeRandNumber(0, BallReachGroundClips.Length)]);
        }
    }

    private void DecreasedPoints(int a)
    {
        if (DecreasedPoint != null)
        {
            DecreasedPoint.PlayOneShot(DecreasedPoint.clip);
        }
    }

    private void LostGameSound()
    {
        if (GameResult != null)
        {
            GameResult.PlayOneShot(LostClip);
        }
    }

    private void WinGameSound()
    {
        if (GameResult != null)
        {
            GameResult.PlayOneShot(WinClip);
        }
    }
}
