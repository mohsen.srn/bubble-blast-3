using UnityEngine;

namespace _02_Scripts.Setting_Related_Classes
{
    public class SettingToggleButton : MonoBehaviour
    {
        private Animator _animator;

        private bool _open;
        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void ExtendSettingPanel()
        {
            if(!_open)
            {
                _open = true;
                _animator.SetTrigger("open");
            }
        }

        public void CloseSettingPanel()
        {
            if (_open)
            {
                _open = false;
                _animator.SetTrigger("close");
            }
        }

        public void ToggleSettingPanel()
        {
            if (_open)
            {
                CloseSettingPanel();
            }
            else
            {
                ExtendSettingPanel();
            }
        }
    }
}
