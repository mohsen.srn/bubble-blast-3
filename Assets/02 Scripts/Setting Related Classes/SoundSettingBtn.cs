﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettingBtn : MonoBehaviour
{
    public Image ButtonImage;
    public Sprite SoundOnSprite;
    public Sprite SoundOffSprite;

    private bool _isSoundOn;
    
    void Start()
    {
        _isSoundOn = SettingManager.Instance.IsSoundEnabled();
        ButtonImage.sprite = _isSoundOn ? SoundOnSprite : SoundOffSprite;
    }

    public void SetSoundSetting()
    {
        _isSoundOn = SettingManager.Instance.IsSoundEnabled();
        if (_isSoundOn)
        {
            ButtonImage.sprite = SoundOffSprite;
            SettingManager.Instance.SetSound(false);
        }
        else
        {
            ButtonImage.sprite = SoundOnSprite;
            SettingManager.Instance.SetSound(true);
        }
    }
}
