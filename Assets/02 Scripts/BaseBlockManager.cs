﻿using System.Diagnostics;
using NaughtyAttributes;
using UnityEngine;

public abstract class BaseBlockManager : MonoBehaviour
{
    protected float _width, _height;

    [BoxGroup("Assets")] public GameObject BallPlus;

    [BoxGroup("Assets")] public GameObject BlockPrefab;
    [BoxGroup("Assets")] public GameObject NullPrefab;

    /*[ReadOnly]*/ protected int HEIGHT_NUMBER = 10;//9;

    /*[ReadOnly]*/ protected int ROW_NUMBER = 7;//6;

    [BoxGroup("Assets")] public GameObject RowPrefab;
}
