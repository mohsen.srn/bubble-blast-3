﻿using System.Collections;
using _02_Scripts;
using UnityEngine;

public abstract class CameraManager : MonoBehaviour
{
    protected abstract int GetRowNumbers();
    protected abstract int GetColumnNumbers();

    protected abstract float TopUiHeightSize();
    protected abstract float GetYPositionOfTopUi();

    public BoxCollider2D topWall, ground, rightWall, leftWall, finishLine;
    public GameObject borderTop, borderDown;
    public Transform boardManager, ballManager, walls, controller;
    public BoxCollider2D controllerCollider;
    protected Camera _camera;
    protected int _columnNumber;
//    public RectTransform topUiCanvas;

    protected void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    protected void Start()
    {
        _columnNumber = GetColumnNumbers();

        ballManager = BallManager.GetInstance.ballManagerTransform;
        
        float rowNumber = GetRowNumbers();
        float orthographicSize = rowNumber / 2 + 2; //_columnNumber / _camera.aspect / 2;
        
//        _camera.orthographicSize = orthographicSize;

        StartCoroutine(ChangeCameraOrthoSize(orthographicSize));
    }

    public float Speed = 0.5f;

    protected IEnumerator ChangeCameraOrthoSize(float orthoSize)
    {
        InitBoard(orthoSize);

        float diff = _camera.orthographicSize - orthoSize;
        int avoidInfinityLoop = 0;
        
        while (Mathf.Abs(_camera.orthographicSize - orthoSize) > 0.01f)
        {
//            _camera.orthographicSize = _camera.orthographicSize + Speed * Time.deltaTime;
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, orthoSize, Speed); // + Speed * Time.deltaTime;
            _camera.Render();
            float newDiff = _camera.orthographicSize - orthoSize;
            if (diff * newDiff < 0)
            {
                // if sign of xDiff and newXDiff is different, so ball passed target 
                break;
            }

            if (avoidInfinityLoop++ > 200)
            {
                break;
            }

            yield return null;
        }

        _camera.orthographicSize = orthoSize;
        
        // we use canvas as full screen, so commented these lines out
//        topUiCanvas.position = new Vector3(0f, GetYPositionOfTopUi(), 0f); ////////
//        topUiCanvas.sizeDelta = new Vector2(_camera.orthographicSize * 2 * _camera.aspect, TopUiHeightSize());
    }

    public float wallWidth = 0.2f;
    protected void InitBoard(float orthographicSize)
    {
        var camPosition = _camera.transform.position;

//        topUiCanvas.position = new Vector3(0f, GetYPositionOfTopUi(), 0f); ////////
//        topUiCanvas.sizeDelta = new Vector2(orthographicSize * 2 * _camera.aspect, TopUiHeightSize());
//        
        /******************************************/
        topWall.size = new Vector2(GetColumnNumbers() + 2, wallWidth);
        Transform topTransform = topWall.transform;
        ground.size = new Vector2(GetColumnNumbers() + 2, wallWidth);
        Transform groundTransform = ground.transform;
        finishLine.size = new Vector2(GetColumnNumbers() + 2, 0.5f);
        rightWall.size = new Vector2(1, orthographicSize * 2 + 2);
        leftWall.size = new Vector2(1, orthographicSize * 2 + 2);

        topTransform.localPosition = new Vector2(0f, GetRowNumbers() / 2f + 1f);
        groundTransform.localPosition = new Vector2(0f, -1 * GetRowNumbers() / 2f);
        finishLine.transform.localPosition = new Vector2(0f, groundTransform.localPosition.y + 0.8f);
        rightWall.transform.localPosition = new Vector3(GetColumnNumbers() * 1f / 2 + 0.5f, 0f);
        leftWall.transform.localPosition = new Vector3(-1 * GetColumnNumbers() * 1f / 2 - 0.5f, 0f);

        topWall.offset = new Vector2(0f, 0.5f * wallWidth - 0.5f);
        ground.offset = new Vector2(0f, (1f - wallWidth) / 2f);
        
/********************************************/
//        topWall.GetComponent<Wall>().Start();
//        ground.GetComponent<Wall>().Start();
        rightWall.GetComponent<Wall>().Start();
        leftWall.GetComponent<Wall>().Start();

/********************************************************/
        borderTop.transform.localPosition = new Vector2(0f, topTransform.position.y + (0.5f * wallWidth - 0.5f) - wallWidth/2f);
        borderDown.transform.localPosition = new Vector2(0f, groundTransform.position.y + 0.5f);
        /********************************************/
        walls.position = new Vector3(camPosition.x,
            (orthographicSize - GetRowNumbers() / 2f)/2 - TopUiHeightSize() + 0.5f, 0f);
        boardManager.position = new Vector3(camPosition.x, topTransform.position.y - 0.5f, 0f);

        ballManager.position = new Vector3(camPosition.x, groundTransform.position.y + 0.8f, 0f);
        controller.position = new Vector3(camPosition.x, camPosition.y + 0.2f, 0f);
        controllerCollider.size = new Vector2(GetColumnNumbers() + 1f,
            UtilityClass.GetRowNumberFromColumnNumber_Stage(GetColumnNumbers()) - 1);
/********************************************************/
    }
}
