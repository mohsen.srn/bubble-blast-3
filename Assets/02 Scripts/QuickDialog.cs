﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickDialog : MonoBehaviour
{
    public GameObject overlay;
    private void OnEnable()
    {
        overlay.SetActive(true);
    }

    private void Update()
    {
        try
        {
            Controller.GetInstance.IsDisable = true;
        }
        catch
        {
        }
    }

    public void OnAnimationEnded()
    {
        this.gameObject.SetActive(false);
    }
    
    private void OnDisable()
    {
        overlay.SetActive(false);

        try
        {
            Controller.GetInstance.IsDisable = false;
        }
        catch
        {
        }
    }
}
