﻿using _02_Scripts.Obstacles;
using UnityEngine;

public abstract class Breakable : Obstacle
{
    // this is performance friendly to take components manually
    public GameObject breakableColliders;
    
    private new void Start()
    {
        base.Start();
    }

    protected void SetColliderEnable()
    {
        breakableColliders.SetActive(true);
    }

    protected void SetColliderDisable()
    {
        breakableColliders.SetActive(false);
    }

    public abstract void Explode();

    public override void Eliminate()
    {
        // this command cleans up row from 0 resistance obstacles
        row.CleanUpRow();
    }
}
