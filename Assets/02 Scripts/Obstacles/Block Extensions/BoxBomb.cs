﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class BoxBomb : ExtentionInterface
{
    public SpriteRenderer SpriteRenderer;
    public List<Sprite> spriteSequence;
    
    public new void Init()
    {
        base.Init();
        Vector2 sd = textMeshProTransform.sizeDelta;
        textMeshProTransform.sizeDelta = new Vector2(sd.x, 0.445f);
        textMeshProTransform.localPosition = new Vector3(0f, -0.22f, 0f);
        
        textMeshProTransform.ForceUpdateRectTransforms();

        StartCoroutine(ChangeSprite());
    }
    
    public override void Collide()
    {
//        StartCoroutine(ChangeSprite());
    }

    IEnumerator ChangeSprite()
    {
        while (true)
        {
            float randNum = Random.Range(0.1f, 0.6f);
            
            SpriteRenderer.sprite = spriteSequence[0];
            yield return new WaitForSeconds(randNum);
            SpriteRenderer.sprite = spriteSequence[1];
            yield return new WaitForSeconds(0.1f);
            SpriteRenderer.sprite = spriteSequence[0];
        }
    }

    public override void Explode()
    {
        if(ExplodeParticles!= null) ExplodeParticles.Play();
        Collider2D[] collider2Ds = new Collider2D[20];
        var size = Physics2D.OverlapBoxNonAlloc(transform.position,
            Vector2.one * 2, 0, collider2Ds, _blockLayerMask);

        CallExplodeInBreakables(collider2Ds, size);
    }
}
