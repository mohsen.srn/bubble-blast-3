﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regular : ExtentionInterface
{
    public override void Collide()
    {
    }

    public override void Explode()
    {
        if(ExplodeParticles!= null) ExplodeParticles.Play();
    }

    public new void Init()
    {
       base.Init();
        Vector2 sd = textMeshProTransform.sizeDelta;
        textMeshProTransform.sizeDelta = new Vector2(sd.x, 0.7f);
        
        textMeshProTransform.ForceUpdateRectTransforms();

        textMeshProTransform.localPosition = new Vector3(0f, 0f, 0f);
    }
}
