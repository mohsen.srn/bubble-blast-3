﻿using System.Collections.Generic;
using RTLTMPro;
using TMPro;
using UnityEngine;

public abstract class ExtentionInterface: MonoBehaviour
{
    protected int _blockLayerMask;
    public ParticleSystem ExplodeParticles;
    public RectTransform textMeshProTransform;
    public TextMeshPro textMeshPro;
    public TMP_FontAsset font;
    protected void Start()
    {
        _blockLayerMask = 1 << 10;
        int a = 1 << 15;
        _blockLayerMask |= a;
    }

    public abstract void Collide();
    public abstract void Explode();

    protected void CallExplodeInBreakables(Collider2D[] collider2Ds, int size)
    {
        for (int i = 0; i < size; i++)
        {
            BlockCollider mCollider = collider2Ds[i].GetComponent<BlockCollider>();
            if (mCollider != null && mCollider.isActiveAndEnabled)
            {
                mCollider.block.Explode();
            }
            
            BrickCollider mBrCollider = collider2Ds[i].GetComponent<BrickCollider>();
            if (mBrCollider != null && mBrCollider.isActiveAndEnabled)
            {
                mBrCollider.brick.Explode();
            }
        }
    }

    public void Init()
    {
        textMeshPro.font = font;
    }
}