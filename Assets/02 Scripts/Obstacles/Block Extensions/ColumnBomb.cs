﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnBomb : ExtentionInterface
{
    public SpriteRenderer spriteRenderer;
    public List<Sprite> spriteSequence;
    public override void Collide()
    {
        StartCoroutine(ChangeSprite());
    }

    IEnumerator ChangeSprite()
    {
        spriteRenderer.sprite = spriteSequence[1];
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.sprite = spriteSequence[0];
    }
 
    public override void Explode()
    {
        if(ExplodeParticles!= null) ExplodeParticles.Play();
        Collider2D[] collider2Ds = new Collider2D[20];
        var size = Physics2D.OverlapBoxNonAlloc(transform.position,
            new Vector2(0.2f, 70f), 0, collider2Ds, _blockLayerMask);
        CallExplodeInBreakables(collider2Ds, size);
    }

    public new void Init()
    {
        base.Init();
        
        spriteRenderer.sprite = spriteSequence[0];
        
        Vector2 sd = textMeshProTransform.sizeDelta;
        textMeshProTransform.sizeDelta = new Vector2(sd.x, 0.50f);
        textMeshProTransform.localPosition = new Vector3(0f, 0f, 0f);
        textMeshProTransform.ForceUpdateRectTransforms();
    }
}
