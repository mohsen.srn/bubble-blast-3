﻿using System;
using UnityEngine;

public class BlockCollider : MonoBehaviour
{
    public Block block;
    // The one and only purpose of this script is to transfer Collision an Trigger events to block script
    private void OnCollisionEnter2D(Collision2D other)
    {
        block.OnCollisionEnter2D(other);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        block.OnTriggerEnter2D(other);
    }

    private void OnMouseDrag()
    {
        block.OnMouseDrag();
    }

    private void OnMouseUp()
    {
        block.OnMouseUp();
    }

    private void OnMouseDown()
    {
        block.OnMouseDown();
    }
}
