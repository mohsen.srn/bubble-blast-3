﻿using System;
using System.Collections;
using NaughtyAttributes;
using UnityEngine;

public class BlockSide : MonoBehaviour
{
    public enum Side
    {
        Top,
        Bottom,
        Right,
        Left,
        RightTopDiagonal,
        LeftTopDiagonal,
        RightDownDiagonal,
        LeftDownDiagonal
    }

    public Side side;
    public Transform blockTransform;
    private Block _blockComponent;
    public Transform top, bottom, right, left;
    private Transform _counterText;
    [ReadOnly] public bool IsCollide;
    private Transform _blockCollider;
//    private Transform _row;

    [ReadOnly] public bool BlockIsHalf;
    
    public Transform halfCollider;
    public Transform anchor;

    private bool _doesBlockComponentAssigned = false;
    private void Awake()
    {
//        blockTransform = transform.parent.gameObject.transform;
        _blockComponent = blockTransform.GetComponent<Block>();
        _doesBlockComponentAssigned = true; // this is an controlling flag to prevent _bug and don't use if(_blockComponent != null)
        
        _counterText = _blockComponent.LiveText.rectTransform;
        _blockCollider = _blockComponent.breakableColliders.transform;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 8 && _blockComponent.Resistance > 1 )
        {
//            StopCoroutine("Return2ItsPosition");
//            StopCoroutine("Return2ItsPosition_half");
//            ResetScale();
            switch (side)
            {
                case Side.Top:
                    if (BlockIsHalf) return;
                    StartCoroutine("Return2ItsPosition", bottom);
                    return;
                case Side.Bottom:
                    if (BlockIsHalf) return;
                    StartCoroutine("Return2ItsPosition", top);
                    return;
                case Side.Right:
                    if (BlockIsHalf) return;
                    StartCoroutine("Return2ItsPosition", left);
                    return;
                case Side.Left:
                    if (BlockIsHalf) return;
                    StartCoroutine("Return2ItsPosition", right);
                    return;
                case Side.LeftDownDiagonal:
                    goto case Side.RightTopDiagonal;
                case Side.LeftTopDiagonal:
                    goto case Side.RightTopDiagonal;
                case Side.RightDownDiagonal:
                    goto case Side.RightTopDiagonal;
                case Side.RightTopDiagonal:
                    StartCoroutine("Return2ItsPosition_half", anchor);
                    return;
            }
        }
    }

    IEnumerator Return2ItsPosition(Transform side)
    {
        yield return null;
        
        if(!IsCollide) yield break;
        IsCollide = false;
        
        side.parent = blockTransform.parent;
        blockTransform.parent = side;
        // we change parent of counter text to prevent change in text scale
        _counterText.SetParent(_blockComponent.row.rowTransform, true);
        _blockCollider.SetParent(_blockComponent.row.rowTransform, true);
        
        side.localScale = new Vector3(1f, 0.9f, 1f);
//        yield return null;
//        yield return null;
        yield return new WaitForSeconds(0.2f);
        side.localScale = new Vector3(1f, 0.8f, 1f);
        yield return new WaitForSeconds(0.1f);
//        yield return null;

        side.localScale = new Vector3(1f, 0.95f, 1f);
        yield return new WaitForSeconds(0.1f);
//        yield return null;

        ResetScale();
    }
    
    IEnumerator Return2ItsPosition_half(Transform side)
    {
        yield return null;
        
        if(!IsCollide) yield break;
        IsCollide = false;
        
        side.parent = blockTransform.parent;
        blockTransform.parent = side;
        // we change parent of counter text to prevent change in text scale
        _counterText.SetParent(_blockComponent.row.rowTransform, true);
        _blockCollider.SetParent(_blockComponent.row.rowTransform, true);
        
        side.localScale = new Vector3(0.9f, 0.9f, 1f);
        yield return null;
//        yield return null;
        side.localScale = new Vector3(0.8f, 0.8f, 1f);
        yield return null;
        side.localScale = new Vector3(0.95f, 0.95f, 1f);
        yield return null;
        ResetScale();
    }

    public void ResetScale()
    {
        top.localScale = new Vector3(1f, 1f, 1f);
        right.localScale = new Vector3(1f, 1f, 1f);
        bottom.localScale = new Vector3(1f, 1f, 1f);
        left.localScale = new Vector3(1f, 1f, 1f);
        
        if (BlockIsHalf)
            anchor.localScale = new Vector3(1f, 1f, 1f);

        // in some situations, a method outside this class calls this method("ResetScale()") before Awake() method
        // get called. so in that situation, these inits are not done. so we check flag and init the proprties
        if (!_doesBlockComponentAssigned)
        {
            _blockComponent = blockTransform.GetComponent<Block>();
            _counterText = _blockComponent.LiveText.rectTransform;
            _blockCollider = _blockComponent.breakableColliders.transform;
        }
        
        blockTransform.parent = _blockComponent.row.rowTransform;
        blockTransform.localPosition = _blockComponent.position;
        
        top.parent = blockTransform;
        bottom.parent = blockTransform;
        right.parent = blockTransform;
        left.parent = blockTransform;

        if (BlockIsHalf)
            anchor.parent = blockTransform;
        
        _counterText.SetParent(blockTransform, true);
        _blockCollider.SetParent(blockTransform, true);

        _blockCollider.localScale = Vector3.one;
        
        if (BlockIsHalf)
            halfCollider.parent = blockTransform;
        
        _counterText.localScale = left.localScale = new Vector3(1f, 1f, 1f);

        if (BlockIsHalf)
        {
            switch (side)
            {
                case Side.LeftDownDiagonal:
                    _counterText.localPosition = new Vector3(-0.15f, -0.15f);
                    break;
                case Side.LeftTopDiagonal:
                    _counterText.localPosition = new Vector3(-0.15f, 0.15f);
                    break;
                case Side.RightDownDiagonal:
                    _counterText.localPosition = new Vector3(0.15f, -0.15f);
                    break;
                case Side.RightTopDiagonal:
                    _counterText.localPosition = new Vector3(0.15f, 0.15f);
                    break;
            }            
        } else
        {
            // if this block in not half, so we reset position to (0, 0, 0)
            _counterText.localPosition = Vector3.zero;
        }
        
        top.localPosition = new Vector2(0f, 0.5f);
        bottom.localPosition = new Vector2(0f, -0.5f);
        left.localPosition = new Vector2(-0.5f, 0f);
        right.localPosition = new Vector2(0.5f, 0f);
    }

    private void OnMouseDown()
    {
        _blockComponent.OnMouseDown();
    }

    private void OnMouseDrag()
    {
        _blockComponent.OnMouseDrag();
    }

    private void OnMouseUp()
    {
        _blockComponent.OnMouseUp();
    }
}
