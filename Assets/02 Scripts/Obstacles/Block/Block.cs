﻿using System.Collections;
using System.Linq.Expressions;
using _02_Scripts;
using NaughtyAttributes;
using TMPro;
using UnityEngine;

public class Block : Breakable
{
    public Sprite BoxBombBlock;
    public Color BlockColor_1;
    
    public Sprite RowBombBlock;
    public Color BlockColor_2;
    
    public Sprite ColumnBombBlock;
    public Color BlockColor_4;

    public Sprite OrdinaryBlock;
    public Color BlockColor_3;

    public Sprite SquareBlock;

    public ParticleSystem DropParticles;
    public ParticleSystem explodeParticles;
    public ParticleSystem[] explodeParticles2;

    public SpriteRenderer spriteRenderer;

    // these two are for optimization
    public GameObject blockGameObject;
    public Transform blockTransform;

    // define event for when ball collide to a breakable and decrease its resistance
    public static event ObstacleActions OnResistanceDecreased;
    public static event ObstacleActions OnBlockReachedGround;
    public static event ObstacleActions OnExplode;
    
    public static event ObstacleActions OnCollide;
    public static event ObstacleActions OnBlast;
    
    [ReadOnly] public Vector3 position;
    public Transform topTransform, bottomTransform, rightTransform, leftTransform,
        rtAnchorTransform, rdAnchorTransform, ltAnchorTransform, ldAnchorTransform; // use this handler for scaling in right direction

    public BlockSide top, bottom, right, left,
        rtDiagonal, rdDiagonal, ltDiagonal, ldDiagonal;

    public ExtentionInterface extension;
    public ExtentionInterface[] extensions;
    
    public GameObject regularCollider;
    public GameObject verticalRowBombCollider;
    public GameObject horizontalColumnCollider;
    public GameObject squareCollider;
        
    public TextMeshPro regularText;
    public TextMeshPro linearBombText;
    public TextMeshPro boxBombText;

    public Sprite bubbleShadow, rowBombShadow, columnBmbShadow, boxBmbShadow, squareShadow;

    [BoxGroup("Half Blocks")] public Sprite rightTopBlockSprite, rightDownBlockSprite, leftTopBlockSprite, leftDownBlockSprite;
    [BoxGroup("Half Blocks")] public GameObject rightTopBlockCollider, rightDownBlockCollider, leftTopBlockCollider, leftDownBlockCollider;
//    [BoxGroup("Half Blocks")] public GameObject rightTopBlock, rightDownBlock, leftTopBlock, leftDownBlock;
    
    private new void Awake()
    {
        base.Awake();
        spriteRenderer = GetComponent<SpriteRenderer>();

        blockGameObject = gameObject;
        blockTransform = obstacleTransform;
    }

    private new void Start()
    {
        base.Start();
        MakeBlock(obstacleType);
    }

    public new void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);

        if(Resistance > 0)
        {
            SpecifyResistance(Resistance - 1); // note: here is an important and bug able part
            OnResistanceDecreased?.Invoke(Resistance);
            DropParticles.Play();
            OnCollide?.Invoke(0); // argument is not important
        
            extension.Collide();
        
            bottom.IsCollide = true;
            top.IsCollide = true;
            right.IsCollide = true;
            left.IsCollide = true;
            rtDiagonal.IsCollide = true;
            rdDiagonal.IsCollide = true;
            ltDiagonal.IsCollide = true;
            ldDiagonal.IsCollide = true;
        }

        // below code make changes in Level persistent. Problem is this code changes ScriptableObject too. so for now we get rid of it 
        // obstacleMetaData.resistance = Resistance;
    }

    public override void Eliminate()
    {
        base.Eliminate();
        
        GetComponent<Animator>().SetTrigger("Blast");
        Visible = false;
        
        shadow.enabled = false;
        
        ResetBlock();
        
        SetColliderDisable();
        /*_spriteRenderer.enabled = false;*/
//        LiveText.enabled = false;
        explodeParticles.Play(true);
        foreach (var particle in explodeParticles2)
        {
            particle.Play();
        }
        OnBlast?.Invoke(0); // here argument is not important
        
        // bombs explode only when they did not collide to finish line
        if (!_isCollide2FinishLine) extension.Explode();
        
        obstacleType = Type.Null; // we HAVE TO set type to null as early as possible even we wait for 3 seconds to add block to pool and inactive it

        if(isActiveAndEnabled)
            StartCoroutine(EliminateBlockWithDelay(3f));
    }

    IEnumerator EliminateBlockWithDelay(float delay)
    {
        yield return new WaitForSeconds(0.1f);
        SetColliderDisable(); // we disable colliders again so that make sure MakeBlock(Type) method do not enable it again and make some strange bugs
        
        yield return new WaitForSeconds(delay);
        PoolManager.Instance.Back2BlockPool(this);
        SetColliderEnable();
        LiveText.enabled = true;
        spriteRenderer.enabled = true;
    }

    public void MakeBlock(Type type)
    {
        // we really need to make collider enable with delay. if collider of block be active as soon as it instantiates sometimes some bugs happen.(collides to ground!!!)   
//        Invoke("SetColliderEnable", 0.09f);
        obstacleType = type;
        Color blockColor = Color.red;

        boxBombText.enabled = false;
        linearBombText.enabled = false;
        regularText.enabled = false;

        shadow.enabled = true;
        
        switch (type)
        {
            case Type.BoxBomb:
                spriteRenderer.sprite = BoxBombBlock;
                blockColor = BlockColor_1;
                extension = extensions[0];
                breakableColliders = regularCollider;
                LiveText = boxBombText;
                boxBombText.enabled = true;
                shadow.sprite = boxBmbShadow;
                
                goto FullBlockCommon;
            case Type.RowBomb:
                spriteRenderer.sprite = RowBombBlock;
                blockColor = BlockColor_2;
                extension = extensions[2];
                breakableColliders = verticalRowBombCollider;
                LiveText = linearBombText;
                linearBombText.enabled = true;
                shadow.sprite = rowBombShadow;
                
                goto FullBlockCommon;
            case Type.Ordinary:
                spriteRenderer.sprite = OrdinaryBlock;
                blockColor = BlockColor_3;
                extension = extensions[1];
                breakableColliders = regularCollider;
                LiveText = regularText;
                regularText.enabled = true;
                shadow.sprite = bubbleShadow;
                
                goto FullBlockCommon;
            case Type.ColumnBomb:
                spriteRenderer.sprite = ColumnBombBlock;
                blockColor = BlockColor_4;
                extension = extensions[3];
                breakableColliders = horizontalColumnCollider;
                LiveText = linearBombText;
                linearBombText.enabled = true;
                shadow.sprite = columnBmbShadow;

                goto FullBlockCommon;
                case Type.Sponge:
                    spriteRenderer.sprite = SquareBlock;
                    extension = extensions[1];
                    breakableColliders = squareCollider;
                    LiveText = regularText;
                    regularText.enabled = true;
                    shadow.sprite = squareShadow;
            
            FullBlockCommon:
                bottom.BlockIsHalf = false;
                top.BlockIsHalf = false;
                right.BlockIsHalf = false;
                left.BlockIsHalf = false;
                ldDiagonal.BlockIsHalf = false;
                rdDiagonal.BlockIsHalf = false;
                ltDiagonal.BlockIsHalf = false;
                rtDiagonal.BlockIsHalf = false;
                bottom.gameObject.SetActive(true);
                top.gameObject.SetActive(true);
                right.gameObject.SetActive(true);
                left.gameObject.SetActive(true);
                
                regularText.transform.localPosition = Vector3.zero;
                break;
            /*********************************************************/
            
            case Type.RightTopBlock:
                spriteRenderer.sprite = rightTopBlockSprite;
                breakableColliders = rightTopBlockCollider;
                rtDiagonal.gameObject.SetActive(true);
                regularText.transform.localPosition = new Vector3(0.15f, 0.15f);

                goto HalfBlockCommon; // go to run common codes related to halfBlocks 
            case Type.RightDownBlock:
                spriteRenderer.sprite = rightDownBlockSprite;
                breakableColliders = rightDownBlockCollider;
                rdDiagonal.gameObject.SetActive(true);
                regularText.transform.localPosition = new Vector3(0.15f, -0.15f);

                goto HalfBlockCommon;
            case Type.LeftTopBlock:
                spriteRenderer.sprite = leftTopBlockSprite;
                breakableColliders = leftTopBlockCollider;
                ltDiagonal.gameObject.SetActive(true);
                regularText.transform.localPosition = new Vector3(-0.15f, 0.15f);

                goto HalfBlockCommon;
            case Type.LeftDownBlock:
                spriteRenderer.sprite = leftDownBlockSprite;
                breakableColliders = leftDownBlockCollider;
                ldDiagonal.gameObject.SetActive(true);
                regularText.transform.localPosition = new Vector3(-0.15f, -0.15f);

            HalfBlockCommon:
                shadow.enabled = false;
                bottom.BlockIsHalf = true;
                top.BlockIsHalf = true;
                right.BlockIsHalf = true;
                left.BlockIsHalf = true;
                ldDiagonal.BlockIsHalf = true;
                rdDiagonal.BlockIsHalf = true;
                ltDiagonal.BlockIsHalf = true;
                rtDiagonal.BlockIsHalf = true;
                
                bottom.gameObject.SetActive(false);
                top.gameObject.SetActive(false);
                right.gameObject.SetActive(false);
                left.gameObject.SetActive(false);

                regularText.enabled = true;
                LiveText = regularText;
                extension = extensions[1];
                break;
        }
        LiveText.text = Resistance + "";
        extension.Init();
//        DropParticles.startColor = blockColor;
//        explodeParticles.startColor = blockColor;
    }

    private void OnEnable()
    {
        // first of all we make sure that all collider of this block is disable. we will enable it with delay on blocks that we need. otherwise some weird bugs will happen.
        SetColliderDisable();
        
        // we really need to make collider enable with delay. if collider of block be active as soon as it instantiates sometimes some bugs happen.(collides to ground!!!)   
        Invoke("SetColliderEnable", 0.09f);
        
        _isCollide2FinishLine = false;
        GetComponent<Animator>().SetTrigger("Enabled");
        StartCoroutine(SetPosition());
    }

    IEnumerator SetPosition()
    {
        yield return new WaitForSeconds(0.1f);
        position = blockTransform.localPosition;
    }

    protected new void OnDisable()
    {
        base.OnDisable();
    }

    public GameObject lostPointPrefab;
    private bool _isCollide2FinishLine;
    public new void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        // layer 14 == Finish Line
        if (other.gameObject.layer == 14 && !_isCollide2FinishLine)
        {
            _isCollide2FinishLine = true;
            
            /* fixme: pooling here*********************************/
            GameObject go = Instantiate(lostPointPrefab,
                blockTransform.position,
                Quaternion.identity);
            
            LostingPoint lp = go.GetComponent<LostingPoint>();
            lp.Point = Resistance;
            /***********************************/
            SpecifyResistance(0);
            
            // i clear row obstacles array as soon as possible to let Board Cleaner (BoardManager.CleanUpEmptyRows()) clean board. this me to bombard rows in better way  
            blockTransform.parent = null;
            
            OnBlockReachedGround?.Invoke(Resistance);
        }
    }

    public new void SpecifyResistance(int r)
    {
        base.SpecifyResistance(r);
        if (Resistance == 1)
        {
            ResetBlock();
        }
        else if (Resistance < 1)
        {
            LiveText.enabled = false;
        }
    }

    public void ResetBlock()
    {
//        blockTransform.SetParent(row);
//        topTransform.SetParent(blockTransform);
//        bottomTransform.SetParent(blockTransform);
//        rightTransform.SetParent(blockTransform);
//        leftTransform.SetParent(blockTransform);
//
//        ldAnchorTransform.SetParent(blockTransform);
//        rdAnchorTransform.SetParent(blockTransform);
//        ltAnchorTransform.SetParent(blockTransform);
//        rtAnchorTransform.SetParent(blockTransform);
//        
//        blockTransform.localPosition = position;
//
//        topTransform.localPosition = new Vector2(0f, 0.5f);
//        bottomTransform.localPosition = new Vector2(0f, -0.5f);
//        leftTransform.localPosition = new Vector2(-0.5f, 0f);
//        rightTransform.localPosition = new Vector2(0.5f, 0f);

        switch (obstacleType)
        {
            case Type.LeftDownBlock:
                ldDiagonal.ResetScale();
                break;
            case Type.RightDownBlock:
                rdDiagonal.ResetScale();
                break;
            case Type.LeftTopBlock:
                ltDiagonal.ResetScale();
                break;
            case Type.RightTopBlock:
                rtDiagonal.ResetScale();
                break;
            default:
                top.ResetScale();
                break;
        }
    }

    public override void Explode()
    {
        OnExplode?.Invoke(Resistance);
        SpecifyResistance(0);
    }

    private new void SetColliderDisable()
    {
        base.SetColliderDisable();
        regularCollider.SetActive(false);
        verticalRowBombCollider.SetActive(false);
        horizontalColumnCollider.SetActive(false);
        rightTopBlockCollider  .SetActive(false) ;
        rightDownBlockCollider .SetActive(false) ;
        leftTopBlockCollider   .SetActive(false) ;
        leftDownBlockCollider  .SetActive(false) ;
        rtDiagonal.gameObject.SetActive(false);
        rdDiagonal.gameObject.SetActive(false);
        ltDiagonal.gameObject.SetActive(false);
        ldDiagonal.gameObject.SetActive(false);
        squareCollider.SetActive(false);
    }
    
    // this methods will be handy when bubble need to blow up due to a bomb explosion
    public void SetResistanceZeroWithRandomDelay(float maxDelay)
    {
        StartCoroutine(SetResistance2Zero(Random.Range(0f, maxDelay)));
    }

    IEnumerator SetResistance2Zero(float delay)
    {
        yield return new WaitForSeconds(delay);
        SpecifyResistance(0);
    }
}
