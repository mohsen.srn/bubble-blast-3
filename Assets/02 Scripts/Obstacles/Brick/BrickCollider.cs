﻿using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Obstacles;
using UnityEngine;

public class BrickCollider : MonoBehaviour
{
    public EmbeddedBrick brick;
    // The one and only purpose of this script is to transfer Collision an Trigger events to block script
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        brick.OnCollisionEnter2D(other);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        brick.OnTriggerEnter2D(other);
    }

    private void OnMouseDrag()
    {
        brick.OnMouseDrag();
    }

    private void OnMouseUp()
    {
        brick.OnMouseUp();
    }

    private void OnMouseDown()
    {
        brick.OnMouseDown();
    }
}
