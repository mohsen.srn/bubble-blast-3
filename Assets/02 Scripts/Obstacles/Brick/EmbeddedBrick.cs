﻿using _02_Scripts.ScriptableObjects;
using UnityEngine;

namespace _02_Scripts.Obstacles
{
    public class EmbeddedBrick : Breakable
    {
        public static event ObstacleActions OnCollide;
        public static event ObstacleActions OnBreak;
        private new void Awake()
        {
            base.Awake();
            // first of all we make sure that all collider of this block is disable. we will enable it with delay on blocks that we need. otherwise some weird bugs will happen.
            SetColliderDisable();
        }
        new void Start()
        {
            base.Start();
            // we really need to make collider enable with delay. if collider of block be active as soon as it instantiates sometimes some bugs happen.(collides to ground!!!)   
            Invoke("SetColliderEnable", 0.1f); 
        }

        public override void Eliminate()
        {
            base.Eliminate();
            
            OnBreak?.Invoke(0); // here argument is not important
            GetComponent<Explodable>().explode();
            GetComponent<ExplosionForce>().doExplosion(transform.position);
            breakableColliders.SetActive(false);
            Invoke("DestroyWIthDelay", 1f);
        }

        private void DestroyWIthDelay()
        {
            Destroy(gameObject);
        }
        
        public new void SpecifyResistance(int r)
        {
            base.SpecifyResistance(1);
        }
        
        public new void OnTriggerEnter2D(Collider2D other)
        {
            base.OnTriggerEnter2D(other);
        
            // layer 14 == Finish Line
            if (other.gameObject.layer == 14)
            {
                Eliminate();
            }
        }

        public new void OnCollisionEnter2D(Collision2D other)
        {
            base.OnCollisionEnter2D(other);
            OnCollide?.Invoke(0); // argument is not important
        }
        
        public override void Explode()
        {
            Eliminate();
        }
    }
}
