﻿using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Obstacles;
using UnityEngine;

public class HalfBrick : EmbeddedBrick
{
    public void MakeHalfBrick(Obstacle.Type type)
    {
        // we really need to make collider enable with delay. if collider of block be active as soon as it instantiates sometimes some bugs happen.(collides to ground!!!)   
        Invoke("SetColliderEnable", 0.09f);
        obstacleType = type;
        shadow.enabled = true;
        switch (type)
        {
            case Type.LeftDownBrick:
                obstacleTransform.Rotate(0f, 0f, 180f);
                break;
            case Type.LeftTopBrick:
                obstacleTransform.Rotate(0f, 0f, 90f);
                break;
            case Type.RightTopBrick:
                obstacleTransform.Rotate(0f, 0f, 0f);
                break;
            case Type.RightDownBrick:
                obstacleTransform.Rotate(0f, 0f, 270f);
                break;
        }
    }
}
