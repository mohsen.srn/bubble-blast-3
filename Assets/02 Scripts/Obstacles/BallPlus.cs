﻿using _02_Scripts;
using UnityEngine;

public class BallPlus : BallManipulator
{
    public BallPlusDrop dropBall;
    public ParticleSystem _ParticleSystem;
    public static event ObstacleActions OnCollide;

    public Sprite ballPlusShadow;
    private new void Start()
    {
        base.Start();
        dropBall._ballPlus = this;
        
        shadow.sprite = ballPlusShadow;
//        ObstacleType = Type.Ball;
    }

    private new void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        if (other.gameObject.layer == 8)
        {
            if (!Hit)
            {
//                _ParticleSystem.Play();

                Inactivate();
                SpecifyResistance(Resistance - 1);
                BallManager.GetInstance.takenBalls++;

                dropBall.dropTransform.parent = null;
                dropBall.dropTransform.localScale = new Vector3(1f, 1f, 1f);
                dropBall.gameObject.SetActive(true);

                OnCollide?.Invoke(0); // argument is not important
            }
        }
        else if (other.gameObject.layer == 14)
        {
            dropBall.dropTransform.parent = null;
            dropBall.dropTransform.localScale = new Vector3(1f, 1f, 1f);
            dropBall.gameObject.SetActive(true);
            
            // i clear row obstacles array as soon as possible to let Board Cleaner (BoardManager.CleanUpEmptyRows()) clean board. this me to bombard rows in better way  
            transform.parent = null;
            
            _ParticleSystem.Play();
            
            BallManager.GetInstance.BallNumbers += 1;
            
            Invoke("CallWithDelay", 0.1f);
        }
    }

    private void CallWithDelay()
    {
        PoolManager.Instance.Back2PlusPool(gameObject);
        dropBall.Jam();
    }

    private void OnEnable()
    {
        obstacleType = Type.Ball;
        dropBall.gameObject.SetActive(false);
        Visible = true;
        Activate();
    }
}
