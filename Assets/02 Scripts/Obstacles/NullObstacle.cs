﻿using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Obstacles;
using UnityEngine;

public class NullObstacle : Obstacle
{
    public override void Eliminate()
    {
        PoolManager.Instance.Back2NullPool(gameObject);
    }

    public new void SpecifyResistance(int r)
    {
        base.SpecifyResistance(r);
    }
}
