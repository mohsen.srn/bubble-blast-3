﻿using System;
using System.Collections;
using _02_Scripts;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class BallPlusDrop : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;

    private Transform _transform;
    public Transform dropTransform {
        get{
            if (_transform == null)
            {
                _transform = transform;
                return _transform;
            }

            return _transform;
        }
        private set
        {
            _transform = value;
            _transform = transform;
        }
    }
    
    public BallPlus _ballPlus;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _transform = transform;
    }

    private void OnEnable()
    {
        BallManager.OnBallStop += Jam;
        _ballPlus._ParticleSystem.Play();
        _rigidbody2D.AddForce(Vector2.up * 7f, ForceMode2D.Impulse);
    }

    private void OnDisable()
    {
        BallManager.OnBallStop -= Jam;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 11)
        {
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.gravityScale = 0f;
        }
    }

    public void Jam()
    {
        if (isActiveAndEnabled)
            StartCoroutine(MoveToCorrectPosition());
    }

    private Vector3 _target;
    private readonly float speed = -9;
    private Vector3 _dirNormalized;

    private IEnumerator MoveToCorrectPosition()
    {
        Vector3 destination = BallManager.GetInstance.ballDestinationPosition;
        // i use this variable and newXDiff, to check if ball passed the _target of not 
        float xDiff = destination.x - dropTransform.position.x;
        int avoidInfinityLoop = 0;
        while (Vector3.Distance(destination, dropTransform.position) >= 0.1f)
        {
            var position1 = dropTransform.position;
            var position = position1;
            try
            {
                _dirNormalized = (position - BallManager.GetInstance.ballDestinationPosition).normalized;
            }
            catch
            {
                break;
            }
            position = position + speed * Time.deltaTime * _dirNormalized;
            position1 = position;
            dropTransform.position = position1;

            float newXDiff = destination.x - position1.x;
            if (xDiff * newXDiff < 0)
            {
                // if sign of xDiff and newXDiff is different, so ball passed target 
                break;
            }

            if (avoidInfinityLoop++ > 40)
            {
                break;
            }

            yield return null;
        }

        dropTransform.position = destination;
        dropTransform.gameObject.SetActive(false);
        dropTransform.parent = _ballPlus.transform;
        dropTransform.localPosition = Vector3.zero;
    }
}
