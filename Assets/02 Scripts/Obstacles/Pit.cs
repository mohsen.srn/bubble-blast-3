﻿using UnityEngine;

namespace _02_Scripts.Obstacles
{
    public class Pit : BallManipulator
    {
        private new void Start()
        {
            base.Start();
            obstacleType = Type.Pit;
        }
        
        private new void OnTriggerEnter2D(Collider2D other)
        {
            base.OnTriggerEnter2D(other);
            if (other.gameObject.layer == 8)
                if (!Hit)
                {
                    Inactivate();
                    SpecifyResistance(Resistance - 1);
                    // todo: maybe later i should separate this from TakenBalls variable
                    BallManager.GetInstance.takenBalls--;
                    other.gameObject.GetComponent<Ball>().FellInPit();
                }
        }
    }
}
