﻿using System;
using _02_Scripts.ScriptableObjects;
using NaughtyAttributes;
using TMPro;
using UnityEngine;

namespace _02_Scripts.Obstacles
{
    public abstract class Obstacle : MonoBehaviour
    {
        // define a delegate to use it as event in obstacle hierarchy 
        public delegate void ObstacleActions(int a);

        public enum Type
        {
            Ordinary,
            BoxBomb,
            RowBomb,
            Null,
            Ball,
            Pit,
            EmbeddedBrick,
            ColumnBomb,
            LeftTopBrick, RightTopBrick, RightDownBrick, LeftDownBrick,
            LeftTopBlock, RightTopBlock, RightDownBlock, LeftDownBlock,
            Sponge
        }

        public TextMeshPro LiveText;

        // use this variable to update metaData of this obstacle
        public StageObstacle obstacleMetaData;

        [ReadOnly] public Type obstacleType;

        [SerializeField] private int resistance = 1;

        public SpriteRenderer shadow;

        // from out side this class no one can set resistance
        public int Resistance => resistance;

        public Row row;

        public bool Visible { get; set; } = true;

        public Transform obstacleTransform;

        protected void Awake()
        {
            obstacleTransform = transform;
        }

        protected void Start()
        {
            SpecifyResistance(resistance);
        }
        
        public abstract void Eliminate();

        public void SpecifyResistance(int r)
        {
            // this "if" checks does this obstacle inserted to pool before or not.
            // now even in codes we set resistance to 0, multiple times, only one time obstacles Insert to pool
            if (r == 0 && resistance == 0)
                return;
            
            resistance = r;
            LiveText.text = resistance.ToString();
            
            if (resistance == 0)
            {
                LiveText.enabled = false;
            }
            
            if (resistance == 0 && obstacleType != Type.EmbeddedBrick) // i don't want to eliminate Brick
                Eliminate();
            else if (resistance == 1 && (obstacleType == Type.Pit || obstacleType == Type.Ball))
            {
                LiveText.enabled = false;
                Visible = true;
            }
            else
            {
                LiveText.enabled = true;
                Visible = true;
            }
        }

        protected void OnCollisionEnter2D(Collision2D other)
        {
            if(obstacleType != Type.EmbeddedBrick)
                // this line of code makes sure all Obstacles reset trap counter(except unbreakable)
                other.gameObject.GetComponent<Ball>()?.trap.HitBlock(); 
            else if (obstacleType == Type.EmbeddedBrick)
            {
                other.gameObject.GetComponent<Ball>()?.trap.HitUnbreakableObstacle(this);
            }
        }

        protected void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer == 8 && obstacleType != Type.EmbeddedBrick)
                // this line of code makes sure all Obstacles reset trap counter(except unbreakable)
                other.gameObject.GetComponent<Ball>().trap.HitBlock();
        }

        protected void OnDisable()
        {
            obstacleType = Type.Null;
        }

        public void OnMouseDown()
        {
            Controller.GetInstance.OnMouseDown();
        }

        public void OnMouseDrag()
        {
            Controller.GetInstance.OnMouseDrag();
        }

        public void OnMouseUp()
        {
            Controller.GetInstance.OnMouseUp();
        }

        public void SetOwnerRow(Row parent)
        {
            row = parent;
            obstacleTransform.parent = parent.rowTransform;
        }
    }
}
