﻿using System.Collections;
using _02_Scripts;
using _02_Scripts.Obstacles;
using UnityEngine;
using UnityEngine.UI;

public abstract class BallManipulator : Obstacle
{
    public Sprite ActiveSprite, InactiveSprite;
    
    [Header("Properties: ")] public float ColliderRadiusRate = 0.5f;

    [Header("Debug:")] public bool Hit;

    protected new void Start()
    {
        base.Start();
        Visible = true;
        // if BallManager stopped, activate
        BallManager.OnBallStop += Activate;
    }

    private void OnDestroy()
    {
        BallManager.OnBallStop -= Activate;
    }

    public override void Eliminate()
    {
        Visible = false;
        obstacleType = Type.Null;
        
        // this command cleans up row from 0 resistance obstacles
        row.CleanUpRow();

        StartCoroutine(PushToPoolWithDelay());
    }

    IEnumerator PushToPoolWithDelay()
    {
        yield return new WaitForSeconds(0.5f);
        PoolManager.Instance.Back2PlusPool(gameObject);
    }

    protected void Activate()
    {
        if (Visible)
        {
            Hit = false;
            GetComponent<SpriteRenderer>().sprite = ActiveSprite;
            GetComponent<CircleCollider2D>().enabled = true;
            shadow.enabled = true;
        }
    }

    protected void Inactivate()
    {
        Hit = true;
        GetComponent<SpriteRenderer>().sprite = InactiveSprite;
        GetComponent<CircleCollider2D>().enabled = false;
        shadow.enabled = false;
    }
}
