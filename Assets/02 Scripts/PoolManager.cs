﻿using System.Collections.Generic;
using _02_Scripts;
using _02_Scripts.Obstacles;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public delegate void PoolActions();

    public static PoolActions OnGetBall;
    public static PoolActions OnBackBall2Pool;

    // Make class as singleton
    public static PoolManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    [Header("Assets")] public Transform ballPoolHolder;
    public Transform ballPlusPoolHolder;
    public Transform blockPoolHolder;
    public Transform rowPoolHolder;
    public Transform NullPoolHolder;
    public Transform DotPoolHolder;

    public GameObject ballPrefab;
    public GameObject blockPrefab;
    public GameObject ballPlusPrefab;
    public GameObject rowPrefab;
    public GameObject NullPrefab;
    public GameObject DotPrefab;

    [Header("Properties")] public int initPoolSize = 20;
    public int increasePoolSize = 10;

    private Queue<Ball> _ballPool = new Queue<Ball>();
    private Queue<Block> _blockPool = new Queue<Block>();
    private Queue<GameObject> _BallPlusPool = new Queue<GameObject>();
    private Queue<Row> _RowPool = new Queue<Row>();
    private Queue<GameObject> _NullPool = new Queue<GameObject>();
    private Queue<Transform> _dotPool = new Queue<Transform>();

    private void Start()
    {
        InitPool();
    }

    private void InitPool()
    {
        for (int i = 0; i < initPoolSize; i++)
        {
            GameObject ball = Instantiate(ballPrefab, ballPoolHolder);
            ball.SetActive(false);
            Ball bBall = ball.GetComponent<Ball>();
            _ballPool.Enqueue(bBall);
        }

        for (int i = 0; i < initPoolSize; i++)
        {
            GameObject block = Instantiate(blockPrefab, blockPoolHolder);
            block.SetActive(false);
            _blockPool.Enqueue(block.GetComponent<Block>());
        }

        for (int i = 0; i < initPoolSize; i++)
        {
            GameObject ballPlus = Instantiate(this.ballPlusPrefab, ballPlusPoolHolder);
            ballPlus.SetActive(false);
            _BallPlusPool.Enqueue(ballPlus);
        }

        for (int i = 0; i < initPoolSize; i++)
        {
            GameObject nullBlock = Instantiate(NullPrefab, NullPoolHolder);
            nullBlock.SetActive(false);
            _NullPool.Enqueue(nullBlock);
        }

        for (int i = 0; i < 10; i++)
        {
            GameObject row = Instantiate(rowPrefab, rowPoolHolder);
            row.SetActive(false);
            _RowPool.Enqueue(row.GetComponent<Row>());
        }

        for (int i = 0; i < initPoolSize; i++)
        {
            GameObject dot = Instantiate(DotPrefab, DotPoolHolder);
            Transform dotTransform = dot.transform;
//            dotTransform.localScale = Vector3.one * DottedLine.Instance.Size;
            dot.SetActive(false);
            _dotPool.Enqueue(dotTransform);
        }
    }

    // this variable is for debugging. later i will remove this
    public int BlockQueue = 0;

    /****************** Block **********************/
    // this method will call by block to set itself to pool
    public void Back2BlockPool(Block block2Back)
    {
        if (block2Back.isActiveAndEnabled)
        {
            block2Back.blockGameObject.SetActive(false);
            block2Back.spriteRenderer.sortingOrder = 0;
            Transform blockTransform = block2Back.blockTransform;
            blockTransform.parent = blockPoolHolder;
            blockTransform.localPosition = Vector3.zero;
            _blockPool.Enqueue(block2Back);
            BlockQueue = _blockPool.Count;
        }
    }

    public Block GetBlock(Row parentRow, int resistance, Obstacle.Type objType)
    {
        if (_blockPool.Count > 0)
        {
            Block block = _blockPool.Dequeue();
            block.row = parentRow;
            block.blockTransform.parent = parentRow.rowTransform;
            block.MakeBlock(objType);
            block.SpecifyResistance(resistance);
            block.blockGameObject.SetActive(true);
            BlockQueue = _blockPool.Count;
            return block;
        }
        else
        {
            AddNewBlocksToPool();
            return GetBlock(parentRow, resistance, objType);
        }
    }

    private void AddNewBlocksToPool()
    {
        for (int i = 0; i < increasePoolSize; i++)
        {
            GameObject block = Instantiate(blockPrefab, blockPoolHolder);
            block.SetActive(false);
            _blockPool.Enqueue(block.GetComponent<Block>());
        }

        BlockQueue = _blockPool.Count;
    }

/**************************** Ball ****************************/
    public Ball GetBall()
    {
        if (_ballPool.Count > 0)
        {
            OnGetBall?.Invoke();
            Ball ball = _ballPool.Dequeue();
            ball.gameObject.SetActive(true);

            BallItemType bit = (BallItemType) PlayerPrefs.GetInt("SelectedBall");
            ball.SetBallType(bit);

            return ball;
        }
        else
        {
            // create new ball and add to pool and return this ball
            AddNewBallsToPool();
            return GetBall();
        }
    }

    private void AddNewBallsToPool()
    {
        for (int i = 0; i < increasePoolSize; i++)
        {
            GameObject ball = Instantiate(ballPrefab, ballPoolHolder);
            ball.SetActive(false);
            Ball bBall = ball.GetComponent<Ball>();
//            print("PoolManager.AddNewBallsToPool() -> Ball position = " + bBall.ballTransform.position);
            _ballPool.Enqueue(bBall);
        }
    }

    // this method will call by ball to set itself to pool
    public void Back2BallPool(Ball ball2Back)
    {
        OnBackBall2Pool?.Invoke();
        GameObject o = ball2Back.gameObject;
        o.SetActive(false);
        o.layer = 8;
        ball2Back.ballRigidbody2D.mass = 0f;
        _ballPool.Enqueue(ball2Back);
    }

    /*********************** Ball plus ********************************/
    public GameObject GetBallPlus()
    {
        if (_BallPlusPool.Count > 0)
        {
            GameObject plus = _BallPlusPool.Dequeue();
            plus.SetActive(true);
            return plus;
        }
        else
        {
            AddNewPlusToPool();
            return GetBallPlus();
        }
    }

    private void AddNewPlusToPool()
    {
        for (int i = 0; i < increasePoolSize; i++)
        {
            GameObject plus = Instantiate(ballPlusPrefab, ballPlusPoolHolder);
            plus.SetActive(false);
            _BallPlusPool.Enqueue(plus);
        }
    }

    // this method will call by ballPlus to set itself to pool
    public void Back2PlusPool(GameObject Plus2Back)
    {
        Plus2Back.SetActive(false);
        Transform pTransform = Plus2Back.transform;
        pTransform.parent = ballPlusPoolHolder;
        pTransform.localPosition = Vector3.zero;

        _BallPlusPool.Enqueue(Plus2Back);
    }

/****************************** Dot ************************************/

    public Transform GetDot(Vector3 scale, Vector3 position)
    {
        if (_dotPool.Count > 0)
        {
            Transform dot = _dotPool.Dequeue();
            dot.localScale = scale;
            dot.position = position;
            dot.gameObject.SetActive(true);
            return dot;
        }
        else
        {
            // create new dot and add to pool and return this dot
            AddNewDotsToPool();
            return GetDot(scale, position);
        }
    }

    private void AddNewDotsToPool()
    {
        for (int i = 0; i < 3; i++)
        {
            GameObject dot = Instantiate(DotPrefab, DotPoolHolder);
            Transform dotTransform = dot.transform;
//            dotTransform.localScale = Vector3.one * DottedLine.Instance.Size;
            dot.SetActive(false);
            _dotPool.Enqueue(dotTransform);
        }
    }

    public void Back2DotPool(Transform dot2Back)
    {
        dot2Back.gameObject.SetActive(false);
        dot2Back.parent = DotPoolHolder;
        _dotPool.Enqueue(dot2Back);
    }

    /***************************** Null ***********************************/

    public GameObject GetNull()
    {
        if (_NullPool.Count > 0)
        {
            GameObject nullB = _NullPool.Dequeue();
            nullB.GetComponent<NullObstacle>().SpecifyResistance(1);
            nullB.SetActive(true);
            return nullB;
        }
        else
        {
            AddNewNullsToPool();
            return GetNull();
        }
    }

    private void AddNewNullsToPool()
    {
        for (int i = 0; i < increasePoolSize; i++)
        {
            GameObject nullB = Instantiate(NullPrefab, NullPoolHolder);
            nullB.SetActive(false);
            _NullPool.Enqueue(nullB);
        }
    }

    public void Back2NullPool(GameObject null2Back)
    {
        null2Back.SetActive(false);
        null2Back.transform.parent = NullPoolHolder;
        _NullPool.Enqueue(null2Back);
    }

    /*************************** Row ********************************/

    public Row GetRow()
    {
        if (_RowPool.Count > 0)
        {
            Row row = _RowPool.Dequeue();
            row.rowGameObject.SetActive(true);
            return row;
        }
        else
        {
            AddNewRowToPool();
            return GetRow();
        }
    }

    private void AddNewRowToPool()
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject row = Instantiate(rowPrefab, rowPoolHolder);
            row.SetActive(false);
            _RowPool.Enqueue(row.GetComponent<Row>());
        }
    }

    public void Back2RowPool(Row row2Back)
    {
        row2Back.rowGameObject.SetActive(false);
        row2Back.rowTransform.parent = rowPoolHolder;

        _RowPool.Enqueue(row2Back);
    }
}