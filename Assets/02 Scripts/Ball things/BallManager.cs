﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using _02_Scripts._02_Stage;
using _02_Scripts.Helper_Classes;
using Firebase.Analytics;
using NaughtyAttributes;
using TMPro;
using UnityEngine;

namespace _02_Scripts
{
    public class BallManager : MonoBehaviour
    {
        // define event to understand when ball stop and other thinks can to appropriate things as soon as balls stop
        public delegate void BallActions();

        public delegate void BallFlyingActions(int allBalls, int flyingBalls);

        public delegate void BallActionWithPosition(Vector3 position);

        [Header("Properties:")] [BoxGroup("CheckOnBug")]
        public int ballNumbers = 1;

        public Vector3 ballDestinationPosition;

        public bool doesFirstDropHappened;

        [BoxGroup("CheckOnBug")]
        public bool doesAllBallShot; // this is for check does all balls shot. if it does not never stop ballManager

        [BoxGroup("CheckOnBug")] public int FlyingBalls;

        [Header("Debug Mode:")] public float interVal = 0.5f;
        public bool isBusy;
        [BoxGroup("CheckOnBug")] public int takenBalls;
        public SpriteRenderer spriteRenderer;
        public static BallManager GetInstance { get; private set; }

        public static event BallActions OnBallStop;
        public static event BallActions OnBallStart;
        public static event BallActions OnPause;
        public static event BallActionWithPosition OnGatherCalled;
        public static event BallFlyingActions OnBallNumberStatusChanged;

        public TextMeshPro ballNumberIndicator;

        // i use ballSprite to never let balls on ground disappear
        public GameObject ballSprite;
        public Transform ballManagerTransform;
        public Transform GroundTransform;

        private void Awake()
        {
            if (GetInstance == null)
            {
                GetInstance = this;
//                DontDestroyOnLoad(this);
            }
            else if (GetInstance != this)
            {
                Destroy(this);
            }
        }

        private void OnDestroy()
        {
            OnPause?.Invoke();
            WinManager.OnGameFinish -= GameFinished;
            GetInstance = null;
        }

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            // instead of calling this function with delay, i call this in appro
//            Invoke("InitWithDelay", 0.2f);

            doesFirstDropHappened = false;
            isBusy = false;
            doesAllBallShot = false;
            ballManagerTransform = transform;
            ballSprite.transform.parent = null;
            ballSprite.SetActive(false);

            PoolManager.OnGetBall += IncreaseFlyingBalls;
            PoolManager.OnBackBall2Pool += DecreaseFlyingBalls;

            WinManager.OnGameFinish += GameFinished;
        }

        public void Init()
        {
            Start();
        }

        public void InitWithDelay()
        {
            ballDestinationPosition = transform.position; //ballManagerTransform.position;
            _launcherPos = ballDestinationPosition;
        }

        public void Shoot(Vector2 direction)
        {
            // this "if" prevents bug related this error:
            // "Rigidbody2D.velocity assign attempt for 'Ball(Clone)' is not valid. Input velocity is { NaN, NaN }."
            if (direction.ToString() == "(NaN, NaN)")
            {
                return;
            }

            // when we start shooting, set jam to false to make sure ballManager can create balls.
            _jam = false;

            // do not create new balls and shoot, when previews balls still alive
            if (isBusy)
                return;

            ballSprite.SetActive(false);
            // if event is not null, invoke the methods 
            OnBallStart?.Invoke();
            GetComponent<SpriteRenderer>().enabled = true;
            StartCoroutine(CreateBall(direction));
        }

        private IEnumerator CreateBall(Vector2 direction)
        {
            isBusy = true;
            doesAllBallShot = false;
            var interval = interVal;

            spriteRenderer.enabled = false;

            for (var i = 0; i < ballNumbers; i++)
            {
                // if player pressed Jam Btn to gather all flying balls, this "if" does not allow ballManager create more balls 
                if (_jam)
                {
                    _jam = false;
                    doesAllBallShot = true;
                    yield break;
                }

                OnBallNumberStatusChanged?.Invoke(ballNumbers - i - 1, FlyingBalls);

//                IncreaseFlyingBalls();
                var ball = PoolManager.Instance.GetBall();
                ball.Shoot(direction);

                // Below commented codes for shooting balls with different intervals. we do not want this for now
                /*if (interval > 0.06f)
                interval /= i + 1;
            else*/
                interval = 0.05f;

                if (ballNumbers == 1)
                    yield return null;
                else if (ballNumbers < 100)
                    yield return new WaitForSeconds(0.05f);
                else
                    yield return null;
            }

            doesAllBallShot = true;
        }

        // this method should be call, only when all ball are dropped
        private void Stop()
        {
            gameObject.layer = 0;

            doesFirstDropHappened = false;
            doesAllBallShot = false;
            BallNumbers += takenBalls;
            takenBalls = 0;
            // when last ball drops. this method will call.
            // so we specify position of LaunchPos to ballManagers position
            _launcherPos = ballDestinationPosition;
            // if event is not null, invoke the methods
            OnBallStop?.Invoke();
            isBusy = false;
        }

        // Increase and Deacrease of Flying Balls only will issue on poolManager
        public void IncreaseFlyingBalls()
        {
            FlyingBalls++;
            ballNumberIndicator.text = ballNumbers - FlyingBalls + "";
            OnBallNumberStatusChanged?.Invoke(-1, FlyingBalls);
        }

        private void DecreaseFlyingBalls()
        {
            FlyingBalls--;
            ballNumberIndicator.text = ballNumbers - FlyingBalls + "";
            // if after deceasing flying ball, it equals zero
            if (FlyingBalls == 0 && doesAllBallShot) Stop();

            OnBallNumberStatusChanged?.Invoke(-1, FlyingBalls);
        }

        public int BallNumbers
        {
            get => ballNumbers;
            set
            {
                ballNumbers = value;
                ballNumberIndicator.text = ballNumbers + "";
                OnBallNumberStatusChanged?.Invoke(ballNumbers, FlyingBalls);
            }
        }

        /// this method gives balls that just want shoot a launch position.
        /// this launch position is position of ballManager when shooting balls begins 
        public Vector3 GetLauncherPosition()
        {
//            print("BallManager.GetLauncherPosition() - > _launcherPos = " + _launcherPos);
            return _launcherPos;
        }

        private Vector3 _launcherPos;

        public void SetPosition(Vector3 vector3)
        {
            _launcherPos = ballManagerTransform.position;

            ballSprite.SetActive(true);
            ballSprite.transform.position = vector3;
            StartCoroutine(Move(vector3));

            ballDestinationPosition = vector3;
        }

        private IEnumerator Move(Vector3 newPosition)
        {
            // i use this variable and newXDiff, to check if ball passed the _target of not 
            float xDiff = newPosition.x - ballManagerTransform.position.x;
            int avoidInfinityLoop = 0;
            while (Vector3.Distance(newPosition, ballManagerTransform.position) >= 0.1f)
            {
                var position1 = ballManagerTransform.position;
                var position = position1;
                Vector3 _dirNormalized = (position - newPosition).normalized;
                position = position + -6 * Time.deltaTime * _dirNormalized;
                position1 = position;
                ballManagerTransform.position = position1;
                float newXDiff = newPosition.x - position1.x;
                if (xDiff * newXDiff < 0)
                {
                    // if sign of xDiff and newXDiff is different, so ball passed target 
                    break;
                }

                if (avoidInfinityLoop++ > 100)
                {
                    break;
                }

                yield return null;
            }

            // this line of code is for circumventing a very nasty _bug!. 
            if (Math.Abs(ballManagerTransform.position.y - (GroundTransform.position.y + 0.7f)) > 0.01f)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine(
                    "BallManager.Move(Vector3 newPosition) -> An Error occured but solved properly.");
                stringBuilder.AppendLine("ballManagerTransform.position.y = " + ballManagerTransform.position.y);
                stringBuilder.AppendLine(" /// GroundTransform.position.y = " + GroundTransform.position.y);
                stringBuilder.AppendLine(
                    "/// GroundTransform.position.y + 0.7 = " + (GroundTransform.position.y + 0.7f));
                // here i only check if this inappropriate situation is met. so only we notify on log 
                print(stringBuilder);
                
                FirebaseAnalytics.LogEvent("Bug in BallManager.IEnumerator Move(Vector3 newPosition)");
            }

            // here we set y of ballManager to prevent bad results in some rare situations
            ballManagerTransform.position = new Vector3(newPosition.x,
                GroundTransform.position.y + 0.7f, newPosition.z);

            spriteRenderer.enabled = true;
        }

        // boolean and method to control Ball gathering process
        private bool _jam;

        private void GameFinished(GameResult gameResult)
        {
            CallBallsGetToGather();
        }

        public void CallBallsGetToGather()
        {
            _jam = true; // to prevent ballManager to shoot
            OnGatherCalled?.Invoke(ballDestinationPosition);
        }
    }
}