using System;
using UnityEngine;

namespace _02_Scripts
{
    [Serializable]
    public class BallTypeAssets
    {
        public BallItemType type;
        public Sprite sprite;
        public GameObject trail;
        public float velocity;
    }
}
