﻿using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts;
using NaughtyAttributes;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum Status
    {
        FLYING,
        ON_GROUND,
        IN_PIT
    }

    [ReadOnly] public Rigidbody2D ballRigidbody2D;
    [ReadOnly] public Status status;
    [ReadOnly] public TrapDetector trap;
    [ReadOnly] public Transform ballTransform;
    [ReadOnly] private SpriteRenderer _spriteRenderer;

    private BallItemType _ballType;
 
    [BoxGroup("Ball Speed Settings")] public int speedTimeLimit = 5;
    [BoxGroup("Ball Speed Settings")] public float velocityCoefficient = 1.2f;
    [BoxGroup("Ball Speed Settings")] public float initialBallVelocity = 1;

    [Header("Ball types assets")]
    public List<BallTypeAssets> ballTypesAssets;

    private void Awake()
    {
        ballRigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        ballTransform = transform;
    }

    private void Start()
    {
        trap = new TrapDetector(gameObject);

        BallManager.OnPause += Reset;
        BallManager.OnGatherCalled += Jam;
    }

    /** 
     * This method transforms this ball to ballManager position that is parent of all ball.
     */
    public void Reset()
    {
        if (ballTransform == null)
            return;
        ballTransform.position =
            BallManager.GetInstance.transform.position;
        ballRigidbody2D.velocity = Vector2.zero;
        if (isActiveAndEnabled)
        {
            PoolManager.Instance.Back2BallPool(this);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == 19)
        {
            Stop();
        }
    }

    private void Stop()
    {
        gameObject.layer = 8;

        ballRigidbody2D.velocity = Vector2.zero;
        ballRigidbody2D.gravityScale = 0f; // it makes sure everything is ok when ball drops on ground
        ballRigidbody2D.mass = 0f;

        // Make ball y position to ballManagers y position
        var transform1 = transform;
        transform1.position = new Vector2(transform1.position.x,
            BallManager.GetInstance.ballManagerTransform.position.y);

        status = Status.ON_GROUND;

        if (!BallManager.GetInstance.doesFirstDropHappened)
        {
            BallManager.GetInstance.doesFirstDropHappened = true;

            var position = ballTransform.position;
            BallManager.GetInstance.SetPosition(new Vector3(position.x, position.y));

//            BallManager.GetInstance.DecreaseFlyingBalls();
            Disable();
        }
        else
        {
            _target = BallManager.GetInstance.ballDestinationPosition;
            StartCoroutine(MoveToCorrectPosition());
        }
    }

    private Vector3 _target;
    private readonly float speed = -9;
    private Vector3 _dirNormalized;

    private IEnumerator MoveToCorrectPosition()
    {
        // i use this variable and newXDiff, to check if ball passed the _target or not 
        float xDiff = _target.x - ballTransform.position.x;
        int avoidInfinityLoop = 0;
        while (Vector3.Distance(_target, ballTransform.position) >= 0.1f)
        {
            var position = ballTransform.position;
            _dirNormalized = (position - BallManager.GetInstance.ballDestinationPosition).normalized;
            position = position + speed * Time.deltaTime * _dirNormalized;
            ballTransform.position = position;

            float newXDiff = _target.x - position.x;
            if (xDiff * newXDiff < 0)
            {
                // if sign of xDiff and newXDiff is different, so ball passed target 
                break;
            }

            if (avoidInfinityLoop++ > 50)
            {
                break;
            }

            yield return null;
        }

        ballTransform.position = _target;
//        Invoke("Disable", 0.5f);
//        BallManager.GetInstance.DecreaseFlyingBalls();
        Disable();
    }

    // this method every _speedTimeLimit seconds multiplies velocity of ball by 'velocityCoefficient'
    private IEnumerator GiveSpeedWhenTimeExceed()
    {
        while (true)
        {
            yield return new WaitForSeconds(speedTimeLimit);

            var velocity = ballRigidbody2D.velocity;
            velocity *= velocityCoefficient;
            ballRigidbody2D.velocity = velocity;

            if (ballRigidbody2D.velocity == Vector2.zero && status != Status.ON_GROUND)
                ballRigidbody2D.velocity = Vector2.down * 15;
        }
    }

    // never let a velocity of a ball fall to below a speed
    IEnumerator PreventSpeedFallDown()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            Vector2 velocity = ballRigidbody2D.velocity;
            if (Mathf.Abs(velocity.x) < 10 &&
                Mathf.Abs(velocity.y) < 10)
            {
                ballRigidbody2D.velocity = velocity.normalized * 15;
            }
        }
    }

    public void FellInPit()
    {
        status = Status.IN_PIT;
//        _ballManager.DecreaseFlyingBalls();
        Disable();
    }

    private void OnEnable()
    {
        status = Status.FLYING;
        /*if (ballTransform == null)
        {
            ballTransform = transform;
        }*/

        try
        { // instead of if(BallManager.GetInstance == null) return;

            ballTransform.position = BallManager.GetInstance.GetLauncherPosition();
        }
        catch
        {
//            print("Ball.OnEnable() => Inside catch -> BallManager.GetInstance == null //// Ball position = " + ballTransform.position);

            return;
        }
        StartCoroutine("GiveSpeedWhenTimeExceed");
        StartCoroutine("PreventSpeedFallDown");
    }

    private void Disable()
    {
//        Invoke("Inactivate", 0.5f);
        StartCoroutine(Inactivate(0.5f));
        trap.HitGround();
        StopCoroutine("GiveSpeedWhenTimeExceed");
        StopCoroutine("PreventSpeedFallDown");
    }

    IEnumerator Inactivate(float delay)
    {
        yield return new WaitForSeconds(delay);
        PoolManager.Instance.Back2BallPool(this);
    }

    /// <summary>
    ///    
    /// </summary>
    /// <param name="target">it does not matter for now</param>
    private void Jam(Vector3 target)
    {
        if (isActiveAndEnabled)
        {
            var direction = target - ballTransform.position;
            ballRigidbody2D.velocity = Vector3.zero;
            status = Status.ON_GROUND;
            // change layer to a layer that we now do not collide to block. on stop we will change it
            gameObject.layer = 16;

            StartCoroutine(MoveToCorrectPosition(target));
        }
    }

    private IEnumerator MoveToCorrectPosition(Vector3 target)
    {
        // i use this variable and newXDiff, to check if ball passed the _target or not 
        var xDiff = target - ballTransform.position;
        int avoidInfinityLoop = 0;
        while (Vector3.Distance(_target, ballTransform.position) >= 0.1f)
        {
            var position = ballTransform.position;
            _dirNormalized = (position - BallManager.GetInstance.ballDestinationPosition).normalized;
            position = position + (speed *2) * Time.deltaTime * _dirNormalized;
            ballTransform.position = position;

            var newXDiff = target - position;
            if (xDiff.x * newXDiff.x < 0 && xDiff.y * newXDiff.y < 0)
            {
                // if sign of xDiff and newXDiff is different, so ball passed target 
                break;
            }

            if (avoidInfinityLoop++ > 50)
            {
                break;
            }

            yield return null;
        }

        ballTransform.position = target;
//        Invoke("Disable", 0.5f);
//        BallManager.GetInstance.DecreaseFlyingBalls();
        Stop();
        Disable();
    }

    public void Shoot(Vector3 dir)
    {
//        print("Ball.Shoot() -> Ball position = " + ballTransform.position);
        ballRigidbody2D.velocity = dir * initialBallVelocity;
    }

    // in fact this is a setter for type of ball
    public void SetBallType(BallItemType type)
    {
        if(_ballType != type)
        {
            _ballType = type;
            foreach (BallTypeAssets typeAsset in ballTypesAssets)
            {
                if (typeAsset.type == type)
                {
                    _spriteRenderer.sprite = typeAsset.sprite;
                    typeAsset.trail.SetActive(true);
                    initialBallVelocity = typeAsset.velocity;

                    BallManager.GetInstance.spriteRenderer.sprite = typeAsset.sprite;
                }
                else
                {
                    typeAsset.trail.SetActive(false);
                }
            }
        }
    }
}
