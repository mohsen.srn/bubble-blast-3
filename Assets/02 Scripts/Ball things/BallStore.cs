using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using _02_Scripts;
using UnityBackStack;
using UnityEngine;

public class BallStore : MonoBehaviour_BackStackSupport
{
    /************************* Singleton ************************/
    public static BallStore GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    /****************************************************************/

    public GameObject ballStoreDialog;
    public GameObject darkOverlay;

    public Animator _animator;

    public List<BallStoreSpecifications> ballStoreSpecifications;
    public List<BallStoreItem> BallStoreItems;
    
    // what is selected/activated ball type to play
    // what is

    private void Start()
    {
        InitStatuses();
        CloseBallStoreDialog();
        _animator = GetComponent<Animator>();
//        ballStoreDialog.SetActive(false);
    }

    public void OpenBallStoreDialog()
    {
        ballStoreDialog.SetActive(true);
        darkOverlay.SetActive(true);
        _animator.ResetTrigger("enter");
        _animator.ResetTrigger("exit");

        _animator.SetTrigger("enter");
        BackStackManager.GetInstance.Push2BackStack(this);
    }

    public void CloseBallStoreDialog()
    {
        StartCoroutine(DisableWithDelay());

        _animator.ResetTrigger("exit");
        _animator.ResetTrigger("enter");

        _animator.SetTrigger("exit");
        darkOverlay.SetActive(false);

    }

    IEnumerator DisableWithDelay()
    {
        yield return new WaitForSeconds(1f);
        ballStoreDialog.SetActive(false);
    }

    public override void BackCalled()
    {
        CloseBallStoreDialog();
    }

    public void SelectBall(int index)
    {
        if (ballStoreSpecifications[index].status == BallStoreItemStatus.Unlocked)
        {
            // The ball in Unlocked
            PlayerPrefs.SetInt("SelectedBall", (int)ballStoreSpecifications[index].ballType);
            PlayerPrefs.Save();

            foreach (BallStoreItem ballStoreItem in BallStoreItems)
            {
                if (ballStoreSpecifications[ballStoreItem.index].status 
                    == BallStoreItemStatus.Unlocked && ballStoreItem.index != index)
                {
                    ballStoreItem.MakeBtnUnSelect();
                }
            }
        }
        else
        {
            int price = ballStoreSpecifications[index].price;
            
            CoinManager.GetInstance.DecreaseCoins(price, 
            () =>
            {
                ballStoreSpecifications[index].status = BallStoreItemStatus.Unlocked;
                PlayerPrefs.SetInt(ballStoreSpecifications[index].ballType.ToString(),
                    (int)BallStoreItemStatus.Unlocked);
                
                PlayerPrefs.SetInt("SelectedBall", (int)ballStoreSpecifications[index].ballType);
                PlayerPrefs.Save();
                
                BallStoreItems[index].MakeBtnSelected();
                
                foreach (BallStoreItem ballStoreItem in BallStoreItems)
                {
                    if (ballStoreSpecifications[ballStoreItem.index].status 
                        == BallStoreItemStatus.Unlocked && ballStoreItem.index != index)
                    {
                        ballStoreItem.MakeBtnUnSelect();
                    }
                }
            },
            () =>
            {
                // failed. so noThing happens
            });
        }
    }

    private void InitStatuses()
    {
        for (int i = 0; i < ballStoreSpecifications.Count; i++)
        {
            BallStoreItems[i].price.text = ballStoreSpecifications[i].price.ToString();
        }
        
        // if game just started
        if (!PlayerPrefs.HasKey(ballStoreSpecifications[0].ballType.ToString()))
        {
            foreach (BallStoreSpecifications ballStoreItem in ballStoreSpecifications)
            {
                PlayerPrefs.SetInt(ballStoreItem.ballType.ToString(),
                    (int)ballStoreItem.status);
                PlayerPrefs.Save();
            }
        }
        else
        {
            foreach (BallStoreSpecifications specification in ballStoreSpecifications)
            {
                specification.status = (BallStoreItemStatus)
                    PlayerPrefs.GetInt(specification.ballType.ToString());
            }
        }
    }
}
