using System;
using System.Collections;
using RTLTMPro;
using UnityEngine;
using UnityEngine.UI;

public class BallStoreItem : MonoBehaviour
{
    public int index;
    public Image ballImage;
    public RTLTextMeshPro price;
    public GameObject coinsImage;

    public Sprite lockedBtnSprite, selectedBtnSprite, unSelectedBtnSprite;

    private Image _image;

    private void OnEnable()
    {
        _image = GetComponent<Image>();
        StartCoroutine(ExecuteWithDelay());
    }

    IEnumerator ExecuteWithDelay()
    {
        yield return new WaitForSeconds(0.1f);
        if (BallStore.GetInstance.ballStoreSpecifications[index].status == BallStoreItemStatus.Locked)
        {
            _image.sprite = lockedBtnSprite;
            ballImage.gameObject.SetActive(false);
            coinsImage.SetActive(true);
            price.gameObject.SetActive(true);
        }
        else
        {
            ballImage.gameObject.SetActive(true);
            coinsImage.SetActive(false);
            price.gameObject.SetActive(false);

            if (PlayerPrefs.HasKey("SelectedBall"))
            {
                BallItemType bit = (BallItemType) PlayerPrefs.GetInt("SelectedBall");

                _image.sprite = BallStore.GetInstance.ballStoreSpecifications[index].ballType
                                == bit
                    ? selectedBtnSprite
                    : unSelectedBtnSprite;
            }
            else
            {
                _image.sprite = unSelectedBtnSprite;
            }
        }
    }

    // when user clicks on ball btn, this method will call
    public void Select()
    {
        BallStore.GetInstance.SelectBall(index);

        if (BallStore.GetInstance.ballStoreSpecifications[index].status == BallStoreItemStatus.Unlocked)
        {
            _image.sprite = selectedBtnSprite;
        }
    }

    public void MakeBtnSelected()
    {
        MakeUnLock();
        _image.sprite = selectedBtnSprite;
    }

    public void MakeBtnUnSelect()
    {
        MakeUnLock();
    }

    public void MakeUnLock()
    {
        _image.sprite = unSelectedBtnSprite;
        ballImage.gameObject.SetActive(true);
        coinsImage.SetActive(false);
        price.gameObject.SetActive(false);
    }

    public void MakeLock()
    {
        _image.sprite = lockedBtnSprite;
        ballImage.gameObject.SetActive(false);
        coinsImage.SetActive(true);
        price.gameObject.SetActive(true);
    }
}

[Serializable]
public class BallStoreSpecifications
{
    public BallItemType ballType;
    public int price;
    public BallStoreItemStatus status;
}

public enum BallStoreItemStatus
{
    Locked,
    Unlocked
}

public enum BallItemType
{
    Regular,
    Football, Basketball,
    Tennis, FunnyOrange, Handball, Bulling,
    BaseballWhite, FunnyBlue, FunnyAdvance, BaseballGreen,
    VolleyballWhite, VolleyballYellow
}
