﻿using _02_Scripts;
using _02_Scripts._02_Stage;
using UnityEngine;

public class BoardCameraManager : CameraManager
{
    protected override int GetRowNumbers()
    {
        return StageManager.Instance.GetBoardRowNumber();
    }

    protected override int GetColumnNumbers()
    {
        return StageManager.Instance.GetColumnNumber();
    }

    protected override float TopUiHeightSize()
    {
        return 2f;
    }

    protected override float GetYPositionOfTopUi()
    {
        return _camera.orthographicSize;
    }
}
