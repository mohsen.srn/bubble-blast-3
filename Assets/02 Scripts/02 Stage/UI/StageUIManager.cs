﻿using System.Collections;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.enums;
using RTLTMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _02_Scripts._02_Stage.UI
{
    public class StageUIManager : MonoBehaviour
    {
        public RTLTextMeshPro levelText;
        public RTLTextMeshPro levelTextInsideIcon;

        public Sprite noStar, oneStar, twoStar, threeStar;
        public GameObject darkShadowUnderDialogs;
        
        public GameObject gameOverDialog;
        public Image gameOverStars;
        public RTLTextMeshPro gameOverPoints;
        
        public GameObject winDialog;
        public Image winStars;
        public RTLTextMeshPro winPoints;

        private Controller _controller;

        private void Start()
        {
            WinManager.OnGameFinish += FinishLevel;
            SetLevel(PlayerPrefs.GetInt("Level", 0) + 1);
            darkShadowUnderDialogs.SetActive(false);
            gameOverDialog.SetActive(false);
            winDialog.SetActive(false);

            _controller = FindObjectOfType<Controller>();
        }
        
        void Update(){
            if (Input.GetKeyDown(KeyCode.Escape)) 
                Back(); 
        }
        
        private void OnDestroy()
        {
            WinManager.OnGameFinish -= FinishLevel;
        }

        void SetLevel(int levelNumber)
        {
            levelText.text = levelNumber + "";
            levelTextInsideIcon.text = levelNumber + "";
        }

        void FinishLevel(GameResult result)
        {
            if (result.Result == GResult.Lose)
            {
                StartCoroutine(ShowGameoverDialog(result, 2f));
            }
            else
            {
                StartCoroutine(ShowWinDialog(result, 2f));
            }
        }

        public void LoadScene()
        {
            SceneManager.LoadScene("01 Stage Levels");
        }

        IEnumerator ShowGameoverDialog(GameResult result, float delay)
        {
            yield return new WaitForSeconds(delay);

            switch (result.Stars)
            {
                case Stars.No_Star:
                    gameOverStars.sprite = noStar;
                    break;
                case Stars.One_Star:
                    gameOverStars.sprite = oneStar;
                    break;
            }
            gameOverPoints.text =  Mathf.FloorToInt
                                       (result.Points * 100f / 
                                        StageManager.Instance.GetTotalResistance()) + "%"; 
            gameOverDialog.SetActive(true);
            
            _controller.IsDisable = true;

            darkShadowUnderDialogs.SetActive(true);
        }

        public void Replay()
        {
            SceneManager.LoadScene("02 Stage Board");
        }

        public void Back()
        {
            LoadScene();
        }

        IEnumerator ShowWinDialog(GameResult result, float delay)
        {
            yield return new WaitForSeconds(delay);

            switch (result.Stars)
            {
                case Stars.Two_Star:
                    winStars.sprite = twoStar;
                    break;
                case Stars.Three_Star:
                    winStars.sprite = threeStar;
                    break;
            }
            winPoints.text =  Mathf.CeilToInt(result.Points * 100f 
                                              / StageManager.Instance.GetTotalResistance()) + "%";

            _controller.IsDisable = true;

            winDialog.SetActive(true);
            darkShadowUnderDialogs.SetActive(true);
        }

        public void NextLevel()
        {
            int oLevel = PlayerPrefs.GetInt("Level", 0);
            PlayerPrefs.SetInt("Level", oLevel + 1);
            
            SceneManager.LoadScene("02 Stage Board");
        }
    }
}
