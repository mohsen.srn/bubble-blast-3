﻿using System.Collections;
using System.Text;
using _02_Scripts._02_Stage;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.enums;
using RTLTMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarManager : MonoBehaviour
{
    public Image progressbar;
    public Image firstStar, secondStar, thirdStar;
    public Sprite inactiveStar, activeStar;

    public delegate void ProgressBarActions(int starNumber);
    public static ProgressBarActions OnTakeStar;

    private Stars _stars;

    public Animator firstStarGainAnimation;
    public Animator secondStarGainAnimation;
    public Animator thirdStarGainAnimation;
    public Transform firstStarTransform;
    public Transform secondStarTransform;
    public Transform thirdStarTransform;
  
    public RTLTextMeshPro pointsText;
    void Start()
    {
        activateFirstStar(false);
        activateSecondStar(false);
        activateThirdStar(false);

        StartCoroutine(InitStars());

        WinManager.OnPointsChanged += UpdatePoints;
        WinManager.OnGameStatusChange += UpdateStatus;

        progressbar.fillAmount = 0f;
        pointsText.text = "0";
    }

    IEnumerator InitStars()
    {
        yield return new WaitForSeconds(0.5f);
        firstStarTransform.position = firstStar.transform.position;
        secondStarTransform.position = secondStar.transform.position;
        thirdStarTransform.position = thirdStar.transform.position;
    }

    private void OnDestroy()
    {
        WinManager.OnPointsChanged -= UpdatePoints;
        WinManager.OnGameStatusChange -= UpdateStatus;
    }

    private void UpdatePoints(int point)
    {
        /*StringBuilder s = new StringBuilder();
        s.Append(point + " ( ");
        s.Append(Mathf.CeilToInt(point * 100f / StageManager.Instance.GetTotalResistance()) + "%");
        s.Append(" )");
        pointsText.text = s.ToString();
        //point+ " / " + StageManager.Instance.GetTotalResistance();
        progressbar.fillAmount = point * 1f / StageManager.Instance.GetTotalResistance();*/
    }

    private void UpdateStatus(GameResult result)
    {
        StringBuilder s = new StringBuilder();
        s.Append(result.Points + " (");
        s.Append(Mathf.FloorToInt(result.Points * 100f / StageManager.Instance.GetTotalResistance()) + "%");
        s.Append(")");
        pointsText.text = s.ToString();
        //point+ " / " + StageManager.Instance.GetTotalResistance();
        progressbar.fillAmount = result.Points * 1f / StageManager.Instance.GetTotalResistance();
        
        switch (result.Stars)
        {
            case Stars.No_Star:
                activateFirstStar(false);
                activateSecondStar(false);
                activateThirdStar(false);
                
                break;
            case Stars.One_Star:
                activateFirstStar(true);
                activateSecondStar(false);
                activateThirdStar(false);

                if (_stars == Stars.No_Star)
                {
                    firstStarGainAnimation.SetTrigger("GainStar");
                    OnTakeStar?.Invoke(1);
                }
                break;
            case Stars.Two_Star:
                activateFirstStar(true);
                activateSecondStar(true);
                activateThirdStar(false);

                if (_stars == Stars.One_Star)
                {
                    secondStarGainAnimation.SetTrigger("GainStar");
                    OnTakeStar?.Invoke(2);
                }

                break;
            case Stars.Three_Star:
                activateFirstStar(true);
                activateSecondStar(true);
                activateThirdStar(true);

                if (_stars == Stars.Two_Star)
                {
                    thirdStarGainAnimation.SetTrigger("GainStar");
                    OnTakeStar?.Invoke(3);
                }
                break;
        }
        
        _stars = result.Stars;
    }

    private void activateFirstStar(bool isActive)
    {
//        firstStar.enabled = isActive;

        if (isActive)
        {
            firstStar.sprite = activeStar;
        }
        else
        {
            firstStar.sprite = inactiveStar;
            
        }
    }
    
    private void activateSecondStar(bool isActive)
    {
//        secondStar.enabled = isActive;
        secondStar.sprite = isActive ? activeStar : inactiveStar;
    }
    
    private void activateThirdStar(bool isActive)
    {
//        thirdStar.enabled = isActive;

        if (isActive)
        {
            thirdStar.sprite = activeStar;
        }
        else
        {
            thirdStar.sprite = inactiveStar;
        }
    }

    private void NoStar()
    {
        
    }
    private void OneStar()
    {
        
    }

    private void TwoStar()
    {
        
    }

    private void ThreeStar()
    {
        
    }
}
