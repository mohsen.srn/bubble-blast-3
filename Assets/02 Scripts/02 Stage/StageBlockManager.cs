﻿using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Obstacles;
using _02_Scripts.ScriptableObjects;
using NaughtyAttributes;
using UnityEngine;

namespace _02_Scripts._02_Stage
{
    public class StageBlockManager : BaseBlockManager
    {
        [Header("Debug:")] 
        public List<Row> board = new List<Row>();

        [ShowNonSerializedField] private int _boardColumnNumber = 10;
        [ShowNonSerializedField] private int _stageRowNumber = 19;

// this variable is for tracking extra rows to add to board
        int _lastRowInserted;

        [BoxGroup("Assets")] 
        public GameObject pitPrefab;
        public GameObject embeddedBrick;
        public GameObject halfBrick;
        
        [ShowNonSerializedField] 
        private int _boardRowNumber = 7;

        public List<StageRow> stageRows = new List<StageRow>();
        public static StageBlockManager Instance { get; private set; }

        public delegate void StageBlockManagerActions();
        public static event StageBlockManagerActions OnNextRow;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(this);
        }

        private void OnDestroy()
        {
            BallManager.OnBallStop -= NextRow;
            Instance = null;
        }

        private void Start()
        {
            _boardRowNumber = StageManager.Instance.GetStageRowNumber();
            _boardColumnNumber = StageManager.Instance.GetColumnNumber();
            _stageRowNumber = StageManager.Instance.GetStageRowNumber();
            stageRows = StageManager.Instance.GetBoardBlocks();

            _lastRowInserted = StageManager.Instance.GetBoardRowNumber();

            if (!StageManager.Instance.IsFix())
            {
                BallManager.OnBallStop += NextRow;
            }

            CreateStageBoard_Instantly();
        }

        private IEnumerator CreateStageBoard()
        {
            BallManager.GetInstance.BallNumbers = StageManager.Instance.GetInitBalls();

            for (int i = stageRows.Count - 1; i >= 0; i--)
            {
                if (Mathf.Abs(i - stageRows.Count) > _lastRowInserted)
                {
                    yield break;
                }

                var row = PoolManager.Instance.GetRow();
                row.rowTransform.parent = BoardManager.Instance.gameObject.transform;
                // we add rows to board list so that we can keep state  
                BoardManager.Instance.AddRow(row, false);
                var blocks = new List<Obstacle>();

                blocks = GetBlocks(stageRows[i].stageObstacles, row);
                row.UpdateObjectsInThisRow(blocks);
                yield return new WaitForSeconds(0.5f);
            }
        }

        private List<Obstacle> GetBlocks(List<StageObstacle> obsts, Row row)
        {
            var blocks = new List<Obstacle>();
            foreach (var obstacle in obsts)
            {
                // ReSharper disable once IdentifierTypo
                Obstacle obstcl;
                switch (obstacle.type)
                {
                    case StageObstacle.ObstacleType.p:

                        var pit = Instantiate(pitPrefab, row.rowTransform);
                        obstcl = pit.GetComponent<Pit>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.O:

                        var ballPlus = PoolManager.Instance.GetBallPlus();
                        ballPlus.transform.parent = row.rowTransform;
                        obstcl = ballPlus.GetComponent<BallPlus>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.LJ:

                        var block = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.Ordinary);
                        block.transform.parent = row.rowTransform;
                        obstcl = block.GetComponent<Block>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        ((Block) obstcl).MakeBlock(Obstacle.Type.Ordinary);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.BOXBMB:

                        var boxBombBlock = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.BoxBomb);
                        boxBombBlock.transform.parent = row.rowTransform;
                        obstcl = boxBombBlock.GetComponent<Block>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        ((Block) obstcl).MakeBlock(Obstacle.Type.BoxBomb);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.ROWBMB:

                        var rowBombBlock = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.RowBomb);
                        rowBombBlock.transform.parent = row.rowTransform;
                        obstcl = rowBombBlock.GetComponent<Block>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        ((Block) obstcl).MakeBlock(Obstacle.Type.RowBomb);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.COLBMB:

                        var colBombBlock = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.ColumnBomb);
                        colBombBlock.transform.parent = row.rowTransform;
                        obstcl = colBombBlock.GetComponent<Block>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        ((Block) obstcl).MakeBlock(Obstacle.Type.ColumnBomb);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.BRK:

                        var eBrick = Instantiate(embeddedBrick, row.rowTransform);
                        obstcl = eBrick.GetComponent<EmbeddedBrick>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    // ReSharper disable once RedundantCaseLabel
                    case StageObstacle.ObstacleType.__:
                    default: // i put default here to ensure all of obstcl will initials
                        var empty = PoolManager.Instance.GetNull();
                        obstcl = empty.GetComponent<NullObstacle>();
                        obstcl.SpecifyResistance(0);
                        obstcl.obstacleMetaData = obstacle;
                        break;
                        
                    case StageObstacle.ObstacleType.LDBr:
                    case StageObstacle.ObstacleType.LTBr:
                    case StageObstacle.ObstacleType.RDBr:
                    case StageObstacle.ObstacleType.RTBr:
                        
                        var hBrick = Instantiate(halfBrick, row.rowTransform);
                        obstcl = hBrick.GetComponent<HalfBrick>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        obstcl.obstacleMetaData = obstacle;
                        
                        Obstacle.Type brickType = Obstacle.Type.LeftTopBrick; // for default
                        if (obstacle.type == StageObstacle.ObstacleType.LTBr)
                        {
                            brickType = Obstacle.Type.LeftTopBrick;
                        } else if (obstacle.type == StageObstacle.ObstacleType.LDBr)
                        {
                            brickType = Obstacle.Type.LeftDownBrick
                            ;
                        } else  if (obstacle.type == StageObstacle.ObstacleType.RTBr)
                        {
                            brickType = Obstacle.Type.RightTopBrick;
                        } else  if (obstacle.type == StageObstacle.ObstacleType.RDBr)
                        {
                            brickType = Obstacle.Type.RightDownBrick;
                        }
                        ((HalfBrick) obstcl).MakeHalfBrick(brickType);
                        break;
                    
                    case StageObstacle.ObstacleType.LDBl:
                    case StageObstacle.ObstacleType.LTBl:
                    case StageObstacle.ObstacleType.RDBl:
                    case StageObstacle.ObstacleType.RTBl:
                        Obstacle.Type sideBlockType = Obstacle.Type.LeftTopBlock; // for default
                        if (obstacle.type == StageObstacle.ObstacleType.LTBl)
                        {
                            sideBlockType = Obstacle.Type.LeftTopBlock;
                        } else if (obstacle.type == StageObstacle.ObstacleType.LDBl)
                        {
                            sideBlockType = Obstacle.Type.LeftDownBlock;
                        } else  if (obstacle.type == StageObstacle.ObstacleType.RTBl)
                        {
                            sideBlockType = Obstacle.Type.RightTopBlock;
                        } else  if (obstacle.type == StageObstacle.ObstacleType.RDBl)
                        {
                            sideBlockType = Obstacle.Type.RightDownBlock;
                        }
                        
                        var sideBlock = PoolManager.Instance.GetBlock(row, obstacle.resistance, sideBlockType);
                        sideBlock.blockTransform.parent = row.rowTransform;
                        obstcl = sideBlock;
                        obstcl.SpecifyResistance(obstacle.resistance);

                        ((Block) obstcl).MakeBlock(sideBlockType);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                    case StageObstacle.ObstacleType.SPNG:

                        var sponge = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.Sponge);
                        sponge.transform.parent = row.rowTransform;
                        obstcl = sponge.GetComponent<Block>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        ((Block) obstcl).MakeBlock(Obstacle.Type.Sponge);
                        obstcl.obstacleMetaData = obstacle;

                        break;
                }
                blocks.Add(obstcl);
            }
            return blocks;
        }

        private void CreateStageBoard_Instantly()
        {
            BallManager.GetInstance.BallNumbers = StageManager.Instance.GetInitBalls();

            for (int i = stageRows.Count - 1; i >= 0; i--)
            {
                if (Mathf.Abs(i - stageRows.Count) > _lastRowInserted)
                {
                    return;
                }

                var row = PoolManager.Instance.GetRow();
                row.rowTransform.parent = BoardManager.Instance.gameObject.transform;
                // we add rows to board list so that we can keep state  
                BoardManager.Instance.AddRowInstantly(row, false);
                var blocks = new List<Obstacle>();

                blocks = GetBlocks(stageRows[i].stageObstacles, row);

                row.UpdateObjectsInThisRow(blocks);
            }
        }

        void NextRow()
        {
           OnNextRow?.Invoke();
           var row = PoolManager.Instance.GetRow();
            if (BoardManager.Instance != null)
            {
                row.rowTransform.parent = BoardManager.Instance.gameObject.transform;
                // we add rows to board list so that we can keep state  
                BoardManager.Instance.AddRow(row, false);
            }

            var blocks = new List<Obstacle>();

            if (_lastRowInserted > _stageRowNumber - 1)
            {
//                CreateEmptyRow(row);
                return;
            }
            blocks = GetBlocks(stageRows[stageRows.Count - ++_lastRowInserted].stageObstacles, row);
            
            row.UpdateObjectsInThisRow(blocks);
        }

        void CreateEmptyRow(GameObject row)
        {
            for (int i = 0; i < _boardColumnNumber; i++)
            {
                var empty = PoolManager.Instance.GetRow();
                Obstacle obstacle = empty.GetComponent<Obstacle>();
                obstacle.SpecifyResistance(0);
                obstacle.obstacleType = Obstacle.Type.Null;
            }
        }
    }
}
