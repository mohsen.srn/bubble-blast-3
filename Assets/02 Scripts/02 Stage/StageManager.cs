﻿using System;
using System.Collections.Generic;
using _02_Scripts._02_Stage.Data;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.enums;
using _02_Scripts.ScriptableObjects;
using UnityEngine;

namespace _02_Scripts._02_Stage
{
    public class StageManager : MonoBehaviour
    {
        public int currentLevel;
        public GameStages gameLevels;

        public delegate void StageManagerActions();
        public static event StageManagerActions OnLost, OnWin;

        public static StageManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                currentLevel = PlayerPrefs.GetInt("Level", 0);
            }
            else if (Instance != this) Destroy(this);
        }

        private void Start()
        {
            WinManager.OnGameFinish += LevelEnded;
        }

        private void OnEnable()
        {
//            BallManager.GetInstance.Init();
        }

        private void OnDestroy()
        {
            WinManager.OnGameFinish -= LevelEnded;
            Instance = null;
            
//            BallManager.GetInstance.Pause();
        }

        public int GetStageRowNumber()
        {
            return gameLevels.stages[currentLevel].gameRows;
        }

        public int GetBoardRowNumber()
        {
            return gameLevels.stages[currentLevel].boardRows;
        }

        public int GetColumnNumber()
        {
            return gameLevels.stages[currentLevel].col;
        }

        public List<StageRow> GetBoardBlocks()
        {
            return StageGameData.Instance.stagesMetaData.Stages[currentLevel].stageBoard;
        }

        public int GetInitBalls()
        {
            return StageGameData.Instance.stagesMetaData.Stages[currentLevel].CurrentBallNumber;
        }

        public int GetTotalResistance()
        {
            return StageGameData.Instance.stagesMetaData.Stages[currentLevel].totalBreakableResistance;
        }

        public bool IsFix()
        {
            return gameLevels.stages[currentLevel].IsFix;
        }

        private void OnDisable()
        {
            StageGameData.Instance.Save();
        }

        private void LevelEnded(GameResult result)
        {
            if (result.Result == GResult.Win)
            {
                OnWin?.Invoke();
                if (StageGameData.Instance.stagesMetaData.Stages[currentLevel].Status == StageStatus.Current)
                {
                    StageGameData.Instance.stagesMetaData.Stages[currentLevel].Status = StageStatus.Passed;
                    if (StageGameData.Instance.stagesMetaData.Stages.Count > currentLevel + 1)
                    {
                        StageGameData.Instance.stagesMetaData.Stages[currentLevel + 1].Status = StageStatus.Current;
                    }
                }
            }
            else
            {
                OnLost?.Invoke();
            }
            StageGameData.Instance.stagesMetaData.Stages[currentLevel].gameResult = result;
        }
    }
}
