﻿using _02_Scripts.Helper_Classes;
using UnityEngine;

namespace _02_Scripts._02_Stage
{
    public class WinManager : MonoBehaviour
    {
        /* region start******************** Make Class Singleton *******************/
        public static WinManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(this);
        }
        /* end region****************************************************************/
        public delegate void ProgressGameActions(int a); 
        public static event ProgressGameActions OnPointsChanged;
        public static event ProgressGameActions OnPunish;
        
        public delegate void GameFinishAction(GameResult result);
        public static event GameFinishAction OnGameFinish;
        public static event GameFinishAction OnGameStatusChange;
        
        private int _totalResistance;
        private int _remainingResistance;
        private int _halfOfResistance;

        // Resistances that user broke by the balls
        private int _takenResistance;
        // Resistance that collide to ground
        private int _lostResistance;

        private int _points;
        
        private void Start()
        {
            Block.OnResistanceDecreased += OnResistanceDecreased;
            _totalResistance = StageManager.Instance.GetTotalResistance();
            _remainingResistance = _totalResistance;
            _halfOfResistance = _totalResistance / 2;
            Block.OnBlockReachedGround += Punish;
            Block.OnExplode += OnBlockExplode;
        }

        private void OnDestroy()
        {
            Block.OnResistanceDecreased -= OnResistanceDecreased;
            Block.OnBlockReachedGround -= Punish;
            Block.OnExplode -= OnBlockExplode;

            Instance = null;
        }

        private void OnResistanceDecreased(int resistanceDecreased)
        {
            _remainingResistance--;
            _takenResistance++;
            CalculatePoints();
        }

        private void OnBlockExplode(int resistance)
        {
            _remainingResistance -= resistance;
            _takenResistance += resistance;
            CalculatePoints();
        }

        /// <summary>
        /// this method is called when a block reaches down to ground
        /// in this situation we decrease some point from player
        /// </summary>
        /// <param name="n"></param>
        private void Punish(int n)
        {
            _lostResistance += n;
            _remainingResistance -= n;
            OnPunish?.Invoke(n);

            CalculatePoints();
        }

        private void CalculatePoints()
        {
            _points = _takenResistance - _lostResistance;
            
            OnPointsChanged?.Invoke(_points);

//            print("_totalResistance = " + _totalResistance + " / taken = " 
//                  + _takenResistance + " / lost = " + _lostResistance);

            if (_lostResistance + _takenResistance >= _totalResistance)
            {
                OnGameFinish?.Invoke(new GameResult(_totalResistance, _takenResistance));
            }
            
            OnGameStatusChange?.Invoke(new GameResult(_totalResistance , _points, 0));
        }
    }
}
