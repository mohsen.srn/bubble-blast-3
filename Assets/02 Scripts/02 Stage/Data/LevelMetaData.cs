using System;
using System.Collections.Generic;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.enums;
using _02_Scripts.Obstacles;
using _02_Scripts.ScriptableObjects;

namespace _02_Scripts._02_Stage.Data
{
    /// <summary>
    ///     We save data about any level that gamer played so far
    /// </summary>
    [Serializable]
    public class LevelMetaData
    {
        public List<StageRow> stageBoard;

        public LevelMetaData(List<StageRow> stageRows, int currentBallNumber)
        {
            CurrentBallNumber = currentBallNumber;
            stageBoard = stageRows;
            Status = StageStatus.Locked;
            CalculateTotalResistance();
        }

        public StageStatus Status { get; set; }
        public GameResult gameResult;
        public int totalBreakableResistance;
        public int CurrentBallNumber { get; set; }

        private void CalculateTotalResistance()
        {
            foreach (var stageRow in stageBoard)
                foreach (var obstacle in stageRow.stageObstacles)
                    if (obstacle.type == StageObstacle.ObstacleType.LJ
                        || obstacle.type == StageObstacle.ObstacleType.BOXBMB
                        || obstacle.type == StageObstacle.ObstacleType.ROWBMB
                        || obstacle.type == StageObstacle.ObstacleType.COLBMB
                        || obstacle.type == StageObstacle.ObstacleType.RDBl
                        || obstacle.type == StageObstacle.ObstacleType.LDBl
                        || obstacle.type == StageObstacle.ObstacleType.LTBl
                        || obstacle.type == StageObstacle.ObstacleType.RTBl
                        || obstacle.type == StageObstacle.ObstacleType.SPNG)
                        totalBreakableResistance += obstacle.resistance;
        }
    }
}
