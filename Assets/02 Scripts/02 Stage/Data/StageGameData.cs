﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using _02_Scripts.ScriptableObjects;
using UnityEngine;

namespace _02_Scripts._02_Stage.Data
{
    public class StageGameData : MonoBehaviour
    {        
        public string preFixFileName;
        public string fileToSave;
        
        public GameStages gameStages;
        public StagesMetaData stagesMetaData;
        public static StageGameData Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
                UpdateAndLoadData();
            }
            else if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void UpdateAndLoadData()
        {
            // first read data
            Load();

            var gameStagesClone = gameStages;
            if (gameStagesClone != null)
            {
                var assetStages = gameStagesClone.stages;
                var metaData = stagesMetaData.Stages;

                if (metaData.Count != assetStages.Count)
                {
                    for (var i = 0; i < assetStages.Count; i++)
                        if (metaData.Count < i + 1)
                        {
                            var levelMetaData =
                                new LevelMetaData(assetStages[i].stageBoard, assetStages[i].initBalls);
                            if (i == 0)
                                levelMetaData.Status = StageStatus.Current;

                            stagesMetaData.Stages.Add(levelMetaData);
                        }

                    Save();
                }
            }
        }

        public void Save()
        {
            // Create a binary formatter which can read binary files
            var formatter = new BinaryFormatter();
            
            // Create a route from app to the file
            var fileStream = File.Open(Application.persistentDataPath + "/" + preFixFileName + fileToSave + ".dat", FileMode.Create);

            // Create and assign the data to save
            var data = new StagesMetaData();
            data = stagesMetaData;

            // Save the data in the file
            formatter.Serialize(fileStream, data);

            // Close the data stream
            fileStream.Close();

//            print("Saved");
        }

        public void Load()
        {
            if (File.Exists(Application.persistentDataPath + "/" + preFixFileName + fileToSave + ".dat"))
            {
                var formatter = new BinaryFormatter();
                var fileStream = File.Open(Application.persistentDataPath + "/" + preFixFileName + fileToSave + ".dat", FileMode.Open);
                stagesMetaData = formatter.Deserialize(fileStream) as StagesMetaData;
                fileStream.Close();
//                print("Loaded");
            }
            else
            {
                stagesMetaData = new StagesMetaData();
            }
        }

        public List<LevelMetaData> GetLevels()
        {
            Load();
            return stagesMetaData.Stages;
        }

        public void SetFileNameToSaveAndLoad()
        {
//            fileToSave = s;
            stagesMetaData.Stages.Clear();
            Save();
            UpdateAndLoadData();
        }
    }
}
