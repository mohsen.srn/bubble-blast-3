using System;
using System.Collections.Generic;
using _02_Scripts._02_Stage.Data;

namespace _02_Scripts._02_Stage
{
    [Serializable]
    public class StagesMetaData
    {
        public List<LevelMetaData> Stages = new List<LevelMetaData>();
        public int TakenStars;
    }
}
