﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LostingPoint : MonoBehaviour
{
    public Transform destinationPosition;
    private int point;
    public Transform lpTransform;
    private SpriteRenderer _spriteRenderer;

    public int Point
    {
        get => point;
        set { 
            point = value;
            pointText.text = " - " + point + "";
        }
    }
    public TextMeshPro pointText;

    public int numberOfFrame2Move = 20;
    public float speed = 0.01f;

    void Start()
    {
        lpTransform = transform;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(MoveUpAndFade());
    }

    IEnumerator MoveUpAndFade()
    {
        
        for (int i = 0; i < numberOfFrame2Move; i++)
        {
            yield return null;
            Vector3 tPos = lpTransform.position;
            lpTransform.position = new Vector3(tPos.x,
                tPos.y + speed * Time.deltaTime,
                tPos.z);

//            Color color = _spriteRenderer.color;
//            _spriteRenderer.color = new Color(color.r, color.g, color.b, color.a * 20 / 21);
        }
        
        GetComponent<Animator>().SetTrigger("out");
        StartCoroutine(DestroyWithDelay());
    }

    IEnumerator DestroyWithDelay()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject); // fix this with pooling system
    }
}
