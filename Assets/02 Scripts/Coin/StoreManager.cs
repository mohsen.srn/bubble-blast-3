using System;
using System.Collections;
using System.Text;
using _02_Scripts.Helper_Classes.Models;
using RTLTMPro;
using UnityBackStack;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoreManager : MonoBehaviour_BackStackSupport
{
    /****************************** Singleton *************************/
    public static StoreManager GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    /******************************************************************/

    public GameObject storeDialog;
    public GameObject darkLayout;
    private Animator _animator;

    public enum StoreType
    {
        Bazaar, MyKet
    }

    public StoreType storeType;

    public static event Action OnStoreOpen;
    public static event Action OnStoreClose;
    public static event Action OnPurchaseDone;

    public RTLTextMeshPro[] prices = new RTLTextMeshPro[5];
    public RTLTextMeshPro[] coinNumbers = new RTLTextMeshPro[5];
    private void Start()
    {
        _animator = GetComponent<Animator>();
        darkLayout.SetActive(false);
        storeDialog.SetActive(false);
    }

    public void Open()
    {
        BackStackManager.GetInstance.Push2BackStack(this);
        
        // here i force to set products from PlayerPrefs. otherwise maybe 'bazaarProducts' was empty
        switch (storeType)
        {
            case StoreType.Bazaar:
                BazaarIABManager.GetInstance.SetProducts();
                
                for (int i = 0; i < 5; i++)
                {
                    BazaarProduct bazaarProduct = BazaarIABManager.GetInstance.bazaarProducts
                        .GetProduct(BazaarIABManager.GetInstance.skus[i]);
                    prices[i].text = bazaarProduct.Price.PersianToEnglish();
            
                    StringBuilder coinNumber = new StringBuilder("+").Append(bazaarProduct.Description);
                    coinNumbers[i].text = coinNumber.ToString();
                }

                break;
            case StoreType.MyKet:
                InAppStore.GetInstance.SetProducts();
                for (int i = 0; i < 5; i++)
                {
                    BazaarProduct bazaarProduct = InAppStore.GetInstance.bazaarProducts
                        .GetProduct(InAppStore.GetInstance.skus[i].productId);
                    prices[i].text = bazaarProduct.Price.PersianToEnglish();
            
                    StringBuilder coinNumber = new StringBuilder("+").Append(bazaarProduct.Description);
                    coinNumbers[i].text = coinNumber.ToString();
                }
                
                break;
        }

        storeDialog.SetActive(true);
        
        _animator.ResetTrigger("enter");
        _animator.ResetTrigger("exit");

        _animator.SetTrigger("enter");
        darkLayout.SetActive(true);
        if (Controller.GetInstance != null)
        {
            Controller.GetInstance.IsDisable = true;
        }

        OnStoreOpen?.Invoke();
//        StartCoroutine("CheckScape");
    }

    public void Close()
    {
        _animator.ResetTrigger("enter");
        _animator.ResetTrigger("exit");

        _animator.SetTrigger("exit");
        darkLayout.SetActive(false);
        if (Controller.GetInstance != null)
        {
            Controller.GetInstance.IsDisable = false;
        }
        
        OnStoreClose?.Invoke();
        StartCoroutine(DisableStoreDialogWithDelay());
    }

    IEnumerator DisableStoreDialogWithDelay()
    {
        yield return new WaitForSeconds(1f);
        storeDialog.SetActive(false);
    }

    /*IEnumerator CheckScape()
    {
        while (!Input.GetKeyDown(KeyCode.Escape))
        {
            yield return null;
        }
        Close();
    }*/

    public void Purchase(int itemIndex)
    {
#if UNITY_EDITOR
        OnPurchaseDone?.Invoke();
        Close();
        // go to first scene
        SceneManager.LoadScene(0);
#endif

        if (GameManager.GetInstance.releaseMode == GameManager.ReleaseMode.Debug)
        {
            OnPurchaseDone?.Invoke();
            Close();
            // go to first scene
            SceneManager.LoadScene(0);
        }
        else
            switch (storeType)
        {
            case StoreType.Bazaar:
                
                BazaarIABManager.GetInstance.Purchase(itemIndex,
                    () =>
                    {
                        OnPurchaseDone?.Invoke();
                        Close();
                        // go to first scene
                        SceneManager.LoadScene(0);
                    });
                
                break;
            
            case StoreType.MyKet:
                
                InAppStore.GetInstance.purchaseProduct(itemIndex, () =>
                {
                    OnPurchaseDone?.Invoke();
                    Close();
                    // go to first scene
                    SceneManager.LoadScene(0);
                });
                
                break;
        }
    }

    public override void BackCalled()
    {
        Close();
    }
}
