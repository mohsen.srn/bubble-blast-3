using System;
using System.Collections;
using I2.Loc;
using NaughtyAttributes;
using RTLTMPro;
using UnityEngine;

public class CoinRepository : MonoBehaviour
{
    /******************************** Singleton **********************************/

    /*public static CoinRepository GetInstance;*/

    private void Awake()
    {
       /* if (GetInstance == null)
        {
            GetInstance = this;
        }
        else
        {
            // Destroy(gameObject);
        }*/
    }

    /****************************************************************************/
    
    public RTLTextMeshPro englishCoinAmountText;
    public RTLTextMeshPro farsiCoinAmountText;
    public ParticleSystem gainCoinParticleSystem;
    private void Start()
    {
        InitCoinText(CoinManager.GetInstance.GetCoins());
        CoinManager.OnCoinChange += SetCoinText;
    }

    private void OnDestroy()
    {
        CoinManager.OnCoinChange -= SetCoinText;
    }

    private void InitCoinText(int amount)
    {
        if(LocalizationManager.CurrentLanguage == 
           LocalizationManager.GetAllLanguages()[0])
        {
            englishCoinAmountText.gameObject.SetActive(true);
            farsiCoinAmountText.gameObject.SetActive(false);

            englishCoinAmountText.text = amount.ToString();
        }
        else
        {
            englishCoinAmountText.gameObject.SetActive(false);
            farsiCoinAmountText.gameObject.SetActive(true);

            farsiCoinAmountText.text = amount.ToString();
        }
    }

    private void SetCoinText(int newAmount)
    {
        int oldAmount;
        oldAmount = Int32.Parse(LocalizationManager.CurrentLanguage == 
                                LocalizationManager.GetAllLanguages()[0] 
            ? englishCoinAmountText.text : farsiCoinAmountText.text);

        float step = (newAmount - oldAmount) / 50f;

        if (Mathf.Abs(step) < 1)
        {
            if (step < 0)
            {
                step = -1;
            }
            else
            {
                step = 1;
            }
        }

        int intStep = Mathf.RoundToInt(step);
        
        if (intStep > 0)
        {
            StartCoroutine(IncreaseTextByFrame(newAmount, oldAmount, intStep));
        }
        else
        {
            StartCoroutine(DecreaseTextByFrame(newAmount, oldAmount, intStep));
        }
    }

    IEnumerator IncreaseTextByFrame(int newAmount, int oldAmount, int step)
    {
        // coinAmountText.fontSize += 10f;
        gainCoinParticleSystem.Play();
        
        oldAmount += step;
        while (oldAmount <= newAmount)
        {
            oldAmount += step;
            yield return null;
            englishCoinAmountText.text = oldAmount.ToString();
            farsiCoinAmountText.text = oldAmount.ToString();
        }
        englishCoinAmountText.text = newAmount.ToString();
        farsiCoinAmountText.text = newAmount.ToString();

        // coinAmountText.fontSize -= 10f;
    }
    
    IEnumerator DecreaseTextByFrame(int newAmount, int oldAmount, int step)
    {
       // coinAmountText.fontSize += 10f;

        oldAmount += step;
        while (oldAmount >= newAmount)
        {
            oldAmount += step;
            yield return null;
            englishCoinAmountText.text = oldAmount.ToString();
            farsiCoinAmountText.text = oldAmount.ToString();
        }
        englishCoinAmountText.text = newAmount.ToString();
        farsiCoinAmountText.text = newAmount.ToString();

        //coinamounttext.fontsize -= 10f;
    }
}
