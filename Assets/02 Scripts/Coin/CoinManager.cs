using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using NaughtyAttributes;
using RTLTMPro;
using TMPro;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    /*********************** Singleton************************/
    public static CoinManager GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    /*****************************************************/

    private static string _coinRepository = "Coin Number";
    public int initCoins = 150;

    public GameObject boughtCoinQuickDialog;
    public RTLTextMeshPro coinChangeBadge;

    public GameObject spendCoinQuickDialog;
    public RTLTextMeshPro coinDecreaseBadge;
    public static event Action<int> OnCoinChange;

    private void Start()
    {
        if (!PlayerPrefs.HasKey(_coinRepository))
        {
            PlayerPrefs.SetInt(_coinRepository, initCoins);            
            PlayerPrefs.Save();
            OnCoinChange?.Invoke(PlayerPrefs.GetInt(_coinRepository));
        }
        coinChangeBadge.gameObject.SetActive(false);
        boughtCoinQuickDialog.SetActive(false);
        spendCoinQuickDialog.SetActive(false);

        StoreManager.OnPurchaseDone += () => { StartCoroutine(EnableBoughtQDWithDelay(0.7f)); };
    }

    IEnumerator EnableBoughtQDWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        boughtCoinQuickDialog.SetActive(true);
    }

    IEnumerator EnableDecreaseCoinQDWithDelay()
    {
        yield return new WaitForSeconds(0.3f);
        spendCoinQuickDialog.SetActive(true);
    }

    public int GetCoins()
    {
        return PlayerPrefs.GetInt(_coinRepository);
    }

    public void SetCoins(int coinNumber)
    {
        PlayerPrefs.SetInt(_coinRepository, coinNumber);
        PlayerPrefs.Save();
        OnCoinChange?.Invoke(PlayerPrefs.GetInt(_coinRepository));
    }

    public void IncreaseCoins(int plus)
    {
        int coins = GetCoins();
        coins += plus;
        SetCoins(coins);
        
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(plus);
        coinChangeBadge.text = stringBuilder.ToString();
        StartCoroutine(EnableAndDisableBadge());
        
        StartCoroutine(EnableBoughtQDWithDelay(0f));
    }

    public bool DecreaseCoins(int minus, Action onSuccess, Action onFailure)
    {
        int coins = GetCoins();
        if (coins < minus)
        {
            onFailure?.Invoke();
            return false;
        }
        else
        {
            coins -= minus;
            SetCoins(coins);
            onSuccess?.Invoke();
            
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("-").Append(minus);
            coinDecreaseBadge.text = stringBuilder.ToString();
            StartCoroutine(EnableAndDisableBadge());

            StartCoroutine(EnableDecreaseCoinQDWithDelay());
            return true;
        }
    }

    IEnumerator EnableAndDisableBadge()
    {
        coinChangeBadge.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        coinChangeBadge.gameObject.SetActive(false);
    }
}
