using System;
using UnityEngine;
using UnityEngine.UI;

namespace _02_Scripts.Coin
{
    [RequireComponent (typeof(Button))]
    public class CheckMarket : MonoBehaviour
    {
        private void Start()
        {
#if UNITY_EDITOR
    // do nothing
#else
            if (GameManager.GetInstance.releaseMode!=GameManager.ReleaseMode.Debug)
            {
                if (StoreManager.GetInstance.storeType == StoreManager.StoreType.Bazaar)
                {
                    if (!SettingManager.Instance.IsBazaarInstalled())
                    {
                        GetComponent<Button>().interactable = false;
                    }
                }
                else if (StoreManager.GetInstance.storeType == StoreManager.StoreType.MyKet)
                {
                    if (!SettingManager.Instance.IsMyketInstalled())
                    {
                        GetComponent<Button>().interactable = false;
                    }
                }
            }
#endif
        }
    }
}
