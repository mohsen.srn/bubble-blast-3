using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class ArcadeCoin : MonoBehaviour
{
    public static ArcadeCoin GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public int giftCoin = 50;
    public int giftLevelSteps = 5;
    public int helpCostFor2Bomb = 50;
    public int helpCostFor5Bomb = 100;
    private void Start()
    {
        ArcadeBlockManager.OnLevelChanged += GiveGift;
    }

    private void OnDestroy()
    {
        ArcadeBlockManager.OnLevelChanged -= GiveGift;
    }
    private void GiveGift(int level)
    {
        switch (level)
        {
            case 10:
                CoinManager.GetInstance.IncreaseCoins(5);
                break;
            case 20:
                CoinManager.GetInstance.IncreaseCoins(10);
                break;
            case 50:
                CoinManager.GetInstance.IncreaseCoins(25);
                break;
            case 100:
                CoinManager.GetInstance.IncreaseCoins(50);
                break;
            case 200:
                CoinManager.GetInstance.IncreaseCoins(100);
                break;
            case 300:
                CoinManager.GetInstance.IncreaseCoins(150);
                break;
            case 400:
                CoinManager.GetInstance.IncreaseCoins(200);
                break;
            default:
                if (level > 400)
                {
                    if (level % 100 == 0)
                    {
                        // Coin giving Animation()
                        CoinManager.GetInstance.IncreaseCoins(level / 2);
                    }
                }
                break;
        }
    }

    public bool SpendCoinToGetHelp(int bombNumber, Action onSuccess, Action onFail)
    {
        if (bombNumber == 2)
        {
            return CoinManager.GetInstance.DecreaseCoins(helpCostFor2Bomb, 
                onSuccess, onFail);
        }
        else
        {
            return CoinManager.GetInstance.DecreaseCoins(helpCostFor5Bomb, 
                onSuccess, onFail);
        }
    }
}
