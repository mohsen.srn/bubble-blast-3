﻿using System;
using System.Collections;
using _02_Scripts;
using UnityEngine;

public class Controller : MonoBehaviour
{
    private BallManager _ballManager;
    private Vector2 _dir;
    private int _layerMask;
    private int _blockLayerMask;
    public float Angle;
    public GameObject Ball;
    public GameObject ShapeAtEntOfHint;
    public float Speed = 1;

    public bool IsDisable;

    public Camera _camera;
    public static Controller GetInstance { get; private set; }

    public delegate void ControllerAction();
    public static event ControllerAction OnScreenTouch;

    private void Awake()
    {
        if (GetInstance == null)
            GetInstance = this;
        else if (GetInstance != this) Destroy(this);
    }

    private void OnDestroy()
    {
        GetInstance = null;
    }

    private void Start()
    {
        _ballManager = BallManager.GetInstance;
        _layerMask = 1 << 11;
        _blockLayerMask = 1 << 10;
        _blockLayerMask = _blockLayerMask | _layerMask | (1 << 15);
    }

    public void OnMouseDown()
    {
        if (!enabled)
        {
            return;
        }
        StartCoroutine("CalMousePos");
    }

    public void OnMouseDrag()
    {
        // we do not want shoot new balls when older balls still running in board
        if (_ballManager.isBusy || IsDisable || !enabled)
        {
            return;
        }

        OnScreenTouch?.Invoke();
        
//        calculate_DirectionAndAngle();

        var position = _ballManager.transform.position;
        
/*        var ray4Line = Physics2D.Raycast(
            position,
            StandardVelocity(_dir), Mathf.Infinity, _layerMask);*/

        var ray4Shape = Physics2D.CircleCast(position, 0.18f,
            StandardVelocity(_dir),
            Mathf.Infinity, _blockLayerMask);

        var positions = new Vector3[3];
        positions[0] = new Vector3(position.x, position.y);
        positions[1] = new Vector3(ray4Shape.centroid.x, ray4Shape.centroid.y);
        positions[2] = Vector2.Reflect(positions[1] - positions[0], ray4Shape.normal);

        DottedLine.Instance.DrawDottedLine(positions[0], positions[1]);
        DottedLine.Instance.DrawDottedLine_withDirection(positions[1], positions[2]);
        ShapeAtEntOfHint.SetActive(true);
        // show a hint ball in the point that we anticipate ball hit
        ShapeAtEntOfHint.transform.position = ray4Shape.centroid;
    }

    public void OnMouseUp()
    {
        if (IsDisable || !enabled)
        {
            return;
        }
        StopCoroutine("CalMousePos");
        ShapeAtEntOfHint.SetActive(false);
        _ballManager.Shoot(StandardVelocity(_dir));
    }

    private Vector2 StandardVelocity(Vector2 dir)
    {
        var max = Mathf.Max(Mathf.Abs(dir.x), Mathf.Abs(dir.y));
        return new Vector2(Speed * dir.x / max, Speed * dir.y / max);
    }

    private Vector3 _oldPos;
    IEnumerator CalMousePos()
    {
        _oldPos = Input.mousePosition;
        while (true)
        {
            _oldPos = Vector3.Lerp(Input.mousePosition, _oldPos, 0.8f);
//            if (Vector3.Distance(_oldPos, Input.mousePosition) > 0.05f)
            {
                calculate_DirectionAndAngle(_oldPos);
            }
            yield return null;
        }
    }

    public float MaxShootAngle = 75;
    private void calculate_DirectionAndAngle(Vector3 mousePussy)
    {
        // first calculate direction
        var ballPos = _ballManager.ballManagerTransform.position;
        
        try //instead of => if (_camera != null)
        {
            var mousePos = _camera.ScreenToWorldPoint(mousePussy);
            _dir = (mousePos - ballPos).normalized;
        } catch {}

        // then calculate angle
        Angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg - 90f;

        // then normalize angle
        if (Angle < -MaxShootAngle && Angle > -180)
            Angle = -MaxShootAngle;
        else if (Angle > MaxShootAngle && Angle < 90 || Angle < -180 && Angle > -275)
            Angle = MaxShootAngle;

        // then make direction from corrected angle
        var ang = (Angle + 90) * Mathf.Deg2Rad;
        _dir = new Vector2(Mathf.Cos(ang) * 10, Mathf.Sin(ang) * 10);
    }

    // this method is called from "ControllerBallManager" class
    // that class helps to control ball direction when user click on ballManager
    public void TouchOnBallManager(Vector2 firstPos, Vector2 secPos)
    {
        // we do not want shoot new balls when older balls still running in board
        if (_ballManager.isBusy || IsDisable)
        {
            return;
        }
        float rate = GetRateOfVar(firstPos.x, secPos.x);
//        float rate = 3.5f;
        Angle = CalculateAngle(rate);

        if (Angle < -MaxShootAngle)
            Angle = -MaxShootAngle;
        else if (Angle > MaxShootAngle)
            Angle = MaxShootAngle;
        
        var position = _ballManager.ballManagerTransform.position;
        
        /*var ray4Line = Physics2D.Raycast(
            position,
            StandardVelocity(_dir), Mathf.Infinity, _layerMask);*/

        var ray4Shape = Physics2D.CircleCast(position, 0.18f,
            StandardVelocity(_dir),
            Mathf.Infinity, _blockLayerMask);

        var positions = new Vector3[3];
        positions[0] = new Vector3(position.x, position.y);
        positions[1] = new Vector3(ray4Shape.centroid.x, ray4Shape.centroid.y);
        positions[2] = Vector2.Reflect(positions[1] - positions[0],
            ray4Shape.normal);

        DottedLine.Instance.DrawDottedLine(positions[0], positions[1]);
        DottedLine.Instance.DrawDottedLine_withDirection(positions[1], positions[2]);
        ShapeAtEntOfHint.SetActive(true);
        // show a hint ball in the point that we anticipate ball hit
        ShapeAtEntOfHint.transform.position = ray4Shape.centroid;
        
        // then make direction from corrected angle
        var ang = (Angle + 90) * Mathf.Deg2Rad;
        _dir = new Vector2(Mathf.Cos(ang) * 10, Mathf.Sin(ang) * 10);
    }

    private float GetRateOfVar(float first, float second)
    {
        float baseDistance = 3.5f - Mathf.Abs(first);
        float diff = first - second;  // if second > first it will be +
        return diff; // baseDistance;
    }

    float CalculateAngle(float rate)
    {
        return rate * 100 * 0.5f;
    }
}
