﻿using _02_Scripts;
using UnityEditor;
using UnityEngine;

public class ManualController : MonoBehaviour
{
    public Transform Handler, Bar;
    public Camera mCamera;

    private void Start()
    {
        mCamera = Camera.main;
        BallManager.OnBallStart += BallShot;
        BallManager.OnBallStop += BallsReachedDown;
    }

    private void OnDestroy()
    {
        BallManager.OnBallStart -= BallShot;
        BallManager.OnBallStop -= BallsReachedDown;
    }

    private void OnMouseDrag()
    {
        Vector2 touchPos = mCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 handlerPos = Handler.position;

        if (Mathf.Abs(touchPos.x) > 2f)
        {
            if (touchPos.x > 0)
            {
                touchPos = new Vector2(2f, 0f);
            }
            else
            {
                touchPos = new Vector2(-2f, 0f);
            }
        }

        Controller.GetInstance.TouchOnBallManager
            (Vector2.zero, touchPos);
        
        Handler.position = new Vector3(touchPos.x, handlerPos.y);
    }

    private void OnMouseUp()
    {
        Vector2 handlerPos = Handler.position;
        Handler.position = new Vector3(0f, handlerPos.y);
        
        Controller.GetInstance.OnMouseUp();
    }

    private void BallShot()
    {
        gameObject.SetActive(false);
        Bar.gameObject.SetActive(false);
    }

    private void BallsReachedDown()
    {
        Bar.position = new Vector3(0f, Bar.position.y);
        gameObject.SetActive(true);
        Bar.gameObject.SetActive(true);
    }
}
