﻿using System;
using _02_Scripts;
using UnityEngine;

public class ControllerBallManager : MonoBehaviour
{
    private Vector2 _firstPos;
    private Controller _controller;
    private Camera _camera;

    private static ControllerBallManager _instance;
    public static ControllerBallManager Instance => _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        } else if (_instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        _controller = FindObjectOfType<Controller>();
        _camera = Camera.main;
    }

    public void OnMouseDown()
    {
        _firstPos = _camera.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnMouseDrag()
    {
        _controller.TouchOnBallManager(_firstPos, _camera.ScreenToWorldPoint(Input.mousePosition));
    }

    public void OnMouseUp()
    {
        _controller.OnMouseUp();
    }
}
