﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _02_Scripts.ScriptableObjects
{
    /// <summary>
    ///     objects of this class identifies every obstacle in screen
    /// </summary>
    [Serializable]
    public class StageObstacle
    {
        public enum ObstacleType
        {
            LJ, BOXBMB, ROWBMB,
            __,
            O,
            p,
            BRK,
            COLBMB,
            LTBr, RTBr, RDBr, LDBr,
            LTBl, RTBl, RDBl, LDBl,
            SPNG
        }

        public int resistance;

        public ObstacleType type;

        public StageObstacle(ObstacleType type, int resistance)
        {
            this.type = type;
            this.resistance = resistance;
        }
    }

    [Serializable]
    public class StageRow
    {
        public List<StageObstacle> stageObstacles = new List<StageObstacle>();
    }

    public enum StageStatus
    {
        Current,
        Passed,
        Locked
    }

    [Serializable]
    [CreateAssetMenu(fileName = "Stage", menuName = "Stage")]
    public class Stage : ScriptableObject
    {
        // this is number of board rows. for example board is 6*9
        public int boardRows = 10;

        public int col = 7;

        // this is number of this stage rows that has defined by Level designer. it can be equal or greater then boardRows.
        public int gameRows = 10;
        public int initBalls;

        public bool IsFix;

        public List<StageRow> stageBoard = new List<StageRow>();

        public void CreateBoard()
        {
            stageBoard.Clear();
            for (var i = 0; i < gameRows; i++)
            {
                var row = new StageRow();
                for (var j = 0; j < col; j++)
                {
                    var obstacle = new StageObstacle(StageObstacle.ObstacleType.__, 0);
                    row.stageObstacles.Add(obstacle);
                }

                stageBoard.Add(row);
            }
        }
    }
}
