﻿using _02_Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Stage))]
public class LevelDesignEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var stage = (Stage) target;

        var btn = new GUIStyle("button");
        btn.padding = new RectOffset(10, 10, 10, 10);
        btn.margin.left = 32;
        btn.fontStyle = FontStyle.Bold;

        if (GUILayout.Button("Create 'Empty' Board. Use it with caution!", btn)) stage.CreateBoard();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUI.indentLevel++;
//        for (ushort i = 0; i < 1; i++)
//        {
        EditorGUI.indentLevel = 0;

        var tableStyle = new GUIStyle("box") {padding = new RectOffset(10, 10, 10, 10), margin = {left = 32}};

        var headerColumnStyle = new GUIStyle {fixedWidth = 35};

        var columnStyle = new GUIStyle {fixedWidth = 65};

        var rowStyle = new GUIStyle {fixedHeight = 60, fixedWidth = 60};

        var enumStyle = new GUIStyle("popup")
        {
            alignment = TextAnchor.MiddleCenter, fontSize = 14,
            fontStyle = FontStyle.Bold, fixedHeight = 25
        };

        var editStyle = new GUIStyle
        {
            alignment = TextAnchor.LowerCenter, margin = new RectOffset(0, 0, 15, 0),
            fontStyle = FontStyle.Normal, fontSize = 14
        };

        EditorGUILayout.BeginVertical(tableStyle);
        for (var x = 0; x < stage.stageBoard.Count; x++)
        {
            EditorGUILayout.BeginHorizontal();
            for (var y = 0; y < stage.stageBoard[x].stageObstacles.Count; y++)
                if (x >= 0 && y >= 0)
                {
                    EditorGUILayout.BeginVertical(rowStyle);

                    EditorGUILayout.BeginVertical();

                    stage.stageBoard[x].stageObstacles[y].type =
                        (StageObstacle.ObstacleType) EditorGUILayout.EnumPopup
                            (stage.stageBoard[x].stageObstacles[y].type, enumStyle);

                    stage.stageBoard[x].stageObstacles[y].resistance =
                        EditorGUILayout.IntField(stage.stageBoard[x].stageObstacles[y].resistance, editStyle);

                    EditorUtility.SetDirty(target);

                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndVertical();
                }

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndVertical();
    }

//    }
}