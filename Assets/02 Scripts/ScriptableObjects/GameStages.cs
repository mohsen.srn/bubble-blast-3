﻿using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace _02_Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "StageCollection", menuName = "StageCollection")]
    public class GameStages : ScriptableObject
    {
        [BoxGroup("Game Stage and Levels")] 
//        [ReorderableList]
        public List<Stage> stages;
    }
}
