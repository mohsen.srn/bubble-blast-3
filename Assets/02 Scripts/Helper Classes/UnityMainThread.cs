using System;
using System.Collections.Generic;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;

namespace _02_Scripts.Helper_Classes
{
    public class UnityMainThread : MonoBehaviour
    {
        private static UnityMainThread _wrk;
        Queue<Action> _jobs = new Queue<Action>();

        public static UnityMainThread GetInstance => _wrk; 
        void Awake()
        {
            if (_wrk == null)
            {
                _wrk = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }

        private void Update()
        {
            while (_jobs.Count > 0)
            {
                _jobs.Dequeue()?.Invoke();
            }
        }

        public void AddJobs(Action newJob)
        {
            _jobs.Enqueue(newJob);
        }
    }
}
