using System;
using _02_Scripts.Helper_Classes.enums;
using UnityEngine;

namespace _02_Scripts.Helper_Classes
{
    [Serializable]
    public class GameResult
    {
        public GResult Result = GResult.Lose;
        public Stars Stars = 0;
        public int Points = 0;
        public int TotalBlocks;

        public GameResult()
        {
            
        }

        public GameResult(int allBlocks, int takenBlocks)
        {
            if (takenBlocks > allBlocks)
            {
                Points = allBlocks;
            }
            else
            {
                int lost = allBlocks - takenBlocks;
                Points = takenBlocks - lost;
            }

            int half = allBlocks / 2;
            int quarter = allBlocks / 4;

//            Debug.Log("All = " + allBlocks + " // Taken = " + takenBlocks + " // lost = " + lost + " // half = " + half + " // quarter = " + quarter);

            if (Points < quarter)
            {
                Result = GResult.Lose;
                Stars = Stars.No_Star;
            } else if (Points < half)
            {
                Result = GResult.Lose;
                Stars = Stars.One_Star;
            } else if (Points < allBlocks)
            {
                Result = GResult.Win;
                Stars = Stars.Two_Star;
            }
            else
            {
                Result = GResult.Win;
                Stars = Stars.Three_Star;
            }
        }

        // we have two constructor. i took total as float to differentiate between two constructor
        // i calculated takenBlocks from points in the parenthesize 
//        public GameResult(float total, int points)
//            :this((int)total, ((int)total + points) / 2){}

        public GameResult(int allBlocks, int points, int alaki)
        {
//            int takenBlocks = (allBlocks + points) / 2;
//            int lost = allBlocks - takenBlocks;
//            Points = takenBlocks - lost;

            if (points > allBlocks)
            {
                Points = allBlocks;
                Debug.Log("Error! Points are bigger than all blocks -> All blocks = " 
                          + allBlocks + " / Points = " + points);
            }
            else
            {
                Points = points;
            }
            
            int half = allBlocks / 2;
            int quarter = allBlocks / 4;

            if (Points < quarter)
            {
                Result = GResult.Lose;
                Stars = Stars.No_Star;
            } else if (Points < half)
            {
                Result = GResult.Lose;
                Stars = Stars.One_Star;
            } else if (Points < allBlocks)
            {
                Result = GResult.Win;
                Stars = Stars.Two_Star;
            }
            else
            {
                Result = GResult.Win;
                Stars = Stars.Three_Star;
            }
        }
    }
}
