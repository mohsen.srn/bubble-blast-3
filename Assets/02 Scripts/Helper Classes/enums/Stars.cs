namespace _02_Scripts.Helper_Classes.enums
{
    public enum Stars
    {
        No_Star,
        One_Star,
        Two_Star,
        Three_Star
    }
}