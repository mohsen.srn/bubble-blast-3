namespace _02_Scripts.Helper_Classes
{
    public static class PlayerPrefsKeys
    {
        public static string USER_INFO = "UserInfo";
        public static string CONFIG = "Config";
        public static string LEADER_BOARD = "Leader Board";
        public static string NUMBER_USERS_INVITED_YOU = "number of users invited you";
    }
}
