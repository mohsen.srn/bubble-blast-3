﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class UtilityClass
{
    public static int MakeRandNumber(int minNum, int maxNum)
    {
        return Random.Range(minNum, maxNum);
    }

    public static int GetRowNumberFromColumnNumber_Stage(int col)
    {
        switch (col)
        {
            case 6: return 7;
            case 7: return 9;
            case 8: return 11;
            case 9: return 12;
            case 10: return 14; // 13
            case 11: return 16; // 15
            case 12: return 18; // 17
            case 13: return 20;
            case 14: return 21;
            case 15: return 23;
            default: return Mathf.FloorToInt(col * 10 / 7f);
        }
    }

    public static int GetRowNumberFromColumnNumber_Arcade(int col)
    {
        switch (col)
        {
            case 6: return 7;
            case 7:
                return 8; //9;
            case 8:
                return 9; // 10;
            case 9:
                return 10; //11;
            case 10: return 11;
            case 11: return 12;
            case 12: return 13;
            case 13: return 20;
            case 14: return 21;
            case 15: return 23;
            default: return Mathf.FloorToInt(col * 10 / 7f);
        }
    }

    public static bool IsAppInstalled(string bundleID)
    {
        var d = SystemInfo.deviceUniqueIdentifier;
        Debug.Log(d);
        
        if (!Application.isEditor)
        {
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
            Debug.Log(" ********LaunchOtherApp ");
            AndroidJavaObject launchIntent = null;
            //if the app is installed, no errors. Else, doesn't get past next line
            try
            {
                launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID);
                //        
                //        ca.Call("startActivity",launchIntent);
            }
            catch (Exception ex)
            {
                Debug.Log("exception" + ex.Message);
            }

            if (launchIntent == null)
                return false;
            return true;
        }
        else
            return false;
    }

    public static string PersianToEnglish(this string persianStr)
    {
        Dictionary<char, char> lettersDictionary = new Dictionary<char, char>
        {
            ['۰'] = '0', ['۱'] = '1', ['۲'] = '2', ['۳'] = '3', ['۴'] = '4', ['۵'] = '5', ['۶'] = '6', ['۷'] = '7',
            ['۸'] = '8', ['۹'] = '9', ['٬'] = ','
        };
        foreach (var item in persianStr)
        {
            try
            {
                persianStr = persianStr.Replace(item, lettersDictionary[item]);
            }
            catch
            {
                continue;
            }
        }

        return persianStr;
    }
    
    
}