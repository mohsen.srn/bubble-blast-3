using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes
{
    [Serializable]
    public class UserInfo
    {
        public string username;
        public string device_brand;
        public string device_model;
        public string device_mac;
        public string invite_code;
        public int score;
        public string token;
        public string is_blocked;
        public string blocked_reason;
        public string blocked_date;
        public string registered_date;
        public static UserInfo GetUserInfo(string json)
        {
            return JsonUtility.FromJson<UserInfo>(json);
        }
        
        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
