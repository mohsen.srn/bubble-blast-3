using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public class LeaderBoardModel
    {
        public TopUser[] top_users;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class TopUser
    {
        public string username;
        public int score;
        public string cdate;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}