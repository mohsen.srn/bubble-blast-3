using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public struct Step
    {
        public int floor;
        public float minBubble;
        public float maxBubble;

        public Step(int floor, float minBubble, float maxBubble)
        {
            this.floor = floor;
            this.minBubble = minBubble;
            this.maxBubble = maxBubble;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static Step GetDefaults()
        {
            return new Step(0, 0, 0);
        }
    }
}
