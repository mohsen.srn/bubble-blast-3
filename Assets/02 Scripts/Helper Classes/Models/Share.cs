using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public struct Share
    {
        public Share(string englishText4Share, string farsiText4Share,
            string googlePlayLink, string cafeBazaarLink, string myketLink)
        {
            this.englishText4Share = englishText4Share;
            this.farsiText4Share = farsiText4Share;
            this.googlePlayLink = googlePlayLink;
            this.cafeBazaarLink = cafeBazaarLink;
            this.myketLink = myketLink;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static Share GetDefaults()
        {
            return new Share(
                "Download and enjoy Playing Bubble Blast game",
                "بازی جذاب و سرگرم کننده ی حباب شکن رو نصب کن",
                "http://play.google.com/store/apps/details?id=com.dornika.bubbleblast",
                "http://cafebazaar.ir/app/?id=com.dornika.bubbleblast&ref=share",
                "http://myket.ir/app/com.dornika.bubbleblast");
        }

        public string englishText4Share;
        public string farsiText4Share;
        public string googlePlayLink;
        public string cafeBazaarLink;
        public string myketLink;
    }
}
