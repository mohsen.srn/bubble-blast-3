using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public struct Columns
    {
        public Wave wave1;
        public Wave wave2;
        public Wave wave3;
        public Wave wave4;
        public Wave wave5;
        public Wave wave6;
        public Wave wave7;

        public Columns(Wave wave1, Wave wave2, Wave wave3, Wave wave4, Wave wave5, Wave wave6, Wave wave7)
        {
            this.wave1 = wave1;
            this.wave2 = wave2;
            this.wave3 = wave3;
            this.wave4 = wave4;
            this.wave5 = wave5;
            this.wave6 = wave6;
            this.wave7 = wave7;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static Columns GetDefaults()
        {
            return new Columns(Wave.GetDefaults(), Wave.GetDefaults(),
                Wave.GetDefaults(), Wave.GetDefaults(), 
                Wave.GetDefaults(), Wave.GetDefaults(),Wave.GetDefaults());
        }
    }
}
