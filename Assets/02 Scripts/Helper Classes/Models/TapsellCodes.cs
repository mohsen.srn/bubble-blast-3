using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public struct TapsellCodes
    {
        public TapsellCodes(string appId, string bannerId, string interstitialId,
            string videoRewardId, string nativeId)
        {
            this.appId = appId;
            this.bannerId = bannerId;
            this.interstitialId = interstitialId;
            this.videoRewardId = videoRewardId;
            this.nativeId = nativeId;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static TapsellCodes GetDefault()
        {
            return new TapsellCodes(
                "mjlbqmnsseeodfamjkghkrfbgmslbtejeasgkrmdeemfpdjccmshkonefgiiaecdgbikho",
                "5d78dc2a7b9c3b0001160bef",
                "5d7907183397d6000100c12b",
                "5d78da233397d6000100c115",
                "");
        }

        public string appId;
        public string bannerId;
        public string interstitialId;
        public string videoRewardId;
        public string nativeId;
    }
}