using System;
using Model;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public struct BazaarProduct
    {
        public string ProductId;
        public string Title;
        public string Price;
        public string Type;
        public string Description;

        public BazaarProduct(string productId, string title, string price, string type, string description)
        {
            Title = title;
            Price = price;
            Type = type;
            Description = description;
            ProductId = productId;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static BazaarProduct GetDefaults()
        {
            return new BazaarProduct("","","","","");
        }
    }
}
