using System;
using UnityEngine;

namespace _02_Scripts.Helper_Classes.Models
{
    [Serializable]
    public struct AdmobCodes
    {
        public AdmobCodes(string appId, string bannerId, string interstitialId,
            string videoRewardId, string nativeId)
        {
            this.appId = appId;
            this.bannerId = bannerId;
            this.interstitialId = interstitialId;
            this.videoRewardId = videoRewardId;
            this.nativeId = nativeId;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static AdmobCodes GetDefault()
        {
            return new AdmobCodes(
                "ca-app-pub-5265276078374743~1676893654",
                "ca-app-pub-5265276078374743/5974397169",
                "ca-app-pub-5265276078374743/3579619376",
                "ca-app-pub-5265276078374743/5971361887",
                "ca-app-pub-5265276078374743/5430282316");
        }

        public string appId;
        public string bannerId;
        public string interstitialId;
        public string videoRewardId;
        public string nativeId;
    }
}
