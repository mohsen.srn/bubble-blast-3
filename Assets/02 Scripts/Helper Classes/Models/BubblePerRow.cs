using System;
using _02_Scripts.Helper_Classes.Models;
using UnityEngine;

namespace Model
{
    [Serializable]
    public struct BubblePerRow
    {
        public Step step1;
        public Step step2;
        public Step step3;
        public Step step4;
        public Step step5;
        public Step step6;
        public Step step7;
        public Step step8;
        public Step step9;
        public Step step10;
        public Step step11;
        public Step step12;
        public Step step13;
        public Step step14;
        public Step step15;
        public Step step16;
        public Step step17;
        public Step step18;
        public Step step19;
        public Step step20;
        public Step step21;
        public Step step22;
        public Step step23;
        public Step step24;
        public Step step25;
        public Step step26;
        public Step step27;
        public Step step28;
        public Step step29;
        public Step step30;

        public BubblePerRow(Step step1, Step step2, Step step3, Step step4, Step step5, Step step6, Step step7,
            Step step8, Step step9, Step step10, Step step11, Step step12, Step step13, Step step14, Step step15,
            Step step16, Step step17, Step step18, Step step19, Step step20, Step step21, Step step22, Step step23,
            Step step24, Step step25, Step step26, Step step27, Step step28, Step step29, Step step30)
        {
            this.step1 = step1;
            this.step2 = step2;
            this.step3 = step3;
            this.step4 = step4;
            this.step5 = step5;
            this.step6 = step6;
            this.step7 = step7;
            this.step8 = step8;
            this.step9 = step9;
            this.step10 = step10;
            this.step11 = step11;
            this.step12 = step12;
            this.step13 = step13;
            this.step14 = step14;
            this.step15 = step15;
            this.step16 = step16;
            this.step17 = step17;
            this.step18 = step18;
            this.step19 = step19;
            this.step20 = step20;
            this.step21 = step21;
            this.step22 = step22;
            this.step23 = step23;
            this.step24 = step24;
            this.step25 = step25;
            this.step26 = step26;
            this.step27 = step27;
            this.step28 = step28;
            this.step29 = step29;
            this.step30 = step30;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static BubblePerRow GetDefaults()
        {
            return new BubblePerRow(
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults(),
                Step.GetDefaults()
            );
        }
    }
}