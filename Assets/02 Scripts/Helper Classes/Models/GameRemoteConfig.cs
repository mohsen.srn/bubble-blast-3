using System;
using _02_Scripts.Helper_Classes.Models;
using Model;
using UnityEngine;

[Serializable]
public class GameRemoteConfig
{
    public AppConfig app_config;
    public CustomJson custom_json;
    public update_dialog update_dialog;
    public AdmobConfig admob_config;
    public TapsellConfig tapsell_config;
    
    public GameRemoteConfig(AppConfig appConfig, CustomJson customJson, update_dialog updateDialog, AdmobConfig admobConfig, TapsellConfig tapsellConfig)
    {
        app_config = appConfig;
        custom_json = customJson;
        update_dialog = updateDialog;
        admob_config = admobConfig;
        tapsell_config = tapsellConfig;
    }

    /*public static GameRemoteConfig GetDefault()
    {
        return new GameRemoteConfig(
            AppConfig.GetDefaults(), CustomJson.GetDefaults());
    }*/

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
[Serializable]
public class AdmobConfig
{
    public string admob_id;

    public Unit[] units;

    public AdmobConfig(string admobId, Unit[] units)
    {
        admob_id = admobId;
        this.units = units;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public static AdmobConfig GetDefault()
    {
        return new AdmobConfig(
            "ca-app-pub-5265276078374743~1676893654",
            new []
            {
                new Unit("BannerId", "ca-app-pub-5265276078374743/5974397169", "","Banner", true),
                new Unit("InterstitialID", "ca-app-pub-5265276078374743/3579619376", "","Interstitial", true),
                new Unit("VideoRewardId", "ca-app-pub-5265276078374743/5971361887", "","VideoRewardId", true),
                new Unit("NativeId", "ca-app-pub-5265276078374743/5430282316", "","NativeId", true),
            });
    }
}
[Serializable]
public class Unit
{
    public string title;
    public string unit_id;
    public string unit_name;
    public string unit_type;
    public bool status;

    public Unit(string title, string unitId, string unitName, string unitType, bool status)
    {
        this.title = title;
        unit_id = unitId;
        unit_name = unitName;
        unit_type = unitType;
        this.status = status;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public static Unit GetDefault()
    {
        return new Unit("","","","",true);
    }
}
[Serializable]
public class AppConfig
{
    public string version_name;
    public long version_code;
    public string terms_message;
    public string guide_message;
    public string welcome_message;
    public string share_body;
    public string contact_email;
    public string contact_mobile;
    public string contact_telegram;
    public string contact_instagram;
    public string contact_website;
    public string direct_apk_url;
    public string market_url;
    public string developer_id;
    public string ads_type;
    public bool show_ads;
    public bool status;
    public string invite_coin;

    public AppConfig(
        string versionName,
        long versionCode,
        string termsMessage,
        string guideMessage,
        string welcomeMessage,
        string shareBody,
        string contactEmail,
        string contactMobile,
        string contactTelegram, 
        string contactInstagram, 
        string contactWebsite, 
        string directApkUrl, 
        string marketUrl, 
        string developerId, 
        string adsType, 
        bool showAds, 
        bool status)
    {
        version_name = versionName;
        version_code = versionCode;
        terms_message = termsMessage;
        guide_message = guideMessage;
        welcome_message = welcomeMessage;
        share_body = shareBody;
        contact_email = contactEmail;
        contact_mobile = contactMobile;
        contact_telegram = contactTelegram;
        contact_instagram = contactInstagram;
        contact_website = contactWebsite;
        direct_apk_url = directApkUrl;
        market_url = marketUrl;
        developer_id = developerId;
        ads_type = adsType;
        show_ads = showAds;
        this.status = status;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public static AppConfig GetDefaults()
    {
        return new AppConfig("",0,"","",
            "","","","","",
            "","","","","",
            "",true,true);
    }
}
[Serializable]
public class CustomJson
{
    public LevelDifficultyBubblePerRow LevelDifficultyBubblePerRow;
    public LevelDifficultyColumns LevelDifficultyColumns;

    public CustomJson(LevelDifficultyBubblePerRow levelDifficultyBubblePerRow, LevelDifficultyColumns levelDifficultyColumns)
    {
        LevelDifficultyBubblePerRow = levelDifficultyBubblePerRow;
        LevelDifficultyColumns = levelDifficultyColumns;
    }

    public static CustomJson GetDefault()
    {
        return new CustomJson(LevelDifficultyBubblePerRow.GetDefaults(), 
            LevelDifficultyColumns.GetDefault());
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
[Serializable]
public class LevelDifficultyBubblePerRow
{
    public Step step1;
    public Step step2;
    public Step step3;
    public Step step4;
    public Step step5;
    public Step step6;
    public Step step7;
    public Step step8;
    public Step step9;
    public Step step10;
    public Step step11;
    public Step step12;
    public Step step13;
    public Step step14;
    public Step step15;
    public Step step16;
    public Step step17;
    public Step step18;
    public Step step19;
    public Step step20;
    public Step step21;
    public Step step22;
    public Step step23;
    public Step step24;
    public Step step25;
    public Step step26;
    public Step step27;
    public Step step28;
    public Step step29;
    public Step step30;

    public LevelDifficultyBubblePerRow(Step step1, Step step2, Step step3, Step step4, Step step5, Step step6,
        Step step7,
        Step step8, Step step9, Step step10, Step step11, Step step12, Step step13, Step step14, Step step15,
        Step step16, Step step17, Step step18, Step step19, Step step20, Step step21, Step step22, Step step23,
        Step step24, Step step25, Step step26, Step step27, Step step28, Step step29, Step step30)
    {
        this.step1 = step1;
        this.step2 = step2;
        this.step3 = step3;
        this.step4 = step4;
        this.step5 = step5;
        this.step6 = step6;
        this.step7 = step7;
        this.step8 = step8;
        this.step9 = step9;
        this.step10 = step10;
        this.step11 = step11;
        this.step12 = step12;
        this.step13 = step13;
        this.step14 = step14;
        this.step15 = step15;
        this.step16 = step16;
        this.step17 = step17;
        this.step18 = step18;
        this.step19 = step19;
        this.step20 = step20;
        this.step21 = step21;
        this.step22 = step22;
        this.step23 = step23;
        this.step24 = step24;
        this.step25 = step25;
        this.step26 = step26;
        this.step27 = step27;
        this.step28 = step28;
        this.step29 = step29;
        this.step30 = step30;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public static LevelDifficultyBubblePerRow GetDefaults()
    {
        return JsonUtility.FromJson<LevelDifficultyBubblePerRow>(GameManager.GetInstance.bubblePerRow.ToString());
    }
}
[Serializable]
public class LevelDifficultyColumns
{
    public Wave wave1;
    public Wave wave2;
    public Wave wave3 ;
    public Wave wave4 ;
    public Wave wave5 ;
    public Wave wave6 ;
    public Wave wave7 ;

    public LevelDifficultyColumns(Wave wave1, Wave wave2, Wave wave3, Wave wave4, Wave wave5, Wave wave6, Wave wave7)
    {
        this.wave1 = wave1;
        this.wave2 = wave2;
        this.wave3 = wave3;
        this.wave4 = wave4;
        this.wave5 = wave5;
        this.wave6 = wave6;
        this.wave7 = wave7;
    }

    public static LevelDifficultyColumns GetDefault()
    {
        return JsonUtility.FromJson<LevelDifficultyColumns>(GameManager.GetInstance.columnNumbers.ToString());
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
/*[Serializable]
public class Wave
{
    public long floor;

    public long columns;

    public Wave(long floor, long columns)
    {
        this.floor = floor;
        this.columns = columns;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}*/
[Serializable]
public class TapsellConfig
{
    public string tapsell_id;

    public Unit[] units;

    public TapsellConfig(string tapsellId, Unit[] units)
    {
        tapsell_id = tapsellId;
        this.units = units;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }

    public static TapsellConfig GetDefault()
    {
        return new TapsellConfig(
            "mjlbqmnsseeodfamjkghkrfbgmslbtejeasgkrmdeemfpdjccmshkonefgiiaecdgbikho",
            new []
            {
                new Unit("BannerId", "5d78dc2a7b9c3b0001160bef", "","Banner", true),
                new Unit("InterstitialId", "5d7907183397d6000100c12b", "","Interstitial", true),
                new Unit("VideoRewardId", "5d78da233397d6000100c115", "","VideoReward", true),
            });
    }
}
[Serializable]
public class update_dialog
{
    public string title;

    public string message;

    public string[] buttons;

    public update_dialog(string title, string message, string[] buttons)
    {
        this.title = title;
        this.message = message;
        this.buttons = buttons;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
