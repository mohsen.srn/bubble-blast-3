using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public struct Wave
    {
        public int floor;
        public int columns;

        public Wave(int floor, int columns)
        {
            this.floor = floor;
            this.columns = columns;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }

        public static Wave GetDefaults()
        {
            return new Wave(0, 7);
        }
    }
}
