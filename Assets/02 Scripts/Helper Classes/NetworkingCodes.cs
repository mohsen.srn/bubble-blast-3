using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using _02_Scripts._00_Home.Dialogs;
using _02_Scripts.Helper_Classes.Models;
using UnityEngine;
using UnityEngine.Networking;

namespace _02_Scripts.Helper_Classes
{
    public class NetworkingCodes : MonoBehaviour
    {
        /****************************** Singleton **************************/
        public static NetworkingCodes GetInstance;
        private void Awake()
        {
            if (GetInstance == null)
            {
                GetInstance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        /********************************************************************/

        public string Api_App_Get_Config =       "https://appconfig.ir/api/v1/app/config";
        public string Api_User_Register =        "https://appconfig.ir/api/v1/app/users/register";
        public string Api_Useres_Add_Score =     "https://appconfig.ir/api/v1/app/users/score";
        public string Api_Useres_Top_Score =     "https://appconfig.ir/api/v1/app/users/score";
        public string Api_Users_Get_My_Invites = "https://appconfig.ir/api/v1/app/users/invites";
        public string Api_Users_Check_Invite_Code = "https://appconfig.ir/api/v1/app/users/invites";
        void Start()
        {
            StartCoroutine(GetRemoteConfig());
            SetRecord();
            GetLeaderBoard();
        }
        
        IEnumerator GetRemoteConfig()
        {
            UnityWebRequest www = UnityWebRequest.Get(Api_App_Get_Config);
            www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("NetworkingCodes.GetRemoteConfig() -> " + www.error + " / " + www.downloadHandler.text);

                // if encounter error, we load defaults
                GameManager.GetInstance.LoadConfig();
            }
            else
            {
                string text = www.downloadHandler.text;
                GameRemoteConfig gameRemoteConfig = JsonUtility.FromJson<GameRemoteConfig>(text);

                // if connection was successful and everything  was ok set config to json received
                GameManager.GetInstance.LoadConfig(gameRemoteConfig);
            }
        }

        public void RegisterUser()
        {
            StartCoroutine(RegisterUserCoroutine());
        }
        IEnumerator RegisterUserCoroutine()
        {
            // if never registered this user before
            GameManager.GetInstance.InitUserInfo();
            UserInfo userInfo = UserInfo.GetUserInfo(PlayerPrefs.GetString(PlayerPrefsKeys.USER_INFO));

            if (userInfo.token.Equals(""))
            {
                WWWForm form = new WWWForm();
                form.AddField("username", userInfo.username);
                form.AddField("device_brand", userInfo.device_brand);
                form.AddField("device_model", userInfo.device_model);
                form.AddField("device_mac", userInfo.device_mac);
            
                UnityWebRequest www = UnityWebRequest.Post(Api_User_Register, form);
                www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log("NetworkingCodes.RegisterUserCoroutine() -> " + www.error + " / " + www.downloadHandler.text);
                }
                else
                {
                    Debug.Log("Form upload complete!");

                    string response = www.downloadHandler.text;
                    UserInfo info = JsonUtility.FromJson<UserInfo>(response);

                    PlayerPrefs.SetString(PlayerPrefsKeys.USER_INFO, info.ToString());
                    PlayerPrefs.Save();
                    
                    FindObjectOfType<HomeUI>().Click_OpenInviteDialog();
                }
            }
        }

        public void SetRecord()
        {
            if (PlayerPrefs.HasKey("Best Record"))
            {
                StartCoroutine(SendRecordCoroutine(PlayerPrefs.GetInt("Best Record")));
            }
            else
            {
                print("NetworkingCodes.SetRecord() -> NO PlayerPrefs.HasKey(\"Best Record\")");
            }
            
            GetLeaderBoard();
        }
        IEnumerator SendRecordCoroutine(int record)
        {
            WWWForm form = new WWWForm();
            form.AddField("my_token", GameManager.GetInstance.GetUserInfo().token);
            form.AddField("score", record);
            
            UnityWebRequest www = UnityWebRequest.Post(Api_Useres_Add_Score, form);
            www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("NetworkingCodes.SendRecordCoroutine() -> " + www.error + " / " + www.downloadHandler.text);
            }
            else
            {
                Debug.Log("Form upload complete! => Record registered");
            }
        }

        public void GetLeaderBoard()
        {
            StartCoroutine(GetLeaderBoardCoroutine());
        }
        IEnumerator GetLeaderBoardCoroutine()
        {
            UnityWebRequest www = UnityWebRequest.Get(Api_Useres_Top_Score);
            www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("NetworkingCodes.GetLeaderBoardCoroutine() -> " + www.error + " / " + www.downloadHandler.text);
            }
            else
            {
                string text = www.downloadHandler.text;
                LeaderBoardModel leaderBoard = JsonUtility.FromJson<LeaderBoardModel>(text);
                
                PlayerPrefs.SetString(PlayerPrefsKeys.LEADER_BOARD, leaderBoard.ToString());
                PlayerPrefs.Save();
            }
        }

        public void UseInviteCode(string code, Action<int> onSuccess, Action<string> onFail)
        {
            StartCoroutine(SendInviteCodeRequest(code, onSuccess, onFail));
        }

        IEnumerator SendInviteCodeRequest(string code, Action<int> onSuccess, Action<string> onFail)
        {
            WWWForm form = new WWWForm();
            form.AddField("my_token", GameManager.GetInstance.GetUserInfo().token);
            form.AddField("invite_code", code);
            
            UnityWebRequest www = UnityWebRequest.Post(Api_Users_Check_Invite_Code, form);
            www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("NetworkingCodes.GetLeaderBoardCoroutine() -> " + 
                          www.error + " / " + 
                          www.downloadHandler.text);
                ErrorMsg errorMsg = JsonUtility.FromJson<ErrorMsg>(www.downloadHandler.text);

                if (errorMsg.message.Trim().Equals("invalid invite code :/"))
                {
                    onFail?.Invoke("کد دعوت معبر نیست :/");
                }
                else
                {
                    onFail?.Invoke(errorMsg.message);
                }
            }
            else
            {
                string text = www.downloadHandler.text;
                CheckInviteCodeModel inviteCodeModel = JsonUtility.FromJson<CheckInviteCodeModel>(text);
                
                print("success => " + text);
                
                onSuccess?.Invoke(inviteCodeModel.received_coins);
            }
        }

        public void GetMyInvites()
        {
            StartCoroutine(GetMyInvitesCoroutine());
        }

        IEnumerator GetMyInvitesCoroutine()
        {
            string url = Api_Users_Get_My_Invites + "?my_token=" + GameManager.GetInstance.GetUserInfo().token;
            print(url);
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SetRequestHeader("token", GameManager.GetInstance.releaseToken);
            
            yield return www.SendWebRequest();
            
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("NetworkingCodes.GetRemoteConfig() -> " + www.error + " / " + www.downloadHandler.text);
            }
            else
            {
                string text = www.downloadHandler.text;
                print(text);
                UsersInvitedYou invitedYou = JsonUtility.FromJson<UsersInvitedYou>(text);

                if (!PlayerPrefs.HasKey(PlayerPrefsKeys.NUMBER_USERS_INVITED_YOU))
                {
                    PlayerPrefs.SetInt(PlayerPrefsKeys.NUMBER_USERS_INVITED_YOU, invitedYou.my_invites.Length);
                    PlayerPrefs.Save();

                    StartCoroutine(IncreaseCoinsDueToInvitation(invitedYou.my_invites.Length));
                }
                else
                {
                    int oldOne = PlayerPrefs.GetInt(PlayerPrefsKeys.NUMBER_USERS_INVITED_YOU);

                    int diff = invitedYou.my_invites.Length - oldOne;
                    if (diff > 0)
                    {
                        for (int i = 0; i < diff; i++)
                        {
                            StartCoroutine(IncreaseCoinsDueToInvitation(diff));
                        }
                    }
                    
                    PlayerPrefs.SetInt(PlayerPrefsKeys.NUMBER_USERS_INVITED_YOU, invitedYou.my_invites.Length);
                    PlayerPrefs.Save();
                }
            }
        }

        IEnumerator IncreaseCoinsDueToInvitation(int a)
        {
            for (int i = 0; i < a; i++)
            {
                CoinManager.GetInstance.IncreaseCoins(GameManager.GetInstance.inviteCoins);
                yield return new WaitForSeconds(2f);
            }
        }
    }

    [Serializable]
    public class CheckInviteCodeModel
    {
        public string status;
        public int received_coins;
        public string total_coins;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class ErrorMsg
    {
        public string status;
        public string message;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class UsersInvitedYou
    {
        public InviteUser[] my_invites;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class InviteUser
    {
        public string username;
        public int score;
        public string cdate;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
