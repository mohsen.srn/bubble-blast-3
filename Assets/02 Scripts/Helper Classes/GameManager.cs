﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using _02_Scripts._Extenal_library_related_scripts;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.enums;
using _02_Scripts.Helper_Classes.Models;
using Model;
using NaughtyAttributes;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    /************************ Singleton ***********************/
    private static GameManager _instance;
    public static GameManager GetInstance => _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    /*********************************************************/
    public enum ReleaseMode
    {
        Debug,
        GooglePlay,
        CafeBazaar,
        Myket
    }

    public ReleaseMode releaseMode;
    public string releaseToken;

    private GameRemoteConfig _config;
    private UserInfo _userInfo;

    private void Start()
    {
        // first we load data that we received form internet before
        // bug: we have a bug here. when internet is offline, on first attempt, Board does not update
        // bug: so i should remember to not update number of columns remotely
        LoadConfig();
    }

    /// <summary>
    /// this method specifies some important fields of systemInfo. only ones
    /// </summary>
    public void InitUserInfo(string userNameParameter = "")
    {
        if (!PlayerPrefs.HasKey(PlayerPrefsKeys.USER_INFO))
        {
            UserInfo userInfo = new UserInfo()
            {
                device_mac = SystemInfo.deviceUniqueIdentifier,
                device_brand = SystemInfo.deviceModel,
                device_model = SystemInfo.deviceName,
                username = userNameParameter,
                token = "",
                invite_code = ""
            };
            PlayerPrefs.SetString(PlayerPrefsKeys.USER_INFO, userInfo.ToString());
            PlayerPrefs.Save();
        }
    }

    public UserInfo GetUserInfo()
    {
        if (_userInfo == null)
        {
            _userInfo = JsonUtility.FromJson<UserInfo>(PlayerPrefs.GetString(PlayerPrefsKeys.USER_INFO));
        }

        return _userInfo;
    }

    public void LoadConfig()
    {
        if (PlayerPrefs.HasKey(PlayerPrefsKeys.CONFIG))
        {
            _config = JsonUtility.FromJson<GameRemoteConfig>
                (PlayerPrefs.GetString(PlayerPrefsKeys.CONFIG));
            InitProperties();
        }
    }

    public void LoadConfig(GameRemoteConfig config)
    {
        _config = config;
        PlayerPrefs.SetString(PlayerPrefsKeys.CONFIG, JsonUtility.ToJson(config));
        PlayerPrefs.Save();

        InitProperties();
    }

    private void InitProperties()
    {
        forceUpdate = !_config.app_config.status;
        if (adType != _config.app_config.ads_type)
        {
            adType = _config.app_config.ads_type;
//            FindObjectOfType<AdBuilder>().RebuildAdManager();
        }

        gameAdmobCodes.appId = _config.admob_config.admob_id;
        gameAdmobCodes.bannerId = _config.admob_config.units[0].unit_id;
        gameAdmobCodes.interstitialId = _config.admob_config.units[1].unit_id;
        gameAdmobCodes.videoRewardId = _config.admob_config.units[2].unit_id;

        tapsellCodes.appId = _config.tapsell_config.tapsell_id;
        tapsellCodes.bannerId = _config.tapsell_config.units[0].unit_id;
        tapsellCodes.interstitialId = _config.tapsell_config.units[1].unit_id;
        tapsellCodes.videoRewardId = _config.tapsell_config.units[2].unit_id;

        shareData.farsiText4Share = _config.app_config.share_body;

        showAds = _config.app_config.show_ads;

        inviteCoins = Int32.Parse(_config.app_config.invite_coin);

        // here we init AdManager -> this is important to init ad after retrieving data from internet
        if (AdManager.GetInstance != null)
        {
            AdManager.GetInstance.InitAd();
        }

        columnNumbers.wave1 = _config.custom_json.LevelDifficultyColumns.wave1;
        columnNumbers.wave2 = _config.custom_json.LevelDifficultyColumns.wave2;
        columnNumbers.wave3 = _config.custom_json.LevelDifficultyColumns.wave3;
        columnNumbers.wave4 = _config.custom_json.LevelDifficultyColumns.wave4;
        columnNumbers.wave5 = _config.custom_json.LevelDifficultyColumns.wave5;
        columnNumbers.wave6 = _config.custom_json.LevelDifficultyColumns.wave6;
        columnNumbers.wave7 = _config.custom_json.LevelDifficultyColumns.wave7;

        bubblePerRow.step1 = _config.custom_json.LevelDifficultyBubblePerRow.step1;
        bubblePerRow.step2 = _config.custom_json.LevelDifficultyBubblePerRow.step2;
        bubblePerRow.step3 = _config.custom_json.LevelDifficultyBubblePerRow.step3;
        bubblePerRow.step4 = _config.custom_json.LevelDifficultyBubblePerRow.step4;
        bubblePerRow.step5 = _config.custom_json.LevelDifficultyBubblePerRow.step5;
        bubblePerRow.step6 = _config.custom_json.LevelDifficultyBubblePerRow.step6;
        bubblePerRow.step7 = _config.custom_json.LevelDifficultyBubblePerRow.step7;
        bubblePerRow.step8 = _config.custom_json.LevelDifficultyBubblePerRow.step8;
        bubblePerRow.step9 = _config.custom_json.LevelDifficultyBubblePerRow.step9;
        bubblePerRow.step10 = _config.custom_json.LevelDifficultyBubblePerRow.step10;
        bubblePerRow.step11 = _config.custom_json.LevelDifficultyBubblePerRow.step11;
        bubblePerRow.step12 = _config.custom_json.LevelDifficultyBubblePerRow.step12;
        bubblePerRow.step13 = _config.custom_json.LevelDifficultyBubblePerRow.step13;
        bubblePerRow.step14 = _config.custom_json.LevelDifficultyBubblePerRow.step14;
        bubblePerRow.step15 = _config.custom_json.LevelDifficultyBubblePerRow.step15;
        bubblePerRow.step16 = _config.custom_json.LevelDifficultyBubblePerRow.step16;
        bubblePerRow.step17 = _config.custom_json.LevelDifficultyBubblePerRow.step17;
        bubblePerRow.step18 = _config.custom_json.LevelDifficultyBubblePerRow.step18;
        bubblePerRow.step19 = _config.custom_json.LevelDifficultyBubblePerRow.step19;
        bubblePerRow.step20 = _config.custom_json.LevelDifficultyBubblePerRow.step20;
        bubblePerRow.step21 = _config.custom_json.LevelDifficultyBubblePerRow.step21;
        bubblePerRow.step22 = _config.custom_json.LevelDifficultyBubblePerRow.step22;
        bubblePerRow.step23 = _config.custom_json.LevelDifficultyBubblePerRow.step23;
        bubblePerRow.step24 = _config.custom_json.LevelDifficultyBubblePerRow.step24;
        bubblePerRow.step25 = _config.custom_json.LevelDifficultyBubblePerRow.step25;
        bubblePerRow.step26 = _config.custom_json.LevelDifficultyBubblePerRow.step26;
        bubblePerRow.step27 = _config.custom_json.LevelDifficultyBubblePerRow.step27;
        bubblePerRow.step28 = _config.custom_json.LevelDifficultyBubblePerRow.step28;
        bubblePerRow.step29 = _config.custom_json.LevelDifficultyBubblePerRow.step29;
        bubblePerRow.step30 = _config.custom_json.LevelDifficultyBubblePerRow.step30;
    }

    [BoxGroup("Update")] public bool updateAvailable = false;
    [BoxGroup("Update")] public bool forceUpdate = false;

    [BoxGroup("Advertisement")] public bool showAds = false;
    [BoxGroup("Advertisement")] public string adType = "admob";
    [BoxGroup("Advertisement")] public AdmobCodes gameAdmobCodes;
    [BoxGroup("Advertisement")] public TapsellCodes tapsellCodes;

    [BoxGroup("Share")] public Share shareData;

    [BoxGroup("Difficulty")] public Columns columnNumbers;
    [BoxGroup("Difficulty")] public BubblePerRow bubblePerRow;

    public int inviteCoins = 200;
}