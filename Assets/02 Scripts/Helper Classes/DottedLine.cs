﻿using System.Collections.Generic;
using UnityEngine;

public class DottedLine : MonoBehaviour
{
    // Inspector fields
    public Sprite Dot;
    [Range(0.01f, 1f)] public float Size = 1;
    [Range(0.1f, 2f)] public float Delta = 1;

    // this property used for add some animation to dotes by scaling some of them
    private int _counter;

    //Static Property with backing field
    private static DottedLine _instance;

    public static DottedLine Instance => _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    //Utility fields
    List<Vector2> positions = new List<Vector2>();
    List<Transform> dots = new List<Transform>();

    // Update is called once per frame
    void FixedUpdate()
    {
        if (positions.Count > 0)
        {
            DestroyAllDots();
            positions.Clear();
        }
    }

    private void DestroyAllDots()
    {
        for (var index = 0; index < dots.Count; index++)
        {
            var dot = dots[index];
            PoolManager.Instance.Back2DotPool(dot);
        }

        dots.Clear();
    }

    Transform GetOneDot(Vector3 scale, Vector3 position)
    {
        return PoolManager.Instance.GetDot(scale, position);
    }

    public void DrawDottedLine(Vector2 start, Vector2 end)
    {
        DestroyAllDots();

        Vector2 point = start;
        Vector2 direction = (end - start).normalized;

        int avoidInfinitiveLoop = 0;
        while ((end - start).magnitude > (point - start).magnitude)
        {
            positions.Add(point);
            point += (direction * Delta);
            
            // this if id for avoid loop trap
            if (avoidInfinitiveLoop++ > 40)
            {
                break;
            }
        }

        Render();
    }

    public void DrawDottedLine_withDirection(Vector2 start, Vector2 direct)
    {
        DestroyAllDots();

        Vector2 point = start;
        Vector2 direction = direct.normalized; // (end - start).normalized;

        int avoidInfinitiveLoop = 0;
        while ((direct - start).magnitude > (point - start).magnitude)
        {
            positions.Add(point);
            point += (direction * Delta);

            // this if id for avoid loop trap
            if (avoidInfinitiveLoop++ > 40)
            {
                break;
            }
        }

        Render();
    }

    private void Render()
    {
        for (var index = 0; index < positions.Count; index++)
        {
            Vector3 scale;
            var position = positions[index];
            
            switch (_counter - index)
            {
                case 0:
                    scale = 2.3f * Size * Vector2.one;
                    break;
                case 1:
                    scale = 2.2f * Size * Vector2.one;
                    break;
                case 2:
                    scale = 2.1f * Size * Vector2.one;
                    break;
                case 4:
                    scale = 2f * Size * Vector2.one;
                    break;
                default:
                    scale = Vector2.one * Size;
                    break;
            }
            var g = GetOneDot(scale, position);
            dots.Add(g);
        }

        if (_counter > 3 * positions.Count)
            _counter = 0;
        else
            _counter++;
    }
}
