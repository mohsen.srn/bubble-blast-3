﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenChanger : MonoBehaviour
{
    /**************** Singleton maker section ******************/
    public static ScreenChanger GetInstance;
    private void Awake()
    {
        if(GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }
    /************************************************************/
    private string _nextScene;

    private Animator _animator;
    void Start()
    {
        _animator = GetComponent<Animator>();
        // SceneManager.activeSceneChanged += OpenBubbleWall; // for now i comment it
    }

    public void Go2Scene(string nextScene)
    {
        _nextScene = nextScene;
        // _animator.SetTrigger("close"); // I comment it for now
        
        OpenNextScene();
    }

    public void OpenNextScene()
    {
        SceneManager.LoadScene(_nextScene);
    }

    private void OpenBubbleWall(Scene scene1, Scene scene2)
    {
        _animator.SetTrigger("open");
    }
}
