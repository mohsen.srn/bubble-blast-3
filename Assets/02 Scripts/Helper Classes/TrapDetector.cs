﻿using _02_Scripts.Obstacles;
using UnityEngine;

public class TrapDetector
{
    private const int HitWallLimit = 20;
    private const int HitBrickLimit = 50;
    
    private readonly GameObject _ball;
    private int _wallHit;
    private bool _unBreakableTrap;

    private Rigidbody2D _ballRigidbody;

    public TrapDetector(GameObject ball)
    {
        _ball = ball;
        _ballRigidbody = _ball.GetComponent<Rigidbody2D>();
    }

    public void HitWall()
    {
        _wallHit++;
        if (_wallHit > HitWallLimit)
        {
            _ballRigidbody.gravityScale = 0.2f;
//            _wallHit = 0;
        }
    }

    public void HitBlock()
    {
        _wallHit = 0;
        _unBreakableTrap = false;
    }

    public void HitGround()
    {
        _wallHit = 0;
        _unBreakableTrap = false;
    }
    
    public void HitUnbreakableObstacle(Obstacle obstacle)
    {
        _wallHit++;
        _unBreakableTrap = true;
        if (_wallHit > HitBrickLimit)
        {
            _wallHit = 0;
            obstacle.Eliminate();

            HitBlock();
        }
    }
}
