﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ShareHelper : MonoBehaviour
{
    public void ShareGame()
    {
        GoogleAnalyticsManager.GetInstance.OnShareBtnClicked();

        string shareTextEn = GameManager.GetInstance.shareData.englishText4Share;
        string shareTextFa = GameManager.GetInstance.shareData.farsiText4Share;

        string shareLinkGooglePlay = GameManager.GetInstance.shareData.googlePlayLink;
        string shareLinkCafeBazaar = GameManager.GetInstance.shareData.cafeBazaarLink;

        string shareLinkMyket = GameManager.GetInstance.shareData.myketLink;

        string subject = "Bubble blast";

        string fa_InviteCode = "کد دعوت برای دریافت سکه رایگان: " + GameManager.GetInstance.GetUserInfo().invite_code;
        string en_InviteCode = "Invite code to take free coins: " + GameManager.GetInstance.GetUserInfo().invite_code;

        string body;
        if (GameManager.GetInstance.releaseMode == GameManager.ReleaseMode.Myket)
        {
            body = (SettingManager.Instance.IsMyketInstalled()
                ? shareTextFa + "\n" + "\n" + fa_InviteCode + "\n" + "\n" + shareLinkMyket
                : shareTextEn + "\n" + "\n" + en_InviteCode + "\n" + "\n" + shareLinkGooglePlay);
        }
        else
        {
            body = (SettingManager.Instance.IsBazaarInstalled()
                ? shareTextFa + "\n" + "\n" + fa_InviteCode + "\n" + "\n" + shareLinkCafeBazaar
                : shareTextEn + "\n" + "\n" + en_InviteCode + "\n" + "\n" + shareLinkGooglePlay);
        }

        // for release in Cafebazzar 
//        string body = shareTextFa + "\n" + (shareLinkCafeBazaar);

        //execute the below lines if being run on a Android device
        if (!Application.isEditor)
        {
            //Refernece of AndroidJavaClass class for intent
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            //Refernece of AndroidJavaObject class for intent
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            //call setAction method of the Intent object created
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            //set the type of sharing that is happening
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            //add data to be passed to the other activity i.e., the data to be sent
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
            //get the current activity
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            //start the activity by sending the intent data
            currentActivity.Call("startActivity", intentObject);
        }
    }
}
