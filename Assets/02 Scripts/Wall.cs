﻿using System.Collections;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public enum WallType{Right, Left, Top, Down}
    public WallType type;
    public Transform WallImage;
    public BoxCollider2D _collider2D;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == 8)
            other.gameObject.GetComponent<Ball>().trap.HitWall();
    }

    public void Start()
    {
        StartCoroutine(init());
    }

    private IEnumerator init()
    {
//        yield return new WaitForSeconds(0.1f);

        var size = _collider2D.size;
        float offsetX = size.x / 2;
        float offsetY = size.y / 2;

        if (WallImage != null)
            switch (type)
            {
                case WallType.Right:
                    WallImage.position = transform.position - new Vector3(offsetX/*-0.1f*/, 0f, 0f);
                    break;
                case WallType.Left:
                    WallImage.position = transform.position + new Vector3(offsetX/*-0.1f*/, 0f, 0f);
                    break;
                case WallType.Top:
                    WallImage.position = transform.position - new Vector3(0f, offsetY, 0f);
                    break;
                case WallType.Down:
                    WallImage.position = transform.position - new Vector3(0f, offsetY, 0f);
                    break;
            }
        
        yield return null;
    }
}
