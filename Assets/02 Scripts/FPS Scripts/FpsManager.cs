﻿using System;
using _02_Scripts;
using UnityEngine;

/// <summary>
/// Create an empty gameObject in scene and assign this Script to it
/// this gameObject limits FPS.
/// less FPS == less CPU/GPU usage == less battery usage
/// </summary>
public class FpsManager : MonoBehaviour {
	public static FpsManager Instance;
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		} else if (Instance != this)
		{
			Destroy(this);
		}
	}

	// Set appropriate FPS for every scene
	public int FPS = 30;
    public int MaxFps = 60;
    public int MidFps = 30;
    public int MinFps = 15;

    void Start () {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = MidFps;

        BallManager.OnBallStart += SetMaxFps;
        BallManager.OnBallStop += SetLowFps;
        Controller.OnScreenTouch += SetMidFps;
    }

    private void OnDestroy()
    {
	    BallManager.OnBallStart -= SetMaxFps;
	    BallManager.OnBallStop -= SetLowFps;
	    Controller.OnScreenTouch -= SetMidFps;
    }

	void Update () {
		if(FPS != Application.targetFrameRate)
            Application.targetFrameRate = FPS;
    }

	private void SetMaxFps()
	{
		FPS = MaxFps;
	}

	private void SetLowFps()
	{
		FPS = MinFps;
	}	
	
	private void SetMidFps()
	{
		FPS = MidFps;
	}
}
