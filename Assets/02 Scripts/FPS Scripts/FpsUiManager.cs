﻿using UnityEngine;
using UnityEngine.UI;

public class FpsUiManager : MonoBehaviour
{
    public bool enableFPS;

    private void Start()
    {
        FpsManager.Instance.gameObject.SetActive(enableFPS);
    }

    public void IncreaseFPS()
    {
        FpsManager.Instance.FPS++;
    }
    
    public void DecreaseFPS()
    {
        FpsManager.Instance.FPS--;
    }

    public void ToggleFPSManager()
    {
        FpsManager.Instance.gameObject.SetActive(!enableFPS);
        enableFPS = !enableFPS;
    }

    public void SetFps(int fps)
    {
        FpsManager.Instance.FPS = fps;
    }
}
