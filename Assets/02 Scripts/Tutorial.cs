﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Sprite[] sprites = new Sprite[5];
    public GameObject DarkPanel;
    public Image TurorialImage;

    private int _tutorialIndex = 0;

    public delegate void TutorialActions();
    public static event TutorialActions OnOpenTutorial;
    public static event TutorialActions OnCloseTutorial;

    public static Tutorial Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DarkPanel.SetActive(false);
            TurorialImage.gameObject.SetActive(false);
        } else
        {
            Destroy(this);
        }
    }

    public void NextTutorial()
    {
        _tutorialIndex++;
        if (_tutorialIndex < 5)
        {
            TurorialImage.sprite = sprites[_tutorialIndex];
        }
        else
        {
            CloseTutorial();
        }
    }

    public void OpenTutorial()
    {
        OnOpenTutorial?.Invoke();
        _tutorialIndex = 0;
        TurorialImage.sprite = sprites[_tutorialIndex];
        DarkPanel.SetActive(true);
        TurorialImage.gameObject.SetActive(true);
    }

    public void CloseTutorial()
    {
        OnCloseTutorial?.Invoke();
        TurorialImage.gameObject.SetActive(false);
        DarkPanel.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && TurorialImage.gameObject.activeSelf)
            CloseTutorial();
    }
}
