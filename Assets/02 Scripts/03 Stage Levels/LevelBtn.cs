﻿using _02_Scripts.Helper_Classes.enums;
using _02_Scripts.ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RTLTMPro;
using TMPro;

public class LevelBtn : MonoBehaviour
{
    public TextMeshPro levelText, pointsText;
    public Sprite noStar, oneStar, twoStar, threeStar;
    public SpriteRenderer starsImage, lockIcon;

    public int _level;

    [Header("Button Appearance")] 
    public Sprite LockSprite, currentLevelSprite, PassedSprite;

    public string stageSceneText;

    public Stars starsTaken;
    public float pointsTakenPercent;

    public Animator _animator;

    public int Level
    {
        get => _level;
        set
        {
            _level = value;
            levelText.SetText((_level + 1).ToString());
        }
    }

    public Stars StarsTaken
    {
        get => starsTaken;
        set
        {
            starsTaken = value;
            if(Status != StageStatus.Locked)
            {
                switch (starsTaken)
                {
                    case Stars.No_Star:
                        starsImage.sprite = noStar;
                        break;
                    case Stars.One_Star:
                        starsImage.sprite = oneStar;
                        break;
                    case Stars.Two_Star:
                        starsImage.sprite = twoStar;
                        break;
                    case Stars.Three_Star:
                        starsImage.sprite = threeStar;
                        break;
                }
            }
            else
            {
                starsImage.sprite = noStar;
            }
        }
    }

    public float PointsTakenPercent
    {
        get => pointsTakenPercent;
        set
        {
            if(value <= 1)
                pointsTakenPercent = value;
            else if (value > 1)
            {
                pointsTakenPercent = 1;
            }
            pointsText.text = Mathf.Round(pointsTakenPercent * 100) + "%";
        }
    }

    private StageStatus _status;

    public StageStatus Status
    {
        get => _status;
        set
        {
            _status = value;
            SetStatus();
        }
    }

    private void Start()
    {
        //LoadData();

        levelText.text = "";
        pointsText.text = "";
        starsImage.sprite = noStar;
        
//        _animator = GetComponent<Animator>();
        
        Invoke("UpdateLevel", 0.5f);
        
//        SetStatus();
    }

    private void SetStatus()
    {
        var image = GetComponent<SpriteRenderer>();
        var button = GetComponent<BoxCollider2D>();
        switch (Status)
        {
            case StageStatus.Current:
                _animator.enabled = true;
                GetComponent<Animator>().SetTrigger("current");

                image.sprite = currentLevelSprite;
                levelText.enabled = true;
                pointsText.enabled = true;
                lockIcon.enabled = false;
                
                button.enabled = true;
                break;
            case StageStatus.Passed:
                _animator.enabled = false;

                image.sprite = PassedSprite;
                levelText.enabled = true;
                pointsText.enabled = true;
                lockIcon.enabled = false;
                button.enabled = true;
                break;
            case StageStatus.Locked:
                _animator.enabled = false;

                image.sprite = LockSprite;
                levelText.enabled = true;
                pointsText.enabled = false;
                lockIcon.enabled = true;
                button.enabled = false;
                starsImage.sprite = noStar;
                break;
        }
    }

    /// <summary>
    ///     When any level clicked, go to board with that scene
    ///     BoardData to that level
    /// </summary>
    public void OnMouseUpAsButton()
    {
        PlayerPrefs.SetInt("Level", _level);
        SceneManager.LoadScene(stageSceneText);
    }

    private void UpdateLevel()
    {
        levelText.text = _level + 1 + "";
        pointsText.text = Mathf.Round(pointsTakenPercent * 100) + "%";
        
        switch (starsTaken)
        {
            case Stars.No_Star:
                starsImage.sprite = noStar;
                break;
            case Stars.One_Star:
                starsImage.sprite = oneStar;
                break;
            case Stars.Two_Star:
                starsImage.sprite = twoStar;
                break;
            case Stars.Three_Star:
                starsImage.sprite = threeStar;
                break;
        }
    }
}
