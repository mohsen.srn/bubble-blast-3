﻿using System.Collections;
using System.Collections.Generic;
using _02_Scripts._02_Stage;
using _02_Scripts._02_Stage.Data;
using _02_Scripts.ScriptableObjects;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.SceneManagement;

namespace _02_Scripts._03_Stage_Levels
{
    public class LevelSceneManage : MonoBehaviour
    {
        [BoxGroup("Assets")] public GameObject levelBtnPrefab;
        public StagesMetaData stagesMetaData;
        public string HomeSceneName;
        // this public variable is for using in scrolling script
        public int LastRowNumber;

        public int ColumnNumber = 4;
        
        List<GameObject> Levels = new List<GameObject>();
        public Transform _currentLevel;

        public RectTransform content;
        private Transform _transform;
        public Camera camera;
        private void Start()
        {
            _transform = transform;
            // this line of code sets scale of levels to fit in scene
            float screenWidth = Screen.width;
            var thisPos = _transform.localPosition;
            _transform.localScale = Vector3.one * screenWidth / 10f;

            if (camera.aspect < (float)Screen.width / Screen.height)
            {
                _transform.localPosition = new Vector3(screenWidth / 2.1818f, thisPos.y, 0f);
            }
            else
            {
                _transform.localPosition = new Vector3(screenWidth / 2f, thisPos.y, 0f);
            }
            
/*********************************************************************/
            stagesMetaData = StageGameData.Instance.stagesMetaData;
            int stageCount = stagesMetaData.Stages.Count;
            
            LastRowNumber = (int)Mathf.Floor(stageCount / (float)ColumnNumber);

            for (int i = 0; i <= Mathf.Floor(stageCount / (float)ColumnNumber); i++)
            {
                for (int j = 0; j < ColumnNumber; j++)
                {
                    int levelNumber;
                    if (i % 2 == 0) // i is even
                    {
                        levelNumber = i * ColumnNumber + j;
                    }
                    else // i is odd
                    {
                        levelNumber = i * ColumnNumber + ColumnNumber - j - 1;
                    }

                    if (levelNumber >= stageCount)
                    {
                        continue;
                    }

                    var btn = Instantiate(levelBtnPrefab, _transform);
                    btn.transform.localPosition = new Vector3((2 * j - 3) * 1.175f, -1 * i * 2.7f - 1f, -1f);
                    Levels.Add(btn);
                    SetLevelButton(btn.GetComponent<LevelBtn>(), levelNumber, j);
                }
            }
            _target = new Vector3(content.position.x, 4400f, 0f);
            StartCoroutine(MoveToCorrectPosition());
            
            /*********************************************************************/
            // this line of code sets height of content gameObject
//            ((RectTransform)_transform.parent).sizeDelta = new Vector2(0f,
//                10 * (screenWidth / (3 * 16)) * (LastRowNumber + 1));
            
            ((RectTransform)_transform.parent).sizeDelta = new Vector2(0f,
                screenWidth * 5.625f);
        }

        private Vector3 _target;
        private IEnumerator MoveToCorrectPosition()
        {
            float speed = 20;
            // i use this variable and newXDiff, to check if ball passed the _target or not 
            float xDiff = 2500f; // this number is a fake number, i hope this make no bug
            int avoidInfinityLoop = 0;
            while (_currentLevel.position.y < 0)
            {
                var position = content.position;
                var _dirNormalized = (_target-position).normalized;
                position = position + speed * Time.deltaTime * _dirNormalized;
                content.position = position;
                float newXDiff = position.y;
                if (Mathf.Abs(Mathf.Abs(xDiff) - Mathf.Abs(newXDiff)) <= 0.05f)
                {
                    // if sign of xDiff and newXDiff is different, so ball passed target 
                    yield break;
                }
                xDiff = newXDiff;
                
                if (avoidInfinityLoop++ > 250)
                {
                    break;
                }
                
                yield return null;
            }
        }

        private void SetLevelButton(LevelBtn btn, int i, int j)
        {
            btn.Status = stagesMetaData.Stages[i].Status;
            btn.Level = i;
            if (stagesMetaData.Stages[i].gameResult != null)
            {
                btn.StarsTaken = stagesMetaData.Stages[i].gameResult.Stars;
                btn.PointsTakenPercent = 1f * stagesMetaData.Stages[i].gameResult.Points /
                                         stagesMetaData.Stages[i].totalBreakableResistance;
            }

            if (btn.Status == StageStatus.Current)
            {
                _currentLevel = btn.transform;
            }
        }

        public void GoToHome()
        {
            SceneManager.LoadScene(HomeSceneName);
        }
        
        void Update(){
            if (Input.GetKeyDown(KeyCode.Escape)) 
                GoToHome(); 
        }
    }
}
