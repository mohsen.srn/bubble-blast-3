﻿using System;
using System.Collections;
using System.Text;
using _02_Scripts;
using I2.Loc;
using NaughtyAttributes;
using UnityEngine;
using RTLTMPro;
using TMPro;
using UnityEngine.SceneManagement;

public class ArcadeUIManager : MonoBehaviour
{
    public RTLTextMeshPro englishLevelText;
    public RTLTextMeshPro farsiLevelText;
    
    public RTLTextMeshPro englishRecord;
    public RTLTextMeshPro farsiRecord;

    private bool _isEnglish;
    
    public TextMeshProUGUI DownBallNumberText;
    public TextMeshProUGUI FlyingBallNumberText;

    public GameObject pauseDialog;
    public GameObject grayBackground;

    /********************* events *************************/
    public static event Action<Action> OnOpenGameOverContinuable;
    public static event Action OnOpenSimpleGameOver;

    void Start()
    {
        if (LocalizationManager.CurrentLanguage ==
            LocalizationManager.GetAllLanguages()[0])
        {
            _isEnglish = true;
            englishRecord.gameObject.SetActive(true);
            englishRecord.gameObject.SetActive(true);
            
            farsiRecord.gameObject.SetActive(false);
            farsiLevelText.gameObject.SetActive(false);
        }
        else
        {
            _isEnglish = false;
            englishRecord.gameObject.SetActive(false);
            englishRecord.gameObject.SetActive(false);
            
            farsiRecord.gameObject.SetActive(true);
            farsiLevelText.gameObject.SetActive(true);
        }
        
        ArcadeBlockManager.OnLevelChanged += UpdateLevel;
        ArcadeGameData.OnRecordChange += UpdateRecord;
        
        StringBuilder recordStringBuilder = new StringBuilder();
        if (_isEnglish)
        {
            recordStringBuilder.AppendLine("Record").Append(ArcadeGameData.Instance.bestRecord.ToString());
            englishRecord.text = recordStringBuilder.ToString();
        }
        else
        {
            recordStringBuilder.AppendLine("رکورد").Append(ArcadeGameData.Instance.bestRecord.ToString());
            farsiRecord.text = recordStringBuilder.ToString();
        }

        Block.OnBlockReachedGround += OpenFinishGameDialog;

        BallManager.OnBallNumberStatusChanged += UpdateBallNumbersStatus;
        pauseDialog.SetActive(false);
        //        StartCoroutine(SetWidthWithDelay());

        AdManager.GetInstance.ShowBannerAd();

        Tutorial.OnCloseTutorial += OnTutorialClosed;

        if (ArcadeGameData.Instance.bestRecord <= 1)
        {
            // for now i do not open tutorial. because our tutorial has no farsi version
//            OpenTutorial();
        }

        StoreManager.OnStoreOpen += () => { _isStoreOpen = true; };
        StoreManager.OnStoreClose += () => { _isStoreOpen = false; };
    }    

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !_isTutorialOpen && !_isStoreOpen && !_isGameoverOpen)
            TogglePauseDialog();
    }

    IEnumerator SetWidthWithDelay()
    {
        yield return new WaitForSeconds(0.2f);

        RectTransform rt = GetComponent<RectTransform>();
        Vector2 sd = rt.sizeDelta;
        rt.sizeDelta = new Vector2(7f, sd.y);
    }

    private void OnDestroy()
    {
        ArcadeBlockManager.OnLevelChanged -= UpdateLevel;
        ArcadeGameData.OnRecordChange -= UpdateRecord;
        Block.OnBlockReachedGround -= OpenFinishGameDialog;
        Tutorial.OnCloseTutorial -= OnTutorialClosed;
    }

    private void UpdateLevel(int level)
    {
        if (_isEnglish)
        {
            englishLevelText.text = level.ToString();
        }
        else
        {
            farsiLevelText.text = level.ToString();
        }
    }

    private void UpdateRecord(int score)
    {
        StringBuilder recordStringBuilder = new StringBuilder();
        if (_isEnglish)
        {
            recordStringBuilder.AppendLine("Record").Append(ArcadeGameData.Instance.bestRecord.ToString());
            englishRecord.text = recordStringBuilder.ToString();
        }
        else
        {
            recordStringBuilder.AppendLine("رکورد").Append(ArcadeGameData.Instance.bestRecord.ToString());
            farsiRecord.text = recordStringBuilder.ToString();
        }
    }

    /***
     * This is called multiple time depend on how many bubble reach down.
     * every bubble that reach down call this function.
     * so we should prevent this happen.
     * so i create a flag called gameOverFlag.
     */
    private bool _gameOverFlag;

    private void OpenFinishGameDialog(int a)
    {
        if (!_gameOverFlag)
        {
            StartCoroutine(LockCallingOpenFinishGameDialog());
/*            if (AdManager.GetInstance.IsRewardAdLoaded())
            {
                OnOpenGameOverContinuable?.Invoke(() => { OnOpenSimpleGameOver?.Invoke(); });
            }
            else
            {
                OnOpenSimpleGameOver?.Invoke();
            }*/

            OnOpenGameOverContinuable?.Invoke(() => { OnOpenSimpleGameOver?.Invoke(); });
            _isGameoverOpen = true;
            
            Controller.GetInstance.enabled = false;
            if (ArcadeGameData.Instance.bestRecord <= ArcadeBlockManager.Instance.GameLevel)
            {
                GoogleAnalyticsManager.GetInstance.OnGainNewRecord(ArcadeBlockManager.Instance.GameLevel);
            }
        }
    }

    public void CloseArcadeScene_Pause()
    {
        SceneManager.LoadScene(0);
        AdManager.GetInstance.ShowInterstitialAd();
    }

    public void Pause()
    {
        TogglePauseDialog();
    }

    public void TogglePauseDialog()
    {
        if (pauseDialog.activeSelf)
        {
            Controller.GetInstance.IsDisable = false;
            pauseDialog.GetComponent<Animator>().SetTrigger("resume");
        }
        else
        {
            Controller.GetInstance.IsDisable = true;
            grayBackground.SetActive(true);
            pauseDialog.SetActive(true);
        }
    }

    private void UpdateBallNumbersStatus(int totalBalls, int flyingBalls)
    {
        if (totalBalls > -1)
        {
            DownBallNumberText.text = totalBalls + "";
        }

        FlyingBallNumberText.text = flyingBalls + "";
    }

    private bool _isTutorialOpen = false;
    private bool _isStoreOpen;
    public bool _isGameoverOpen;

    public void OpenTutorial()
    {
        Tutorial.Instance.OpenTutorial();
//        Controller.GetInstance.IsDisable = true;
        StartCoroutine("KeepControllerDisable");
        _isTutorialOpen = true;
    }

    IEnumerator KeepControllerDisable()
    {
        while (true)
        {
            yield return null;
            Controller.GetInstance.IsDisable = true;
        }
    }

    private void OnTutorialClosed()
    {
        StopCoroutine("KeepControllerDisable");
        Controller.GetInstance.IsDisable = false;
        _isTutorialOpen = false;
    }

    public void SwipeGameOverDialogs()
    {
//        adLoadedGameOver.SetActive(false);
//        noAdGameOver.SetActive(true);
//        dialogScoreNoAd.text = levelText.OriginalText;
    }

    IEnumerator LockCallingOpenFinishGameDialog()
    {
        _gameOverFlag = true;
        yield return null;
        _gameOverFlag = false;
    }

    public void OpenStore()
    {
        StoreManager.GetInstance.Open();
    }
}
