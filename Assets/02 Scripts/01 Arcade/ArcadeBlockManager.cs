﻿using System;
using System.Collections.Generic;
using _02_Scripts;
using _02_Scripts.Obstacles;
using _02_Scripts.ScriptableObjects;

public class ArcadeBlockManager : BaseBlockManager
{
    private int _gameLevel;

    public int GameLevel
    {
        get => _gameLevel;
        set
        {
            _gameLevel = value;

            OnLevelChanged?.Invoke(_gameLevel);
        }
    }

    public delegate void ArcadeActions(int level);

    public static event ArcadeActions OnLevelChanged;
    public static ArcadeBlockManager Instance { get; private set; }
    
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(Instance);
    }

    protected void Start()
    {
        //  i call create first row with delay to prevent collide the blocks to ground and cause premature failure
//        Invoke("NextLevel", 0.2f);

        // listen to BallManager.OnBallStop to go to next level and save game
        BallManager.OnBallStop +=  NextLevel;
        BoardManager.OnLastRowsDestroyed += SaveGame;        
        if(_gameLevel != 1)
            SetBoardColumns(_gameLevel);
    }

    private void OnDestroy()
    {
        BallManager.OnBallStop -=  NextLevel;
        BoardManager.OnLastRowsDestroyed -= SaveGame;
    }

    public void NextLevel()
    {
        // todo: here i should check if GameLevel == 0 -> reset gameOver count
        
        GameLevel++;
        CreateRow();
        SaveGame();
    }

    private void CreateRow()
    {
        var rowBlocks = GetRowBlocks();
        var blocks = new List<Obstacle>();

        var row = PoolManager.Instance.GetRow();
        try
        {
            row.rowTransform.parent = BoardManager.Instance.transform;
        }
        catch
        {
            // when player presses pause or closes game exactly when this method is calling, exception throws. so we handle it
            return;
        }
        // we add rows to board list so that we can keep state  
        BoardManager.Instance.AddRow(row);

        foreach (var objType in rowBlocks)
        {
            switch (objType)
            {
                case Obstacle.Type.Null:
                {
                    var obj = PoolManager.Instance.GetNull();
                    obj.GetComponent<NullObstacle>().SpecifyResistance(0);
                    blocks.Add(obj.GetComponent<Obstacle>());
                    break;
                }

                case Obstacle.Type.Ball:
                {
                    var obj = PoolManager.Instance.GetBallPlus();
                    BallPlus ballPlus = obj.GetComponent<BallPlus>();
                    ballPlus.SpecifyResistance(1);
                    blocks.Add(ballPlus);
                    break;
                }

                case Obstacle.Type.RowBomb:
                case Obstacle.Type.Ordinary:
                case Obstacle.Type.BoxBomb:
                {
                    var block = PoolManager.Instance.GetBlock(row, GameLevel, objType);
                    blocks.Add(block);
                    break;
                }
            }
        }
        row.UpdateObjectsInThisRow(blocks);
    }

    public void LoadSavedBoard()
    {
        CreateStageBoard(ArcadeGameData.Instance.savedBoard.stageBoard);
    }

    private void CreateStageBoard(List<StageRow> stageRows)
    {
        BallManager.GetInstance.BallNumbers = ArcadeGameData.Instance.savedBoard.ballNumber;
        GameLevel = ArcadeGameData.Instance.savedBoard.currentLevel;

        bool hasAnyBlock = false;

        for (int i = 0; i < stageRows.Count; i++)
        {
            var row = PoolManager.Instance.GetRow();
            row.rowTransform.parent = BoardManager.Instance.gameObject.transform;
            // we add rows to board list so that we can keep state  
            BoardManager.Instance.AddRowInstantly(row);
            var blocks = new List<Obstacle>();

            foreach (var obstacle in stageRows[i].stageObstacles)
            {
                // ReSharper disable once IdentifierTypo
                Obstacle obstcl;

                switch (obstacle.type)
                {
                    case StageObstacle.ObstacleType.O:

                        var ballPlus = PoolManager.Instance.GetBallPlus();
                        ballPlus.transform.parent = row.rowTransform;
                        obstcl = ballPlus.GetComponent<BallPlus>();
                        obstcl.SpecifyResistance(obstacle.resistance);
                        obstcl.obstacleMetaData = obstacle;

                        hasAnyBlock = true;

                        break;
                    case StageObstacle.ObstacleType.LJ:

                        obstcl = PoolManager.Instance.GetBlock(row, obstacle.resistance, Obstacle.Type.Ordinary);
                        obstcl.obstacleMetaData = obstacle;

                        hasAnyBlock = true;

                        break;
                    // ReSharper disable once RedundantCaseLabel
                    case StageObstacle.ObstacleType.__:
                    default: // i put default here to ensure all of obstcl will initials
                        var empty = PoolManager.Instance.GetNull();
                        obstcl = empty.GetComponent<NullObstacle>();
                        obstcl.SpecifyResistance(0);
                        obstcl.obstacleMetaData = obstacle;
                        break;
                }

                blocks.Add(obstcl);
            }

            row.UpdateObjectsInThisRow(blocks);
        }

        if (!hasAnyBlock)
        {
            print("this board saved with no obstacle. so codes called NextLevel automatically");
            GameLevel--; // here i decrease gameLevel to Compensate ball adder
            NextLevel();
        }
    }

/************************** Helper Methods***********************/
/// <summary>
/// This method controls def 
/// </summary>
/// <returns></returns>
    private int MakeBlockRow()
    {
        ArcadeDifficultyManager.GetInstance.GetBubbleCounts(_gameLevel, out var min, out var max);
        int blockNumber = UtilityClass.MakeRandNumber(min, max);
        
        SetBoardColumns(_gameLevel);

        return blockNumber;
    }

    /// <summary>
    ///     This method creates row format. it helps us to create actual row of block and balls and blank area
    /// </summary>
    /// <param name="blockCount"> how many blocks should this row have</param>
    /// <returns>array of row containers</returns>
    private List<Obstacle.Type> GetRowBlocks()
    {
        ROW_NUMBER = _gameLevel % 2 == 0 ? ArcadeBoardCameraManager.Instance.columnNumbers - 1 : ArcadeBoardCameraManager.Instance.columnNumbers;

        var blockCount = MakeBlockRow();

        // tracking items added to row
        var remainingObjects = GetRemainingItemsTrackerList(blockCount);

        var row = new List<Obstacle.Type>();
        while (remainingObjects.Count > 0)
        {
            if (remainingObjects.Contains(Obstacle.Type.Ordinary))
                if (ShouldHaveBlock())
                {
                    row.Add(Obstacle.Type.Ordinary);

                    remainingObjects.Remove(Obstacle.Type.Ordinary);
                    continue;
                }

            if (remainingObjects.Contains(Obstacle.Type.Ball) && ShouldHaveBall())
            {
                row.Add(Obstacle.Type.Ball);
                remainingObjects.Remove(Obstacle.Type.Ball);
                continue;
            }

            if (remainingObjects.Contains(Obstacle.Type.Null))
            {
                row.Add(Obstacle.Type.Null);
                remainingObjects.Remove(Obstacle.Type.Null);
            }
        }

        return row;
    }

    // this method creates an array that specifies how many null block should this row have. this for tracking what item types we had added to this row before.
    private List<Obstacle.Type> GetRemainingItemsTrackerList(int blockCount)
    {
        var remainingObjects = new List<Obstacle.Type>();

        remainingObjects.Add(Obstacle.Type.Ball);

        for (var i = 0; i < blockCount; i++) remainingObjects.Add(Obstacle.Type.Ordinary);

        for (var i = remainingObjects.Count; i < ROW_NUMBER; i++)
        {
            remainingObjects.Add(Obstacle.Type.Null);
        }

        return remainingObjects;
    }

    // use this method to decide should we use block in this column or not
    private bool ShouldHaveBlock()
    {
        return UtilityClass.MakeRandNumber(0, 2) != 0;
    }

    private bool ShouldHaveBall()
    {
        return UtilityClass.MakeRandNumber(0, 2) != 0;
    }

    private void SaveGame()
    {
        SavedBoard savedBoard = new SavedBoard
        {
            stageBoard = BoardManager.Instance.Convert2SerializableObject(),
            ballNumber = BallManager.GetInstance.ballNumbers,
            currentLevel = GameLevel
        };
        ArcadeGameData.Instance.savedBoard = savedBoard;
        ArcadeGameData.Instance.PauseGame();
    }

    private void SetBoardColumns(int gameLevel)
    {
        int colNum = ArcadeDifficultyManager.GetInstance.GetColumnNumbers(gameLevel);
        ArcadeBoardCameraManager.Instance.SetColumnNumber(colNum);
    }
}
