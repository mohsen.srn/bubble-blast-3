﻿using System;
using _02_Scripts.Helper_Classes.Models;
using Firebase.Analytics;
using NaughtyAttributes;
using UnityEngine;
public class ArcadeDifficultyManager : MonoBehaviour
{
/**************************************************/
    public static ArcadeDifficultyManager GetInstance;

    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            // here we set hardness of game that sent by remoteConfig  
            SetDifficulties();
        }
        else
        {
            Destroy(this);
        }
    }
/*******************************************************/

    private void SetDifficulties()
    {
        levelColumnCharacteristics = new LevelColumnCharacteristics[7];

        levelColumnCharacteristics[0] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave1.floor,
            columns = GameManager.GetInstance.columnNumbers.wave1.columns
        };
        
        levelColumnCharacteristics[1] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave2.floor,
            columns = GameManager.GetInstance.columnNumbers.wave2.columns
        };

        levelColumnCharacteristics[2] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave3.floor,
            columns = GameManager.GetInstance.columnNumbers.wave3.columns
        };

        levelColumnCharacteristics[3] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave4.floor,
            columns = GameManager.GetInstance.columnNumbers.wave4.columns
        };

        levelColumnCharacteristics[4] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave5.floor,
            columns = GameManager.GetInstance.columnNumbers.wave5.columns
        };

        levelColumnCharacteristics[5] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave6.floor,
            columns = GameManager.GetInstance.columnNumbers.wave6.columns
        };

        levelColumnCharacteristics[6] = new LevelColumnCharacteristics
        {
            fromLevelNumber = GameManager.GetInstance.columnNumbers.wave7.floor,
            columns = GameManager.GetInstance.columnNumbers.wave7.columns
        };
        ///////////////////////////////////////////////////////////////

        levelBubbleCharacteristics = new LevelBubbleCharacteristics[30];
        for (int i = 0; i < 30; i++)
        {
            switch (i)
            {
                case 0:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step1);
                    break;
                case 1:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step2);
                    break;
                case 2:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step3);
                    break;
                case 3:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step4);
                    break;
                case 4:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step5);
                    break;
                case 5:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step6);
                    break;
                case 6:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step7);
                    break;
                case 7:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step8);
                    break;
                case 8:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step9);
                    break;
                case 9:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step10);
                    break;
                case 10:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step11);
                    break;
                case 11:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step12);
                    break;
                case 12:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step13);
                    break;
                case 13:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step14);
                    break;
                case 14:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step15);
                    break;
                case 15:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step16);
                    break;
                case 16:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step17);
                    break;
                case 17:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step18);
                    break;
                case 18:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step19);
                    break;
                case 19:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step20);
                    break;
                case 20:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step21);
                    break;
                case 21:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step22);
                    break;
                case 22:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step23);
                    break;
                case 23:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step24);
                    break;
                case 24:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step25);
                    break;
                case 25:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step26);
                    break;
                case 26:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step27);
                    break;
                case 27:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step28);
                    break;
                case 28:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step29);
                    break;
                case 29:
                    SetBubblePerRow(i, GameManager.GetInstance.bubblePerRow.step30);
                    break;
            }
        }
    }

    private void SetBubblePerRow(int index, Step step)
    {
        levelBubbleCharacteristics[index] = new LevelBubbleCharacteristics
        {
            fromLevelNumber = step.floor, minMaxBubble = {x = step.minBubble, y = step.maxBubble}
        };
    }

    [BoxGroup("Board Columns:")]
    public LevelColumnCharacteristics[] levelColumnCharacteristics;

    [BoxGroup("Bubble Numbers:")]
    public LevelBubbleCharacteristics[] levelBubbleCharacteristics;
    
    public int GetColumnNumbers(int levelNumber)
    {
        for (var i = 0; i < levelColumnCharacteristics.Length - 1; i++)
        {
            if (IsIn(levelNumber, 
                levelColumnCharacteristics[i].fromLevelNumber, 
                levelColumnCharacteristics[i+1].fromLevelNumber))
            {
                return levelColumnCharacteristics[i].columns;
            }
        }

        FirebaseAnalytics.LogEvent("1 Bug in ArcadeDifficultyManager.GetColumnNumbers(int levelNumber) -> Level number is out of...");

        return 7; // fuck fuck fuck i should fix this later
// something went wrong!
//        throw new Exception("Level number is out of ...");
        
        print("Error Happened => Level number is out of ...");
    }

    public void GetBubbleCounts(int levelNumber, 
        out int minBubbleNumber, out int maxBubbleNumber)
    {
        for (int i = 0; i < levelBubbleCharacteristics.Length - 1; i++)
        {
            if (IsIn(levelNumber,
                levelBubbleCharacteristics[i].fromLevelNumber,
                levelBubbleCharacteristics[i+1].fromLevelNumber))
            {
                // here we get column number that this row has. even and odd levelNumbers have different columns
                int col = GetColumnNumbers(levelNumber);
                col = levelNumber % 2 == 0 ? col - 1 : col;
                
                // max bubbles that can put to this row is columns minus one -> only one for adder
                int maxBubble = col - 1;

                minBubbleNumber = Mathf.CeilToInt(levelBubbleCharacteristics[i].minMaxBubble.x * maxBubble);
                maxBubbleNumber = Mathf.CeilToInt(levelBubbleCharacteristics[i].minMaxBubble.y * maxBubble);
                
                return;
            }
        }

        minBubbleNumber = 4;
        maxBubbleNumber = 5;
        print("GetBubbleCounts - > Error");
//        minBubbleNumber = 0; maxBubbleNumber = 0;
        // something went wrong!
//        throw new Exception("Level number is out of ...");
        FirebaseAnalytics.LogEvent("2 Bug in ArcadeDifficultyManager.GetBubbleCounts(int levelNumber) -> Level number is out of...");
    }
    
    private bool IsIn(int level, float min, float max)
    {
        return level >= min && level < max;
    }
}

[Serializable]
public class LevelBubbleCharacteristics
{
    public int fromLevelNumber;
    public Vector2 minMaxBubble;
}

[Serializable]
public class LevelColumnCharacteristics
{
    public int fromLevelNumber;
    public int columns;
}
