using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using _02_Scripts.Helper_Classes;
using Firebase.Analytics;
using I2.Loc;
using NaughtyAttributes;
using RTLTMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverContineueDialog : MonoBehaviour
{
    public int videoAdRowRemoval = 1;
    public int spendCoin50 = 2;
    public int spendCoin100 = 5;

    public GameObject dialog;
    public GameObject darkOverlay;
    
    [BoxGroup("ُScore Area")] public RTLTextMeshPro englishScoreText;
    [BoxGroup("ُScore Area")] public RTLTextMeshPro farsiScoreText;

    public Animator storeBtnAnimator;

    [BoxGroup("Watch Video")] public Button watchVideoBtn;
    
    [BoxGroup("Spend Coin")] public Button spend100CoinBtn;
    [BoxGroup("Spend Coin")] public Button spend50CoinBtn;

    public Animator closeBtn;
    private Action _failAction;
    
    private void Start()
    {
        ArcadeUIManager.OnOpenGameOverContinuable += OpenDialog;
        dialog.SetActive(false);
    }

    private void OnDestroy()
    {
        ArcadeUIManager.OnOpenGameOverContinuable -= OpenDialog;
    }

    private void OpenDialog(Action onFail)
    {
        NetworkingCodes.GetInstance.SetRecord();

        _failAction = onFail;
        darkOverlay.SetActive(true);
        dialog.SetActive(true);

        StringBuilder scoreStringBuilder = new StringBuilder();
        
        if (LocalizationManager.CurrentLanguage == LocalizationManager.GetAllLanguages()[0])
        {
            englishScoreText.gameObject.SetActive(true);
            farsiScoreText.gameObject.SetActive(false);

            scoreStringBuilder.Append("Score ").Append(ArcadeBlockManager.Instance.GameLevel.ToString());
            englishScoreText.text = scoreStringBuilder.ToString();
        }
        else
        {
            englishScoreText.gameObject.SetActive(false);
            farsiScoreText.gameObject.SetActive(true);

            scoreStringBuilder.Append("امتیاز ").Append(ArcadeBlockManager.Instance.GameLevel.ToString());
            farsiScoreText.text = scoreStringBuilder.ToString();
        }

        if (CoinManager.GetInstance.GetCoins() < ArcadeCoin.GetInstance.helpCostFor5Bomb)
        {
            spend100CoinBtn.interactable = false;
            storeBtnAnimator.SetBool("animate", true);
        }
        else
        {
            spend100CoinBtn.interactable = true;
            storeBtnAnimator.SetBool("animate", false);
        }

        if (CoinManager.GetInstance.GetCoins() < ArcadeCoin.GetInstance.helpCostFor2Bomb)
        {
            spend50CoinBtn.interactable = false;
            storeBtnAnimator.SetBool("animate", true);
        }
        else
        {
            spend50CoinBtn.interactable = true;
            storeBtnAnimator.SetBool("animate", false);
        }

        if (AdManager.GetInstance.IsRewardAdLoaded())
        {
            watchVideoBtn.interactable = true;
        }
        else
        {
            watchVideoBtn.interactable = false;
        }

        StartCoroutine("CheckForScape");
    }

    IEnumerator CheckForScape()
    {
        /*while (!Input.GetKeyDown(KeyCode.Escape))
        {
            yield return null;
        }
        Click_Close();*/

        while (true)
        {
            yield return null;
            darkOverlay.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                closeBtn.SetBool("animate" , true);
            }
        }
    }

    public void Click_Close()
    {
        ArcadeGameData.Instance.GameFinished(0);
        SceneManager.LoadScene(0);

        AdManager.GetInstance.ShowInterstitialAd();
        
        FirebaseAnalytics.LogEvent("BubbleBlast_Click_GameOver_Close", "Score", ArcadeBlockManager.Instance.GameLevel.ToString());
    }

    public void Click_VideoRewardAd()
    {
        
        FirebaseAnalytics.LogEvent("BubbleBlast_Click_GameOver_VideoRewardAd", "Score", ArcadeBlockManager.Instance.GameLevel.ToString());
        
        StopCoroutine("CheckForScape");
        darkOverlay.SetActive(false);
#if UNITY_EDITOR
//        dialog.SetActive(false);
        StartCoroutine(RemoveRowsWithDelay(1, 0f));

#else
        AdManager.GetInstance.ShowRewardedAd(
            () =>
            {
//                dialog.SetActive(false);
//                RemoveLastRowsAndContinue(2);
                StartCoroutine(RemoveRowsWithDelay(1, 0f));
            }, 
            () => 
            {
                dialog.SetActive(false);
                darkOverlay.SetActive(false);
                _failAction();
            });
#endif
    }
    public GameObject quickDialogBomb;
    public GameObject[] bombImages = new GameObject[5];
    IEnumerator RemoveRowsWithDelay(int row, float bombDelay)
    {
        dialog.SetActive(false);
        yield return new WaitForSeconds(bombDelay);
        // first enable bombs according to row parameter
        for (int i = 0; i < bombImages.Length; i++)
        {
            if (i < row)
            {
                bombImages[i].SetActive(true);
            } 
            else
                bombImages[i].SetActive(false);
        }
        // first disable controller
        Controller.GetInstance.enabled = false;
        darkOverlay.SetActive(false);
        
        quickDialogBomb.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        RemoveLastRowsAndContinue(row);
    }

    public void Click_Spend100Coin()
    {
        FirebaseAnalytics.LogEvent(
            "BubbleBlast_Click_GameOver_Spend100Coin", 
            new Parameter("Score", ArcadeBlockManager.Instance.GameLevel),
            new Parameter("Coin repository", CoinManager.GetInstance.GetCoins()));

        StopCoroutine("CheckForScape");
        darkOverlay.SetActive(false);
        
        ArcadeCoin.GetInstance.SpendCoinToGetHelp
        ( 5,
            () =>
            {
//                StartCoroutine(ShowQuickDialogThenRemoveRows(spendCoin100));
                StartCoroutine(RemoveRowsWithDelay(spendCoin100, 1.5f));
            },
            () =>
            {
                /*do something else*/
                print("Oh you have not enough coins");
            }
        );
    }

    public void Click_Spend50Coin()
    {
        FirebaseAnalytics.LogEvent(
            "BubbleBlast_Click_GameOver_Spend50Coin", 
            new Parameter("Score", ArcadeBlockManager.Instance.GameLevel),
            new Parameter("Coin repository", CoinManager.GetInstance.GetCoins()));

        
        StopCoroutine("CheckForScape");
        darkOverlay.SetActive(false);
        
        ArcadeCoin.GetInstance.SpendCoinToGetHelp
        ( 2,
            () =>
            {
//                StartCoroutine(ShowQuickDialogThenRemoveRows(spendCoin50));
                StartCoroutine(RemoveRowsWithDelay(spendCoin50, 2f));
            },
            () =>
            {
                /*do something else*/
                print("Oh you have not enough coins");
            }
        );
    }

    IEnumerator ShowQuickDialogThenRemoveRows(int rowToREmove)
    {
        dialog.SetActive(false);
        /*darkOverlay.SetActive(false);
        QuickDialog_SpendCoin2Continue.SetActive(true);*/
        yield return new WaitForSeconds(2f);
        RemoveLastRowsAndContinue(rowToREmove);
    }

    private void RemoveLastRowsAndContinue(int rowNums)
    {
        FindObjectOfType<ArcadeUIManager>()._isGameoverOpen = false;
        
        Controller.GetInstance.enabled = true;
        BoardManager.Instance.RemoveRows(rowNums);

/*        if (ArcadeGameData.Instance.bestRecord <= ArcadeBlockManager.Instance.GameLevel)
        {
            GoogleAnalyticsManager.GetInstance.OnGainNewRecord(ArcadeBlockManager.Instance.GameLevel);
        }*/
    }

    public void Click_OpenStore()
    {
        FirebaseAnalytics.LogEvent(
            "BubbleBlast_Click_GameOver_OpenStore", 
            new Parameter("Score", ArcadeBlockManager.Instance.GameLevel),
            new Parameter("Coin repository", CoinManager.GetInstance.GetCoins()));

        StoreManager.GetInstance.Open();
    }
}
