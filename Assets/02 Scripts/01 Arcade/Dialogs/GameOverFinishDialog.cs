using System;
using RTLTMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverFinishDialog : MonoBehaviour
{
    public GameObject dialog;
    public GameObject darkGray;
    public RTLTextMeshPro scoreText;

    private void Start()
    {
        ArcadeUIManager.OnOpenSimpleGameOver += OpenDialog;
        dialog.SetActive(false);
        darkGray.SetActive(false);
    }

    private void OnDestroy()
    {
        ArcadeUIManager.OnOpenSimpleGameOver -= OpenDialog;
    }

    private void OpenDialog()
    {
        dialog.SetActive(true);
        darkGray.SetActive(true);
        scoreText.text = ArcadeBlockManager.Instance.GameLevel.ToString();
    }

    public void ResetBoard()
    {
        ArcadeGameData.Instance.GameFinished(0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void CloseArcadeScene()
    {
        ArcadeGameData.Instance.GameFinished(0);
        SceneManager.LoadScene(0);
        AdManager.GetInstance.ShowInterstitialAd();
    }
}
