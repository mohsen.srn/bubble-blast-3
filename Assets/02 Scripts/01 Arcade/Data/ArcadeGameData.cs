﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ArcadeGameData : MonoBehaviour
{
    public int bestRecord;
    public bool isGamePaused;
    public SavedBoard savedBoard;
    
    private const string BestRecord = "Best Record";
    private const string GamePause = "Is Pause?";
    public string fileToSave = "0";

    public delegate void ArcadeDataActions(int a);
    public static event ArcadeDataActions OnRecordChange;
    
    public static ArcadeGameData Instance { get; private set; }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    
        Load();
    
        if (!PlayerPrefs.HasKey(BestRecord))
        {
            PlayerPrefs.SetInt(BestRecord, 0);
            bestRecord = 0;
            PlayerPrefs.Save();
        }
        else
        {
            bestRecord = PlayerPrefs.GetInt(BestRecord);
        }

        if (!PlayerPrefs.HasKey(GamePause))
        {
            PlayerPrefs.SetInt(GamePause, 0);
            isGamePaused = false;
            PlayerPrefs.Save();
        }
        else
        {
            var gamePaused = PlayerPrefs.GetInt(GamePause);
            isGamePaused = gamePaused != 0;
        }
    }

    private void Start()
    {
//        Block.OnBlockReachedGround += GameFinished;
        ArcadeBlockManager.OnLevelChanged += KeepBestRecord;
        if (isGamePaused)
        {
            Load();
            if (savedBoard.ballNumber == 0)
            {
                ArcadeBlockManager.Instance.NextLevel();
                return;
            }
            
            ArcadeBlockManager.Instance.LoadSavedBoard();
        }
        else
        {
            ArcadeBlockManager.Instance.NextLevel();
        }
    }

    private void OnDestroy()
    {
        Save();
//        PauseGame();
        Instance = null;
        Block.OnBlockReachedGround -= GameFinished;
        ArcadeBlockManager.OnLevelChanged -= KeepBestRecord;
    }

    public void PauseGame()
    {
        isGamePaused = true;
        PlayerPrefs.SetInt(GamePause, 1);
        PlayerPrefs.Save();
        Save();
    }

    public void GameFinished(int a)
    {
        isGamePaused = false;
        PlayerPrefs.SetInt(GamePause, 0);
        PlayerPrefs.Save();
        savedBoard.stageBoard.Clear();
        savedBoard.currentLevel = 1;
        savedBoard.ballNumber = 1;
//        Save();
    }

    private void KeepBestRecord(int score)
    {
        if (bestRecord < score)
        {
            PlayerPrefs.SetInt(BestRecord, score);
            bestRecord = score;
            PlayerPrefs.Save();
            OnRecordChange?.Invoke(bestRecord);
        }
    }

    private void Save()
    {
        // Create a binary formatter which can read binary files
        var formatter = new BinaryFormatter();

        // Create a route from app to the file
        var fileStream = File.Open(Application.persistentDataPath + "/Arcade" + fileToSave + ".dat", FileMode.Create);

        // Create and assign the data to save
        var data = new SavedBoard();
        data = savedBoard;

        // Save the data in the file
        formatter.Serialize(fileStream, data);

        // Close the data stream
        fileStream.Close();

//        print("Arcade Saved");
    }

    private void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/Arcade" + fileToSave + ".dat"))
        {
            var formatter = new BinaryFormatter();
            var fileStream = File.Open(Application.persistentDataPath + "/Arcade" + fileToSave + ".dat", FileMode.Open);
            savedBoard = formatter.Deserialize(fileStream) as SavedBoard;
            fileStream.Close();
//            print("Arcade Loaded");
        }
        else
        {
            savedBoard = new SavedBoard();
            
            PlayerPrefs.SetInt(BestRecord, 0);
            bestRecord = 0;
            PlayerPrefs.Save();
            
            PlayerPrefs.SetInt(GamePause, 0);
            isGamePaused = false;
            PlayerPrefs.Save();
        }
    }
}
