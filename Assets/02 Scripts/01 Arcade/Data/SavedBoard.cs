﻿using System;
using System.Collections.Generic;
using _02_Scripts.ScriptableObjects;

[Serializable]
public class SavedBoard
{
    public List<StageRow> stageBoard = new List<StageRow>();
    public int currentLevel;
    public int ballNumber;
}
