﻿using System.Collections;
using _02_Scripts;
using UnityEditor;
using UnityEngine;

public class ArcadeBoardCameraManager : CameraManager
{
    public static ArcadeBoardCameraManager Instance;

    private new void Awake()
    {
        base.Awake();                
        if (Instance == null)
        {
            Instance = this;
        }
    }
    
    public int columnNumbers = 5;

    protected override int GetRowNumbers()
    {
        return UtilityClass.GetRowNumberFromColumnNumber_Arcade(columnNumbers);
    }

    protected override int GetColumnNumbers()
    {
        return columnNumbers;
    }

    protected override float TopUiHeightSize()
    {
        return 1.083f;
    }

    protected override float GetYPositionOfTopUi()
    {
//        return _camera.orthographicSize - TopUiHeightSize() / 2f;
        return GetRowNumbers()/2f + TopUiHeightSize();
    }

    public void SetColumnNumber(int colNum)
    {
//        print("Arcade.SetColumnNumber(" + colNum + ")");
        
        if (columnNumbers == colNum)
        {
            return;
        }
        columnNumbers = colNum;
        
        InitCamera();
    }

    protected new void Start()
    {
        ballManager = BallManager.GetInstance.transform;
//        InitCamera();
//        InitBoard(_camera.orthographicSize);
    }

    protected void InitCamera()
    {
//        print("ArcadeBoardCameraManager.InitCamera()");
        
        // now i don't know what is this line for!
        _columnNumber = GetColumnNumbers();
        
        float rowNumber = GetRowNumbers();
        
        float orthographicSize = 0;
        if (GetColumnNumbers() < rowNumber / 2 + 2)
        {
            orthographicSize = rowNumber / 2 + 2;
        }
        else
        {
            orthographicSize = GetColumnNumbers() / (2f * _camera.aspect);
        }
        
        StartCoroutine(ChangeCameraOrthoSize(orthographicSize));
    }
    
    new IEnumerator ChangeCameraOrthoSize(float orthoSize)
    {
//        print("ArcadeBoardCameraManager.ChangeCameraOrthoSize( orthoSize : " + orthoSize + " )");
        InitBoard(orthoSize);

        float diff = _camera.orthographicSize - orthoSize;
        int avoidInfinityLoop = 0;
        
        while (Mathf.Abs(_camera.orthographicSize - orthoSize) > 0.1f)
        {
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, orthoSize, Speed);
//            _camera.Render();
            float newDiff = _camera.orthographicSize - orthoSize;
            if (diff * newDiff < 0)
            {
                // if sign of xDiff and newXDiff is different, so ball passed target 
                break;
            }

            if (avoidInfinityLoop++ > 50)
            {
                break;
            }

            yield return null;
        }

        _camera.orthographicSize = orthoSize;
    }
    
    protected new void InitBoard(float orthographicSize)
    {
//        print("ArcadeBoardCameraManager.InitBoard()");
        var camPosition = _camera.transform.position;
        // we use canvas as full screen, so commented these lines out
//        topUiCanvas.position = new Vector3(0f, GetYPositionOfTopUi(), 0f); ////////
//        topUiCanvas.sizeDelta = new Vector2(orthographicSize * 2 * _camera.aspect, TopUiHeightSize());
      
        /******************************************/
        topWall.size = new Vector2(GetColumnNumbers() + 2, wallWidth);
        Transform topTransform = topWall.transform;
        ground.size = new Vector2(GetColumnNumbers() + 2, wallWidth);
        Transform groundTransform = ground.transform;
        finishLine.size = new Vector2(GetColumnNumbers() + 2, 0.5f);
        rightWall.size = new Vector2(1, orthographicSize * 2 + 2);
        leftWall.size = new Vector2(1, orthographicSize * 2 + 2);

        topTransform.localPosition = new Vector2(0f, GetRowNumbers() / 2f + 1f);
        groundTransform.localPosition = new Vector2(0f, -1 * GetRowNumbers() / 2f);
        finishLine.transform.localPosition = new Vector2(0f, groundTransform.localPosition.y + 0.8f);
        rightWall.transform.localPosition = new Vector3(GetColumnNumbers() * 1f / 2 + 0.5f, 0f);
        leftWall.transform.localPosition = new Vector3(-1 * GetColumnNumbers() * 1f / 2 - 0.5f, 0f);

        topWall.offset = new Vector2(0f, 0.5f * wallWidth - 0.5f);
        ground.offset = new Vector2(0f, (1f - wallWidth) / 2f);

/********************************************/
//        topWall.GetComponent<Wall>().Start();
//        ground.GetComponent<Wall>().Start();
        rightWall.GetComponent<Wall>().Start();
        leftWall.GetComponent<Wall>().Start();

/********************************************************/
        borderTop.transform.localPosition = new Vector2(0f, topTransform.position.y + (0.5f * wallWidth - 0.5f) - wallWidth/2f);
        borderDown.transform.localPosition = new Vector2(0f, groundTransform.position.y + 0.5f);
        /********************************************/
//        walls.position = new Vector3(camPosition.x,
//            (orthographicSize - GetRowNumbers() / 2)/2 - TopUiHeightSize() + 0.5f, 0f);
        
        walls.position = new Vector3(camPosition.x, 0f, 0f);
        boardManager.position = new Vector3(camPosition.x, topTransform.position.y - 0.5f, 0f);

        ballManager.position = new Vector3(ballManager.position.x, groundTransform.position.y + 0.7f, 0f);

        controller.position = new Vector3(camPosition.x, camPosition.y + 0.2f, 0f);
        controllerCollider.size = new Vector2(GetColumnNumbers() + 1f,
            UtilityClass.GetRowNumberFromColumnNumber_Arcade(GetColumnNumbers()) - 1);
        /********************************************/
        
        // Here i call this function to assign  '_launcherPos' and 'ballDestinationPosition'
        // i expect this fix a weird bug.
        // this bug cause when we shoot ball early, ball collide to ground.
        /// edit: it did not help
        BallManager.GetInstance.InitWithDelay();
    }
}
