﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using _02_Scripts;
using _02_Scripts.Obstacles;
using _02_Scripts.ScriptableObjects;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public int boardRowsNumber = 10;
    public List<Row> rows = new List<Row>();
    private static BoardManager _instance;
    public static BoardManager Instance => _instance;

    public delegate void BoardManagerActions();

    public static event BoardManagerActions OnLastRowsDestroyed;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        StartCoroutine("CleanUpUnusedRows");
    }

    public void AddRow(Row row, bool freeFirstLine = true)
    {
        float offset;
        if (freeFirstLine) offset = -0.5f;
        else offset = +0.5f;

        row.rowTransform.localPosition = new Vector3(0f, 0 + offset, 0f);
        rows.Add(row);

        UpdateBoard();
    }

    public void AddRowInstantly(Row row, bool freeFirstLine = true)
    {
        float offset;
        if (freeFirstLine) offset = -0.5f;
        else offset = +0.5f;

        row.rowTransform.localPosition = new Vector3(0f, 0f + offset, 0f);
        rows.Add(row);

        for (var index = 0; index < rows.Count; index++)
        {
            var fRow = rows[index];
            fRow.GoOneStepDownInstantly();
        }
    }

    /// <summary>
    /// step down all the rows
    /// </summary>
    private void UpdateBoard()
    {
        foreach (var row in rows)
        {
            row.GoOneStepDown();
        }
    }

    // This method every x seconds checks if any empty row in bottom of board exits. if exits, cleans up board
    private IEnumerator CleanUpUnusedRows()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            CleanUpEmptyRows();
        }
    }

    private void CleanUpEmptyRows()
    {
        if (rows.Count > 0)
            while (rows[0].IsEmpty())
            {
                PoolManager.Instance.Back2RowPool(rows[0]);
                rows.RemoveAt(0);

                if (rows.Count == 0) break;
            }
    }

    public List<StageRow> Convert2SerializableObject()
    {
        List<StageRow> stageRows = new List<StageRow>();
        for (var index = 0; index < rows.Count; index++)
        {
            var row = rows[index];
            stageRows.Add(row.Convert2SerializableObject(index == rows.Count - 1));
        }

        return stageRows;
    }

    public void RemoveRows(int rowCount)
    {
        CleanUpEmptyRows();
//        StartCoroutine(SequentialBubbleRemover(rowCount));
        StartCoroutine(SequentialBombMaker(rowCount));

        /*if (rows.Count < rowCount)
        {
            rowCount = rows.Count;
        }

        for (int i = 0; i < rowCount; i++)
        {
            for (int j = 0; j < rows[i].obstacles.Count; j++)
            {
                if (rows[i].obstacles[j].obstacleType == Obstacle.Type.Ordinary)
                {
                    (rows[i].obstacles[j] as Block).SetResistanceZeroWithRandomDelay(1f);
                }
            }
        }*/
    }

    IEnumerator SequentialBubbleRemover(int rowNum)
    {
        StopCoroutine("CleanUpUnusedRows");
        if (rows.Count < rowNum)
        {
            rowNum = rows.Count;
        }

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < rowNum; i++)
        {
            for (int j = 0; j < rows[i].obstacles.Count; j++)
            {
                if (rows[i].obstacles[j].obstacleType == Obstacle.Type.Ordinary)
                {
                    rows[i].obstacles[j].SpecifyResistance(0);
                    //                    (rows[i].obstacles[j] as Block).SetResistanceZeroWithRandomDelay(0.5f);
                    yield return new WaitForSeconds(0.2f);
                }
            }
        }

        OnLastRowsDestroyed?.Invoke();
        StartCoroutine("CleanUpUnusedRows");
    }

    IEnumerator SequentialBombMaker(int rowNum)
    {
        Controller.GetInstance.IsDisable = true;
        StopCoroutine("CleanUpUnusedRows");
        
        yield return null;
        
        if (rows.Count < rowNum)
        {
            rowNum = rows.Count;
        }

//        yield return new WaitForSeconds(1f);

        /*for (int i = 0; i < rows[0].obstacles.Count; i++)
        {
            rows[0].obstacles[i].SpecifyResistance(0);
        }*/

        for (int i = 0; i < rowNum; i++)
        {
            Vector3 pos = Vector3.zero;
            for (int j = 1; j < rows[i].obstacles.Count; j++)
            {
                if (Mathf.Abs(rows[i].obstacles[j].obstacleTransform.position.x) <= 1.1f)
                {
                    pos = rows[i].obstacles[j].obstacleTransform.localPosition;
                    rows[i].obstacles[j].SpecifyResistance(0);
                    break;
                }
            }
            yield return new WaitForSeconds(0.1f);
            rows[i].obstacles[0] = PoolManager.Instance.GetBlock(rows[i], 1, Obstacle.Type.RowBomb);
            rows[i].obstacles[0].obstacleTransform.localPosition = pos;
            ((Block) rows[i].obstacles[0]).spriteRenderer.sortingOrder = 1;
            
            yield return new WaitForSeconds(0.1f);
        }

        for (int i = 0; i < rowNum; i++)
        {
            yield return new WaitForSeconds(0.1f);
            rows[i].obstacles[0].SpecifyResistance(0);
        }

        OnLastRowsDestroyed?.Invoke();
        StartCoroutine("CleanUpUnusedRows");
        
        Controller.GetInstance.IsDisable = false;
    }
}
