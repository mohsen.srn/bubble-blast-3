﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using _02_Scripts.Obstacles;
using _02_Scripts.ScriptableObjects;
using UnityEngine;

namespace _02_Scripts
{
    public class Row : MonoBehaviour
    {
        public List<Obstacle> obstacles = new List<Obstacle>();
        public Transform rowTransform;
        public GameObject rowGameObject;
        private void Awake()
        {
            rowTransform = transform;
            rowGameObject = gameObject;
        }

        private void Start()
        {
            CreateRow();
        }

        public void UpdateObjectsInThisRow(List<Obstacle> blocks)
        {
            obstacles = blocks;
            for (var index = 0; index < obstacles.Count; index++)
            {
                var obstacle = obstacles[index];
                if (obstacle.obstacleType != Obstacle.Type.Null)
                    obstacle.SetOwnerRow(this);
            }
            CreateRow();
        }

        public bool IsEmpty()
        {
            foreach (var child in rowTransform)
            {
                return false;
            }
            // here i specify 0 resistance to Null blocks to push them to pool
            for (int i = 0; i < obstacles.Count; i++)
            {
                if (obstacles[i] is NullObstacle)
                {
                    obstacles[i].SpecifyResistance(0);
                }
            }
            return true;
        }

        private void CreateRow()
        {
            int blockNumbers = obstacles.Count;
            for (int i = 0; i < blockNumbers; i++)
            {
                obstacles[i].obstacleTransform.localPosition = 
                    new Vector3(i - (blockNumbers - 1)/2.0f ,0f,0f);
            }
        }

        /// Here i can change row position with animation or without
        public void GoOneStepDown()
        {
            StartCoroutine(MoveToNewPosition());
        }

        public void GoOneStepDownInstantly()
        {
            float y = rowTransform.localPosition.y;
            rowTransform.localPosition = new Vector3(0f, y-1, 0f );
        }

        IEnumerator MoveToNewPosition()
        {
            float y = rowTransform.localPosition.y;
            while (Vector3.Distance(new Vector3(0f, y-1, 0f ),
                       rowTransform.position) >= 0.5f 
                   && rowTransform.localPosition.y > y-1) // this prevent from golding on trap and looping
            {
                var position = rowTransform.localPosition;
                var dirNormalized = (position - new Vector3(0f, y-1, 0f )).normalized;
                position += -3 * Time.deltaTime * dirNormalized;
                rowTransform.localPosition = position;
                yield return null;
            }

            rowTransform.localPosition = new Vector3(0f, y-1, 0f );
        }

        /// <summary>
        /// This method cleans row from 0 resistance blocks.
        /// these 0 resistance blocks will dirty saved file and sometimes make
        /// restores board version differs from last playing board.
        /// </summary>
        public void CleanUpRow()
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                if (obstacles[i].Resistance == 0)
                {
                    if (!(obstacles[i] is NullObstacle))
                    {
                        obstacles[i] = PoolManager.Instance.GetNull().GetComponent<NullObstacle>();
                    }
                }
            }
        }
        
        // i convert this row to some object that is serializable and can be stored to a file
        public StageRow Convert2SerializableObject(bool flag)
        {
            StageRow stageRow = new StageRow();
            List<StageObstacle> stageObstacles = new List<StageObstacle>();
            
            for (var index = 0; index < obstacles.Count; index++)
            {
                var obstacle = obstacles[index];
                switch (obstacle.obstacleType)
                {
                    case Obstacle.Type.Null:
                        stageObstacles.Add(new StageObstacle(StageObstacle.ObstacleType.__,
                            obstacle.Resistance));
                        break;
                    case Obstacle.Type.Ordinary:
                        stageObstacles.Add(new StageObstacle(StageObstacle.ObstacleType.LJ,
                            obstacle.Resistance));

                        if (obstacle.Resistance == ArcadeBlockManager.Instance.GameLevel
                            && !flag)
                        {
                            throw new Exception("ُBug in an already solved piece of code");
                        }

                        break;
                    case Obstacle.Type.BoxBomb:
                        stageObstacles.Add(new StageObstacle(StageObstacle.ObstacleType.BOXBMB,
                            obstacle.Resistance));
                        break;
                    case Obstacle.Type.RowBomb:
                        stageObstacles.Add(new StageObstacle(StageObstacle.ObstacleType.ROWBMB,
                            obstacle.Resistance));
                        break;
                    case Obstacle.Type.Ball:
                        stageObstacles.Add(new StageObstacle(StageObstacle.ObstacleType.O,
                            obstacle.Resistance));
                        break;
                }
            }
            stageRow.stageObstacles = stageObstacles;
            return stageRow;
        }
    }
}
