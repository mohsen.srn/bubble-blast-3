﻿using System;
using System.Collections;
using System.Text;
using _02_Scripts.Helper_Classes;
using Firebase.Analytics;
using I2.Loc;
using NaughtyAttributes;
using RTLTMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityBackStack;

public class HomeUI : MonoBehaviour_BackStackSupport
{
    public string ArcadeSceneName;
    public string StageSceneName;
    public string LevelDesignSceneName;

    public RTLTextMeshPro englishRecord;
    public RTLTextMeshPro farsiRecord;

    private bool _isStoreOpen;

    public GameObject selectLanguageDialog;
    public GameObject darkLayout;

    public GameObject setUsernameDialog;

    public Button videoAdRewardBtn;

    public GameObject leaderBoardDialog;

    private void Start()
    {
        if (AdManager.GetInstance != null)
        {
            AdManager.GetInstance.HideBannerAd();
        }

        StoreManager.OnStoreOpen += () => { _isStoreOpen = true;};
        StoreManager.OnStoreClose += () => { _isStoreOpen = false;};

        string bestRecord = "";
        if (PlayerPrefs.HasKey("Best Record"))
            bestRecord = PlayerPrefs.GetInt("Best Record", 0).ToString();
        else
        {
            bestRecord = "0";
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (LocalizationManager.CurrentLanguage == LocalizationManager.GetAllLanguages()[0])
        {
            englishRecord.gameObject.SetActive(true);
            farsiRecord.gameObject.SetActive(false);
            
            stringBuilder.Append("Best record: ").Append(bestRecord);
            englishRecord.text = stringBuilder.ToString();
        }
        else
        {
            englishRecord.gameObject.SetActive(false);
            farsiRecord.gameObject.SetActive(true);
            
            stringBuilder.Append("بهترین امتیاز: ").Append(bestRecord);
            farsiRecord.text = stringBuilder.ToString();
        }

        selectLanguageDialog.SetActive(false);
        darkLayout.SetActive(false);
        if (!PlayerPrefs.HasKey("first Open"))
        {
            selectLanguageDialog.SetActive(true);
            darkLayout.SetActive(true);
        }
        else
        {
            if (PlayerPrefs.HasKey(PlayerPrefsKeys.USER_INFO))
            {
                UserInfo userInfo = JsonUtility.FromJson<UserInfo>(PlayerPrefs.GetString(PlayerPrefsKeys.USER_INFO));
                if (userInfo.username.Equals(""))
                {
                    setUsernameDialog.SetActive(true);
                }
                else
                {
                    setUsernameDialog.SetActive(false);
                }
            }
            else
            {
                setUsernameDialog.SetActive(true);
            }
        }
        
        StartCoroutine(EnableVideoAdRewardBtnWithDelay());

        StartCoroutine(GetMyInvitesWithDelay());
    }

    IEnumerator GetMyInvitesWithDelay()
    {
        yield return new WaitForSeconds(1f);
        NetworkingCodes.GetInstance.GetMyInvites();
    }

    IEnumerator EnableVideoAdRewardBtnWithDelay()
    {
        videoAdRewardBtn.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        if (AdManager.GetInstance != null)
        {
            videoAdRewardBtn.gameObject.SetActive(AdManager.GetInstance.IsRewardAdLoaded());
            videoAdRewardBtn.gameObject.GetComponent<Animator>().SetBool("animate", true);
        }
    }

    public void SetLang()
    {
        selectLanguageDialog.SetActive(false);
        darkLayout.SetActive(false);

        if (PlayerPrefs.HasKey(PlayerPrefsKeys.USER_INFO))
        {
            UserInfo userInfo = JsonUtility.FromJson<UserInfo>(PlayerPrefs.GetString(PlayerPrefsKeys.USER_INFO));
            if (userInfo.username.Equals(""))
            {
                setUsernameDialog.SetActive(true);
            }
            else
            {
                setUsernameDialog.SetActive(false);
            }
        }
        else
        {
            setUsernameDialog.SetActive(true);
        }
        
        PlayerPrefs.SetInt("first Open", 1);
    }

    public void OpenLanguageDialog()
    {
        selectLanguageDialog.SetActive(true);
        darkLayout.SetActive(true);
    }

    public void Arcade()
    {
        BackStackManager.GetInstance.ResetBackStack();
        ScreenChanger.GetInstance.Go2Scene(ArcadeSceneName);
        // SceneManager.LoadScene(ArcadeSceneName);
    }

    public void Stage()
    {
        ScreenChanger.GetInstance.Go2Scene(StageSceneName);
        // SceneManager.LoadScene(StageSceneName);
    }                 

    public void LevelDesign()
    {
        SceneManager.LoadScene(LevelDesignSceneName);
    }

    public GameObject ExitDialog;
    public void Close()
    {
        ExitDialog.SetActive(true);
    }

    public void ShowTutorial()
    {
        Tutorial.Instance.OpenTutorial();
    }

    public void OpenStore()
    { 
        FirebaseAnalytics.LogEvent(
            "BubbleBlast_Click_Home_OpenStore",
            new Parameter("Coin repository", CoinManager.GetInstance.GetCoins()));

        StoreManager.GetInstance.Open();
    }

    public void OpenBallStore()
    {
        FirebaseAnalytics.LogEvent("BubbleBlast_Click_Home_OpenBallStore");

        BallStore.GetInstance.OpenBallStoreDialog();
    }

    public override void BackCalled()
    {
        Close();
    }

    public void LoadVideoAd()
    {
        AdManager.GetInstance.ShowRewardedAd(
            () => {CoinManager.GetInstance.IncreaseCoins(25);},
            () => {});
    }

    public GameObject copyRightDialog;
    public void Click_OpenCopyRight()
    {
//        darkLayout.SetActive(true);
        copyRightDialog.SetActive(true);
    }

//    public void Click_CloseCopyRight()
//    {
//        darkLayout.SetActive(false);
//        copyRightDialog.SetActive(false);
//    }

    public GameObject noInternetDialog;
    public void Click_OpenLeaderBoard()
    {
        if (PlayerPrefs.HasKey(PlayerPrefsKeys.LEADER_BOARD))
        {
            leaderBoardDialog.SetActive(true);
        }
        else
        {
            noInternetDialog.SetActive(true);
        }
    }

    [BoxGroup("Dialogs")]public GameObject inviteDialog;
    public void Click_OpenInviteDialog()
    {
        inviteDialog.SetActive(true);
    }
}
