﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityBackStack;
using UnityEngine;

public class ExitDialog : MonoBehaviour_BackStackSupport
{
    public string PrivacyPolicyUrl = "https://app.termly.io/document/privacy-policy/fafa3df6-66db-4bc0-8c61-96adf0708127";
    public GameObject DarkPanel;
    private Animator _animator;
    private void Start()
    {
        _animator = GetComponent<Animator>();
//        gameObject.SetActive(false);
    }
    
    /*public void enableDialog()
    {
        gameObject.SetActive(false);
    }*/

    private void OnEnable()
    {
        DarkPanel.SetActive(true);
        //_animator.SetTrigger("Enter");
    }

    public void PrivacyPolicyClicked()
    {
        Application.OpenURL(PrivacyPolicyUrl);
    }

    public void QuiteGame()
    {
        Application.Quit();
    }

    public void Exit()
    {
        _animator.SetTrigger("Close");
    }

    public void MakeDialogDisable()
    {
        DarkPanel.SetActive(false);
        gameObject.SetActive(false);
    }

    public override void BackCalled()
    {
        Exit();
        BackStackManager.GetInstance.Push2BackStack(FindObjectOfType<HomeUI>());
    }
}
