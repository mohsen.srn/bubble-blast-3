using System;
using System.Collections;
using _02_Scripts.Helper_Classes;
using I2.Loc;
using RTLTMPro;
using TMPro;
using UnityBackStack;
using UnityEngine;
using UnityEngine.UI;

namespace _02_Scripts._00_Home.Dialogs
{
    public class InviteCodeDialog : MonoBehaviour_BackStackSupport
    {
        public GameObject darkLayout;
        public RTLTextMeshPro faInviteCode;
        public TextMeshProUGUI enInviteCode;

        public RTLTextMeshPro faWarning;
        public GameObject enSection, faSection;
        public GameObject faButtonText, enButtonText;

        public TMP_InputField friendInviteCode;

        private void OnEnable()
        {
            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("open");
            
            darkLayout.SetActive(true);
            faInviteCode.text = GameManager.GetInstance.GetUserInfo().invite_code;
            enInviteCode.text = GameManager.GetInstance.GetUserInfo().invite_code;

            if (LocalizationManager.CurrentLanguage 
                == LocalizationManager.GetAllLanguages()[0])
            {
                enSection.SetActive(true);
                faSection.SetActive(false);
                
                enButtonText.SetActive(true);
                faButtonText.SetActive(false);
            }
            else
            {
                enSection.SetActive(false);
                faSection.SetActive(true);
                
                enButtonText.SetActive(false);
                faButtonText.SetActive(true);
            }
        }

        public override void BackCalled()
        {
            darkLayout.SetActive(false);
            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("close");
            
            StartCoroutine(Disable());
        }

        IEnumerator Disable()
        {
            yield return new WaitForSeconds(1f);
            gameObject.SetActive(false);
        }

        public void Click_OkSendCode()
        {
            NetworkingCodes.GetInstance.UseInviteCode(friendInviteCode.text, 
                (int coinGot) =>
                {
                    CoinManager.GetInstance.IncreaseCoins(coinGot);
                },
                (string msg) => { faWarning.text = msg; });
        }

        public void ShareBtn()
        {
            NetworkingCodes.GetInstance.GetMyInvites();
        }
    }
}
