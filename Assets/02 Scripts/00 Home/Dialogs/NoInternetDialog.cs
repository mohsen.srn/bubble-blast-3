﻿using I2.Loc;
using UnityBackStack;
using UnityEngine;

public class NoInternetDialog : MonoBehaviour_BackStackSupport
{
    public GameObject darkLayout;
    public GameObject faMsg, enMsg;
    private void OnEnable()
    {
        darkLayout.SetActive(true);
        if (LocalizationManager.CurrentLanguage 
            == LocalizationManager.GetAllLanguages()[0])
        {
            faMsg.SetActive(false);
            enMsg.SetActive(true);
        }
        else
        {
            faMsg.SetActive(true);
            enMsg.SetActive(false);
        }
    }

    public override void BackCalled()
    {
        Click_Close();
    }

    public void Click_Close()
    {
        darkLayout.SetActive(false);
        gameObject.SetActive(false);
    }
}
