using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Helper_Classes;
using I2.Loc;
using TMPro;
using Unity.Collections;
using UnityBackStack;
using UnityEngine;

namespace _02_Scripts._00_Home.Dialogs
{
    public class SetUserNameDialog : MonoBehaviour_BackStackSupport
    {
        private string _userName = "";
        public GameObject darkLayout;
        public TMP_InputField inputField;

        public GameObject Fa_Message;
        public GameObject En_Message;
        private void Start()
        {
            if (PlayerPrefs.HasKey(PlayerPrefsKeys.USER_INFO))
            {
                UserInfo userInfo = JsonUtility.FromJson<UserInfo>(PlayerPrefs.GetString(PlayerPrefsKeys.USER_INFO));
                if (userInfo.username.Equals(""))
                {
                    OpenDialog();
                }
                else
                {
                    CloseDialog();
                }
            }
            else
            {
                OpenDialog();
            }
        }

        private void OpenDialog()
        {
            if (LocalizationManager.CurrentLanguage == LocalizationManager.GetAllLanguages()[0])
            {
                En_Message.gameObject.SetActive(true);
                Fa_Message.gameObject.SetActive(false);
            }
            else
            {
                En_Message.gameObject.SetActive(false);
                Fa_Message.gameObject.SetActive(true);
            }

            gameObject.SetActive(true);
            darkLayout.SetActive(true);
            
            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("open");

        }

        private void CloseDialog()
        {
            darkLayout.SetActive(false);
            BackStackManager.GetInstance.RemoveFromBackStack();

            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("close");

            StartCoroutine(Disable());
        }
        IEnumerator Disable()
        {
            yield return new WaitForSeconds(1f);
            gameObject.SetActive(false);
        }
        public override void BackCalled()
        {
            // do nothing
        }

        public void SetUsername()
        {
            _userName = inputField.text;
        }

        public void Click_Ok()
        {
            if (_userName.Equals(""))
            {
                // do nothing
            }
            else
            {
                GameManager.GetInstance.InitUserInfo(_userName);
                NetworkingCodes.GetInstance.RegisterUser();
                
                CloseDialog();
            }
        }
    }
}
