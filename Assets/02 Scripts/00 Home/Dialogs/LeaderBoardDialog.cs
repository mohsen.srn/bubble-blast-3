using System;
using System.Collections;
using _02_Scripts.Helper_Classes;
using _02_Scripts.Helper_Classes.Models;
using NaughtyAttributes;
using TMPro;
using UnityBackStack;
using UnityEngine;

namespace _02_Scripts._00_Home.Dialogs
{
    public class LeaderBoardDialog : MonoBehaviour_BackStackSupport
    {
        public GameObject darkLayout;
        private Animator _animator;

        public RectTransform content;
        
        [BoxGroup("1")]public TextMeshProUGUI username1;
        [BoxGroup("2")]public TextMeshProUGUI username2;
        [BoxGroup("3")]public TextMeshProUGUI username3;
        [BoxGroup("4")]public TextMeshProUGUI username4;
        [BoxGroup("5")]public TextMeshProUGUI username5;
        [BoxGroup("6")]public TextMeshProUGUI username6;
        [BoxGroup("7")]public TextMeshProUGUI username7;
        [BoxGroup("8")]public TextMeshProUGUI username8;
        [BoxGroup("9")]public TextMeshProUGUI username9;
        [BoxGroup("10")]public TextMeshProUGUI username10;
        [BoxGroup("11")]public TextMeshProUGUI username11;
        [BoxGroup("12")]public TextMeshProUGUI username12;
        [BoxGroup("13")]public TextMeshProUGUI username13;
        [BoxGroup("14")]public TextMeshProUGUI username14;
        [BoxGroup("15")]public TextMeshProUGUI username15;

        [BoxGroup("1")]public TextMeshProUGUI  record1;
        [BoxGroup("2")]public TextMeshProUGUI  record2;
        [BoxGroup("3")]public TextMeshProUGUI  record3;
        [BoxGroup("4")]public TextMeshProUGUI  record4;
        [BoxGroup("5")]public TextMeshProUGUI  record5;
        [BoxGroup("6")]public TextMeshProUGUI  record6;
        [BoxGroup("7")]public TextMeshProUGUI  record7;
        [BoxGroup("8")]public TextMeshProUGUI  record8;
        [BoxGroup("9")]public TextMeshProUGUI  record9;
        [BoxGroup("10")]public TextMeshProUGUI record10;
        [BoxGroup("11")]public TextMeshProUGUI record11;
        [BoxGroup("12")]public TextMeshProUGUI record12;
        [BoxGroup("13")]public TextMeshProUGUI record13;
        [BoxGroup("14")]public TextMeshProUGUI record14;
        [BoxGroup("15")]public TextMeshProUGUI record15;

        public override void BackCalled()
        {
            Click_Close();
        }

        private void OnEnable()
        {
            _animator = GetComponent<Animator>();
            Open();
            LeaderBoardModel boardModel =
                JsonUtility.FromJson<LeaderBoardModel>(
                    PlayerPrefs.GetString(PlayerPrefsKeys.LEADER_BOARD));

            TopUser[] topUsers = new TopUser[15];
            for (int i = 0; i < topUsers.Length; i++)
            {
                topUsers[i] = new TopUser();
            }
            for (int i = 0; i < boardModel.top_users.Length; i++)
            {
                topUsers[i] = boardModel.top_users[i];
            }

            username1 .text = topUsers[0].username;
            username2 .text = topUsers[1].username;
            username3 .text = topUsers[2].username;
            username4 .text = topUsers[3].username;
            username5 .text = topUsers[4].username;
            username6 .text = topUsers[5].username;
            username7 .text = topUsers[6].username;
            username8 .text = topUsers[7].username;
            username9 .text = topUsers[8].username;
            username10.text = topUsers[9].username;
            username11.text = topUsers[10].username;
            username12.text = topUsers[11].username;
            username13.text = topUsers[12].username;
            username14.text = topUsers[13].username;
            username15.text = topUsers[14].username;
            
            record1 .text = topUsers[0].score .ToString();
            record2 .text = topUsers[1].score .ToString();
            record3 .text = topUsers[2].score .ToString();
            record4 .text = topUsers[3].score .ToString();
            record5 .text = topUsers[4].score .ToString();
            record6 .text = topUsers[5].score .ToString();
            record7 .text = topUsers[6].score .ToString();
            record8 .text = topUsers[7].score .ToString();
            record9 .text = topUsers[8].score .ToString();
            record10.text = topUsers[9].score .ToString();
            record11.text = topUsers[10].score.ToString();
            record12.text = topUsers[11].score.ToString();
            record13.text = topUsers[12].score.ToString();
            record14.text = topUsers[13].score.ToString();
            record15.text = topUsers[14].score.ToString();
        }

        public void Open()
        {
            _animator.ResetTrigger("close");
            _animator.ResetTrigger("open");
            
            _animator.SetTrigger("open");
            darkLayout.SetActive(true);
            
//            content.SetPositionAndRotation(Vector2.zero, Quaternion.identity);
        }  

        public void Click_Close()
        {
            _animator.ResetTrigger("close");
            _animator.ResetTrigger("open");
            
            _animator.SetTrigger("close");
            StartCoroutine(CloseCr());
        }

        IEnumerator CloseCr()
        {
            yield return new WaitForSeconds(0.5f);
            darkLayout.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
