using System;
using System.Collections;
using UnityBackStack;
using UnityEngine;

namespace _02_Scripts._00_Home
{
    public class CopyrightDialog : MonoBehaviour_BackStackSupport
    {
        public GameObject darkLayout;
        private void Start()
        {
            
        }

        private void OnEnable()
        {
            darkLayout.SetActive(true);
            
            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("open");
        }
        
        public override void BackCalled()
        {
            darkLayout.SetActive(false);
            
            GetComponent<Animator>().ResetTrigger("open");
            GetComponent<Animator>().ResetTrigger("close");
            GetComponent<Animator>().SetTrigger("close");
            
            StartCoroutine(Disable());
        }
        
        IEnumerator Disable()
        {
            yield return new WaitForSeconds(1f);
            gameObject.SetActive(false);
        }
    }
}
