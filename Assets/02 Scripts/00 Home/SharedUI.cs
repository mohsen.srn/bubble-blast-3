﻿using UnityEngine;
using UnityEngine.UI;

public class SharedUI : MonoBehaviour
{
    public GameObject DialogSetting;
    public GameObject DarkPanel;

    public Sprite EnableMusic, DisableMusic, EnableSound, DisableSound;
    public Image SoundIcon, MusicIcon;

    private SettingManager _settingManager;

    public static SharedUI Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
//            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    void Start()
    {
        CloseDialogSettings();
        _settingManager = FindObjectOfType<SettingManager>();
    }

    private Controller _controller; 
    public void CloseDialogSettings()
    {
        if(_controller != null) _controller.IsDisable = false;
   
        DialogSetting.SetActive(false);
        DarkPanel.SetActive(false);
    }
    public void OpenDialogSettings()
    {
        _controller = FindObjectOfType<Controller>();
        if(_controller != null) _controller.IsDisable = true;
                
        SoundIcon.sprite = _settingManager.IsSoundEnabled() ? EnableSound : DisableSound;
        MusicIcon.sprite = _settingManager.IsMusicEnabled() ? EnableMusic : DisableMusic;

        DialogSetting.SetActive(true);
        DarkPanel.SetActive(true);
    }

    public void ToggleSound()
    {
        bool sound = _settingManager.IsSoundEnabled();
        _settingManager.SetSound(!sound);
        SoundIcon.sprite = _settingManager.IsSoundEnabled() ? EnableSound : DisableSound;
    }

    public void ToggleMusic()
    {
        bool music = _settingManager.IsMusicEnabled();
        _settingManager.SetMusic(!music);
        MusicIcon.sprite = _settingManager.IsMusicEnabled() ? EnableMusic : DisableMusic;
    }
}
