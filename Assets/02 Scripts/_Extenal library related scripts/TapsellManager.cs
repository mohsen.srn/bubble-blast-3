﻿using System;
using _02_Scripts._Extenal_library_related_scripts.Tapsell_Codes;
using TapsellSDK;

public class TapsellManager : AdManager
{
    private TapsellBannerManager _tapsellBannerManager;
    private TapsellInterstitialManager _tapsellInterstitialManager;
    private TapsellRewardedAdManager _tapsellRewardedAdManager;
    
    /*private void Awake()
    {

    }*/

    void Start()
    {
        _tapsellBannerManager = gameObject.AddComponent<TapsellBannerManager>();
        _tapsellInterstitialManager = gameObject.AddComponent<TapsellInterstitialManager>();
        _tapsellRewardedAdManager = gameObject.AddComponent<TapsellRewardedAdManager>();
        InitAd();
    }

    public override void InitAd()
    {
        if (GameManager.GetInstance.showAds)
        {
            appId = GameManager.GetInstance.tapsellCodes.appId;
            bannerId = GameManager.GetInstance.tapsellCodes.bannerId;
            interstitialId = GameManager.GetInstance.tapsellCodes.interstitialId;
            videoAwardId = GameManager.GetInstance.tapsellCodes.videoRewardId;
            nativeId = GameManager.GetInstance.tapsellCodes.nativeId;
        }
        else
        {
            appId = GameManager.GetInstance.tapsellCodes.appId;
            bannerId = "";
            interstitialId = "";
            videoAwardId = "";
            nativeId = "";
        }
        
        Tapsell.Initialize(appId);

        if (_tapsellBannerManager!=null)
        {
            _tapsellBannerManager.RequestBanner(bannerId);
            _tapsellInterstitialManager.RequestInterstitial(interstitialId);
            _tapsellRewardedAdManager.RequestRewardedAd(videoAwardId);
        }
    }

    public override void ShowBannerAd()
    {
        _tapsellBannerManager.ShowBanner();
    }

    public override void HideBannerAd()
    {
        try
        {
            _tapsellBannerManager.HideBanner();
        }
        catch
        {
            // ignored
        }
    }

    public override void ShowInterstitialAd()
    {
        _tapsellInterstitialManager.ShowAd();
    }

    public override bool IsRewardAdLoaded()
    {
        return _tapsellRewardedAdManager.isAdLoaded;
    }

    public override void ShowRewardedAd(Action successAction, Action failedAction)
    {
        _tapsellRewardedAdManager.ShowRewardedAd(successAction, failedAction);
    }

    private void OnDestroy()
    {
        Destroy(_tapsellBannerManager);
        Destroy(_tapsellInterstitialManager);
        Destroy(_tapsellRewardedAdManager);
    }
}
