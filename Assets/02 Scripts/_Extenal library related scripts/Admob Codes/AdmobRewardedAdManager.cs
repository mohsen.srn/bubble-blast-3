using System;
using _02_Scripts.Helper_Classes;
using GoogleMobileAds.Api;
using UnityEngine;

namespace _02_Scripts._Extenal_library_related_scripts.Admob_Codes
{
    public class AdmobRewardedAdManager : MonoBehaviour
    {
        private RewardedAd _rewardedAd;
        private string _adUnitId;

        public void RequestRewardedAd(string adUnitId)
        {
            _adUnitId = adUnitId;
            _rewardedAd = new RewardedAd(adUnitId);

            // Called when an ad request has successfully loaded.
            this._rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            // Called when an ad request failed to load.
            this._rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
            // Called when an ad is shown.
            this._rewardedAd.OnAdOpening += HandleRewardedAdOpening;
            // Called when an ad request failed to show.
            this._rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
            // Called when the user should be rewarded for interacting with the ad.
            this._rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            // Called when the ad is closed.
            this._rewardedAd.OnAdClosed += HandleRewardedAdClosed;

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the rewarded ad with the request.
            _rewardedAd.LoadAd(request);
        }

        public bool IsVideoLoaded()
        {
            return _rewardedAd.IsLoaded();
        }

        private Action _rewardGrantedAction;
        private Action _failedToShowRewardAdAction;
        public void Show(Action successAction, Action failedAction)
        {
            _rewardGrantedAction = successAction;
            _failedToShowRewardAdAction = failedAction;
            if (_rewardedAd.IsLoaded())
            {
                _rewardedAd.Show();
            }
        }

/************************ events *********************************/
        private void HandleRewardedAdLoaded(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardedAdLoaded event received");
        }

        private void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
        {
            MonoBehaviour.print(
                "HandleRewardedAdFailedToLoad event received with message: "
                + args.Message);
        }

        private void HandleRewardedAdOpening(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardedAdOpening event received");
        }
        
        private void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
        {
            if (GameManager.GetInstance.releaseMode == GameManager.ReleaseMode.Debug)
            {
                print(
                    "HandleRewardedAdFailedToShow event received with message: "
                    + args.Message);
            }

            UnityMainThread.GetInstance.AddJobs(_failedToShowRewardAdAction);

        }

        private void HandleRewardedAdClosed(object sender, EventArgs args)
        {
            // when video reward closed, we request another one
            RequestRewardedAd(_adUnitId);
            
            MonoBehaviour.print("HandleRewardedAdClosed event received");
        }

        private void HandleUserEarnedReward(object sender, Reward args)
        {
            string type = args.Type;
            double amount = args.Amount;
            MonoBehaviour.print(
                "HandleRewardedAdRewarded event received for "
                + amount.ToString() + " " + type);
            
            UnityMainThread.GetInstance.AddJobs(_rewardGrantedAction);
        }
    }
}
