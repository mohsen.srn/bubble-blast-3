﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdmobInterstitialManager : MonoBehaviour
{
    private InterstitialAd _interstitial;
    private string _adUnitId;
    public void RequestInterstitial(string adUnitId)
    {
        _adUnitId = adUnitId;
        // Initialize an InterstitialAd.
        this._interstitial = new InterstitialAd(adUnitId);
        
        // Called when an ad request has successfully loaded.
        this._interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this._interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        this._interstitial.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        this._interstitial.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this._interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this._interstitial.LoadAd(request);
    }

    public void ShowInterstitialAd()
    {
        if (this._interstitial.IsLoaded())
        {
            this._interstitial.Show();
        }
    }

    private void OnDestroy()
    {
        _interstitial.Destroy();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
//        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
//                            + args.Message);

        StartCoroutine(RequestAd());
    }
    
    /// <summary>
    /// When loading ad fails, wait for a few seconds and request again
    /// </summary>
    /// <returns></returns>
    IEnumerator RequestAd()
    {
        yield return new WaitForSeconds(5f);
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        _interstitial.LoadAd(request);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdClosed event received");
        RequestInterstitial(_adUnitId);
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
}
