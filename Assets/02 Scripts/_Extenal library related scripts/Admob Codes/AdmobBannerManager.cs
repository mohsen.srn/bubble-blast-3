﻿using System;
using System.Collections;
using System.Collections.Generic;
using _02_Scripts.Helper_Classes;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;

public class AdmobBannerManager : MonoBehaviour
{
    BannerView _bannerView;
    bool _isBannerReady;

    private Action _failed;
    private Action _success;
    public void RequestBanner(string bannerId, Action failed, Action success)
    {
        _failed = failed;
        _success = success;
        _bannerView = new BannerView(bannerId, AdSize.Banner, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        _bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        _bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        _bannerView.OnAdOpening += HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        _bannerView.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        _bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        _bannerView.LoadAd(request);
    }
    public void ShowBannerAd()
    {
        if (_isBannerReady) ; // && SceneManager.GetActiveScene().buildIndex != 0)
            _bannerView.Show();
    }
    
    public void HideBannerAd()
    {
        try
        {
            _bannerView.Hide();
        }
        catch
        {
            // ignored
        }
    }

    private void OnDestroy()
    {
        _bannerView.Destroy();
    }

/********************* events ********************** */

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        // MonoBehaviour.print("HandleAdLoaded event received");
        UnityMainThread.GetInstance.AddJobs(_success);
        _isBannerReady = true;
        ShowBannerAd();
    }
    
    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
//        StartCoroutine(RequestBanner());
        UnityMainThread.GetInstance.AddJobs(_failed);
    }

    /// <summary>
    /// When loading banner fails, wait for a few seconds and request again
    /// </summary>
    /// <returns></returns>
/*    IEnumerator RequestBanner()
    {
        yield return new WaitForSeconds(3f);
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        _bannerView.LoadAd(request);
    }*/

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
//        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
}
