﻿using System;
using System.Collections;
using _02_Scripts._Extenal_library_related_scripts.Admob_Codes;
using GoogleMobileAds.Api;
using UnityEngine;

public class AdmobManager : AdManager
{
    private AdmobBannerManager _admobBannerManager;
    private AdmobInterstitialManager _admobInterstitialManager;
    private AdmobRewardedAdManager _admobRewardedAdManager;

    void Start()
    {
        _admobBannerManager = gameObject.AddComponent<AdmobBannerManager>(); 
        _admobInterstitialManager = gameObject.AddComponent<AdmobInterstitialManager>();
        _admobRewardedAdManager = gameObject.AddComponent<AdmobRewardedAdManager>();
        InitAd();
    }

    public override void InitAd()
    {
        if (GameManager.GetInstance.showAds)
        {
            if (GameManager.GetInstance.releaseMode == GameManager.ReleaseMode.Debug)
            {
                appId = "ca-app-pub-5265276078374743~1676893654";
                bannerId = "ca-app-pub-3940256099942544/6300978111";
                interstitialId = "ca-app-pub-3940256099942544/1033173712";
                videoAwardId = "ca-app-pub-3940256099942544/5224354917";
                nativeId = "ca-app-pub-3940256099942544/2247696110";
            }
            else
            {
                appId = GameManager.GetInstance.gameAdmobCodes.appId;
                bannerId = GameManager.GetInstance.gameAdmobCodes.bannerId;
                interstitialId = GameManager.GetInstance.gameAdmobCodes.interstitialId;
                videoAwardId = GameManager.GetInstance.gameAdmobCodes.videoRewardId;
                nativeId = GameManager.GetInstance.gameAdmobCodes.nativeId;
            }
        }
        else
        {
            appId = GameManager.GetInstance.gameAdmobCodes.appId;
            bannerId = "";
            interstitialId = "";
            videoAwardId = "";
            nativeId = "";
        }

        MobileAds.Initialize(appId);
        if (_admobBannerManager != null)
        {
            _admobBannerManager.RequestBanner(bannerId,
                () => { StartCoroutine("RepeatBannerRequest");}
                , ()=>{StopCoroutine("RepeatBannerRequest");});
            _admobInterstitialManager.RequestInterstitial(interstitialId);
            _admobRewardedAdManager.RequestRewardedAd(videoAwardId);
        }
    }

    public override void ShowBannerAd()
    {
        _admobBannerManager.ShowBannerAd();
    }

    public override void HideBannerAd()
    {
        try
        {
            _admobBannerManager.HideBannerAd();
        }
        catch
        {
            // ignored
        }
    }

    public override void ShowInterstitialAd()
    {
        _admobInterstitialManager.ShowInterstitialAd();
    }

    public override bool IsRewardAdLoaded()
    {
        return _admobRewardedAdManager.IsVideoLoaded();
    }

    public override void ShowRewardedAd(Action successAction, Action failedAction)
    {
        _admobRewardedAdManager.Show(successAction, failedAction);
    }
    
    /*********************** helper method ***********************************/
    IEnumerator RepeatBannerRequest()
    {
        yield return new WaitForSeconds(3f);
        _admobBannerManager.RequestBanner(bannerId,null,null);
    }

    private void OnDestroy()
    {
        Destroy(_admobBannerManager);
        Destroy(_admobInterstitialManager);
        Destroy(_admobRewardedAdManager);
    }
}
