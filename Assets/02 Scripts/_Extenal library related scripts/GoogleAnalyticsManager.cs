﻿using Firebase.Analytics;
using UnityEngine;

public class GoogleAnalyticsManager : MonoBehaviour
{
    public static GoogleAnalyticsManager GetInstance;
    void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    public void OnGainNewRecord(int newRecord)
    {
        print("GoogleAnalyticsManager.OnGainNewRecord(int newRecord) / " + newRecord);
        try
        {
            FirebaseAnalytics.LogEvent
            (FirebaseAnalytics.EventPostScore,
                FirebaseAnalytics.ParameterScore, newRecord);
        }
        catch
        {
            // in android 5.1.1 and below firebase Analytics does not work
            // ignored
        }
    }

    public void OnShareBtnClicked()
    {
        print("GoogleAnalyticsManager.OnShareBtnClicked()");

        try
        {
            FirebaseAnalytics.LogEvent(
                FirebaseAnalytics.EventShare,
                new Firebase.Analytics.Parameter[]
                {
                    new Firebase.Analytics.Parameter(
                        Firebase.Analytics.FirebaseAnalytics.ParameterContentType,
                        "Share -> ParameterContentType"),
                    new Firebase.Analytics.Parameter(
                        Firebase.Analytics.FirebaseAnalytics.ParameterItemId,
                        "Share -> ParameterItemId"),
                    new Firebase.Analytics.Parameter(
                        Firebase.Analytics.FirebaseAnalytics.ParameterMethod,
                        "Share -> ParameterMethod"),
                }
            );
        }
        catch
        {
            // in android 5.1.1 and below firebase Analytics does not work
            // ignore
        }
    }
}
