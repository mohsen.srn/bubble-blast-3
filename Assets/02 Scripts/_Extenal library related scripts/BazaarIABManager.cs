using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using _02_Scripts.Helper_Classes.Models;
using UnityEngine;
using _02_Scripts.ScriptableObjects;
using BazaarPlugin;
using UnityEditor;
using UnityEngine.Assertions;

public class BazaarIABManager : MonoBehaviour
{
    /********************************* Singlton *****************************************/
    public static BazaarIABManager GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    /****************************************************************************************/

    public IABKey iabKeyKey;
    public string[] skus = new string[5]
    {
        "ordinary.product1",
        "ordinary.product2",
        "ordinary.product3",
        "ordinary.product4",
        "ordinary.product5"
    };

    public BazaarProducts bazaarProducts = BazaarProducts.GetDefaults();
    private void Start()
    {
        BazaarIAB.init(iabKeyKey.key);
    }

    public void RequestProductInfo()
    {
        BazaarIAB.querySkuDetails(skus);
    }

    public void SetProducts(List<BazaarSkuInfo> skus)
    {
        print("BazaarIABManager.SetProducts(List<BazaarSkuInfo> skus) -> skus.count = " + skus.Count);

        bazaarProducts.productOne = new BazaarProduct(skus[0].ProductId,
            skus[0].Title, skus[0].Price, skus[0].Type, skus[0].Description);
        
        bazaarProducts.productTwo = new BazaarProduct(
            skus[1].ProductId,
            skus[1].Title,
            skus[1].Price,
            skus[1].Type, 
            skus[1].Description);

        bazaarProducts.productThree = new BazaarProduct(
            skus[2].ProductId,
            skus[2].Title,
            skus[2].Price,
            skus[2].Type, 
            skus[2].Description);

        bazaarProducts.productFour = new BazaarProduct(
            skus[3].ProductId,
            skus[3].Title,
            skus[3].Price,
            skus[3].Type, 
            skus[3].Description);

        bazaarProducts.productFive = new BazaarProduct(
            skus[4].ProductId,
            skus[4].Title,
            skus[4].Price,
            skus[4].Type, 
            skus[4].Description);

        bazaarProducts.productSix = new BazaarProduct(
            skus[5].ProductId,
            skus[5].Title,
            skus[5].Price,
            skus[5].Type, 
            skus[5].Description);

        print("BazaarIABManager.SetProducts(List<BazaarSkuInfo> skus) -> bazaarProducts.ToString() = " + bazaarProducts.ToString());
        
        PlayerPrefs.SetString("Bazaar Products",bazaarProducts.ToString());
        PlayerPrefs.Save();
    }

    public void SetProducts()
    {
        string temp = PlayerPrefs.GetString("Bazaar Products");
        
        if(!temp.Equals(""))
            bazaarProducts = JsonUtility.FromJson<BazaarProducts>(temp);

        print("BazaarIABManager.SetProducts() -> PlayerPrefs.GetString(Bazaar Products) = " + temp);
        print("BazaarIABManager.SetProducts() -> PbazaarProducts = " + bazaarProducts);
    }

    private Action _onPurchaseDone;
    public void Purchase(int skuIndex, Action onPurchaseDone)
    {
        string sku = skus[skuIndex];
        _onPurchaseDone = onPurchaseDone;
        BazaarIAB.purchaseProduct(sku);
    }

    public void PurchaseDone(BazaarPurchase purchase)
    {
        BazaarProduct purchasedProduct = new BazaarProduct();
        if (bazaarProducts.productOne.ProductId.Equals(purchase.ProductId))
        {
            purchasedProduct = bazaarProducts.productOne;
        } else if (bazaarProducts.productTwo.ProductId.Equals(purchase.ProductId))
        {
            purchasedProduct = bazaarProducts.productTwo;
        } else if (bazaarProducts.productThree.ProductId.Equals(purchase.ProductId))
        {
            purchasedProduct = bazaarProducts.productThree;
        } else if (bazaarProducts.productFour.ProductId.Equals(purchase.ProductId))
        { 
            purchasedProduct = bazaarProducts.productFour;
        } else if (bazaarProducts.productFive.ProductId.Equals(purchase.ProductId))
        { 
            purchasedProduct = bazaarProducts.productFive;
        } else if (bazaarProducts.productSix.ProductId.Equals(purchase.ProductId))
        {
            purchasedProduct = bazaarProducts.productSix;
        }
        
        // in product filed we put coin number that we should give
        int coinNumber = Int32.Parse(purchasedProduct.Description);
        CoinManager.GetInstance.IncreaseCoins(coinNumber);
        _onPurchaseDone?.Invoke();

        BazaarIAB.consumeProduct(purchase.ProductId);
        
        Debug.Log("BazaarIABManager.PurchaseDone -> Error: user bought a product that has no corresponding in app");
    }
}

[Serializable]
public struct BazaarProducts
{
    public BazaarProduct productOne;
    public BazaarProduct productTwo;
    public BazaarProduct productThree;
    public BazaarProduct productFour;
    public BazaarProduct productFive;
    public BazaarProduct productSix;

    public BazaarProducts(BazaarProduct productOne, BazaarProduct productTwo, BazaarProduct productThree, BazaarProduct productFour, BazaarProduct productFive, BazaarProduct productSix)
    {
        this.productOne = productOne;
        this.productTwo = productTwo;
        this.productThree = productThree;
        this.productFour = productFour;
        this.productFive = productFive;
        this.productSix = productSix;
    }

    public BazaarProduct GetProduct(string productId)
    {
        if (productOne.ProductId.Equals(productId))
        {
            return productOne;
        } else if (productTwo.ProductId.Equals(productId))
        {
            return productTwo;
        } else if (productThree.ProductId.Equals(productId))
        {
            return productThree;
        } else if (productFour.ProductId.Equals(productId))
        { 
            return productFour;
        } else if (productFive.ProductId.Equals(productId))
        { 
            return productFive;
        } else if (productSix.ProductId.Equals(productId))
        {
            return productSix;
        }
        
        /*if (GameManager.GetInstance.isDebugMode)
        {
            throw new Exception("BazaarProducts.GetProduct(string productId) -> try to get wrong productID");
        }*/
        return BazaarProduct.GetDefaults();
    }

    public static BazaarProducts GetDefaults()
    {
        return new BazaarProducts(
            BazaarProduct.GetDefaults(),
            BazaarProduct.GetDefaults(),
            BazaarProduct.GetDefaults(),
            BazaarProduct.GetDefaults(),
            BazaarProduct.GetDefaults(),
            BazaarProduct.GetDefaults());
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
