﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TapsellSDK;

public class TapsellInterstitialManager : MonoBehaviour
{
    private string _adUnitId;
    private TapsellAd _ad;
    public void RequestInterstitial(string adUnitId)
    {
        _adUnitId = adUnitId;
        
        Tapsell.RequestAd(adUnitId, false, 
            (TapsellAd result) => {
                // onAdAvailable
                Debug.Log("Action: onAdAvailable");
                _ad = result; // store this to show the ad later
            },

            (string zoneId) => {
                // onNoAdAvailable
                Debug.Log("No Ad Available");
                StartCoroutine(RequestAgain(60f));
            },

            (TapsellError error) => {
                // onError
                Debug.Log(error.message);
                StartCoroutine(RequestAgain(30f));
            },

            (string zoneId) => {
                // onNoNetwork
                Debug.Log("No Network");
                StartCoroutine(RequestAgain(60f));
            },

            (TapsellAd result) => {
                // onExpiring
                Debug.Log("Expiring");
                // this ad is expired, you must download a new ad for this zone
                StartCoroutine(RequestAgain(5f));
            }
        );
        
        Tapsell.SetRewardListener( (TapsellAdFinishedResult result) => 
            {
                // you may give rewards to user if result.completed and
                // result.rewarded are both true
                StartCoroutine(RequestAgain(10f));
            }
        );
    }

    IEnumerator RequestAgain(float delay)
    {
        yield return new WaitForSeconds(delay);
        RequestInterstitial(_adUnitId);
    }

    public void ShowAd()
    {
        if (_ad != null)
        {
            print("Mohsen, Mohsen => (TapsellAd != null)");

            TapsellShowOptions showOptions = new TapsellShowOptions
            {
                backDisabled = true,
                immersiveMode = false,
                rotationMode = TapsellShowOptions.ROTATION_UNLOCKED,
                showDialog = true
            };

            Tapsell.ShowAd(_ad, showOptions);
        }
        else
        {
            print("Mohsen, Mohsen => (TapsellAd == null)");
            RequestInterstitial(_adUnitId);
        }
    }
}
