using System;
using System.Collections;
using TapsellSDK;
using UnityEngine;

namespace _02_Scripts._Extenal_library_related_scripts.Tapsell_Codes
{
    public class TapsellRewardedAdManager : MonoBehaviour
    {
        private TapsellAd _ad;
        private string _adUnitId;
        public bool isAdLoaded;
        
        public void RequestRewardedAd(string adUnitId)
        {
            _adUnitId = adUnitId;
            
            Tapsell.RequestAd(adUnitId, true, 
                (TapsellAd result) => {
                    // onAdAvailable
                    Debug.Log("Action: onAdAvailable");
                    
                    isAdLoaded = true;
                    _ad = result; // store this to show the ad later
                },

                (string zoneId) => {
                    // onNoAdAvailable
                    Debug.Log("No Ad Available");

                    isAdLoaded = false;
                },

                (TapsellError error) => {
                    // onError
                    Debug.Log(error.message);
                    
                    isAdLoaded = false;
                },

                (string zoneId) => {
                    // onNoNetwork
                    Debug.Log("No Network");
                    
                    isAdLoaded = false;
                },

                (TapsellAd result) => {
                    // onExpiring
                    Debug.Log("Expiring");
                    // this ad is expired, you must download a new ad for this zone
                    isAdLoaded = false;
                    StartCoroutine(RequestWithDelay(1f));
                }
            );
            
            Tapsell.SetRewardListener( (TapsellAdFinishedResult result) => 
                {
                    // you may give rewards to user if result.completed and
                    // result.rewarded are both true

                    if ( result.completed && result.rewarded )
                    {
                        _rewardGrantedAction?.Invoke();
                    }
                    else
                    {
                        _failedToShowRewardAdAction?.Invoke();
                    }
                    
                    // after finishing video add, we request another one
                    RequestRewardedAd(_adUnitId);
                    isAdLoaded = false;
                }
            );
        }

        IEnumerator RequestWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            RequestRewardedAd(_adUnitId);
        }

        private Action _rewardGrantedAction;
        private Action _failedToShowRewardAdAction;
        public void ShowRewardedAd(Action successAction, Action failedAction)
        {
            _rewardGrantedAction = successAction;
            _failedToShowRewardAdAction = failedAction;
            
            TapsellShowOptions showOptions = new TapsellShowOptions
            {
                backDisabled = false,
                immersiveMode = false,
                rotationMode = TapsellShowOptions.ROTATION_UNLOCKED,
                showDialog = true
            };
            Tapsell.ShowAd(_ad, showOptions);
        }
    }
}
