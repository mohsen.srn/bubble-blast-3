﻿using System.Collections;
using TapsellSDK;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TapsellBannerManager : MonoBehaviour
{
    private string _bannerId;
    public void RequestBanner(string bannerId)
    {
        _bannerId = bannerId;
        Tapsell.RequestBannerAd(bannerId, 
            BannerType.BANNER_320x50, Gravity.BOTTOM, 
            Gravity.CENTER, 
            (string zoneId)=>{
                Debug.Log("Action: onBannerRequestFilledAction");
            },
            (string zoneId)=>{
                Debug.Log("Action: onNoBannerAdAvailableAction");
                StartCoroutine(RequestAgain(60f));
            },
            (TapsellError tapsellError)=>{
                Debug.Log("Action: onBannerAdErrorAction");
                StartCoroutine(RequestAgain(10f));
            },
            (string zoneId)=>{
                Debug.Log("Action: onNoNetworkAction");
                StartCoroutine(RequestAgain(60f));
            }, 
            (string zoneId)=>{
                Debug.Log("Action: onHideBannerAction");
            });
    }

    public void ShowBanner()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0)
        {
            Tapsell.ShowBannerAd(_bannerId); //To make visible
        }
    }

    public void HideBanner()
    {
        try
        {
            Tapsell.HideBannerAd(_bannerId); //To hide banner
        }
        catch
        {
            // ignored
        }
    }

    IEnumerator RequestAgain(float delay)
    {
        yield return new WaitForSeconds(delay);
        RequestBanner(_bannerId);
    }
}
