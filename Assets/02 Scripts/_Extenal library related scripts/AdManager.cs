﻿using System;
using UnityEngine;

public abstract class AdManager : MonoBehaviour
{
    public static AdManager GetInstance;
    private void Awake()
    {
        if (GetInstance == null)
        {
            GetInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    public string appId;
    public string bannerId;
    public string interstitialId;
    public string videoAwardId;
    public string nativeId;

    private string _homeSceneName = "01 Home";
    private string _arcadeSceneName = "03 Arcade";
    private string _levelsSceneName = "01 Stage Levels";
    private string _stageSceneName = "02 Stage Board";

    public abstract void InitAd();
    public abstract void ShowBannerAd();
    public abstract void HideBannerAd();
    public abstract void ShowInterstitialAd();
    public abstract bool IsRewardAdLoaded();
    public abstract void ShowRewardedAd(Action successAction, Action failedAction);
}
