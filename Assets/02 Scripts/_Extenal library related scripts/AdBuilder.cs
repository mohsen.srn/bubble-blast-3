using System;
using System.Collections;
using UnityEngine;

namespace _02_Scripts._Extenal_library_related_scripts
{
    public class AdBuilder : MonoBehaviour
    {
        private AdmobManager _admobManager;
        private TapsellManager _tapsellManager;

        public static AdBuilder GetInstance;

        private void Awake()
        {
            if (GetInstance == null)
            {
                GetInstance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            StartCoroutine(BuildAdManager());
        }

        IEnumerator BuildAdManager()
        {
            yield return new WaitForSeconds(1f);
            switch (GameManager.GetInstance.adType)
            {
                case "admob":
                    _admobManager = gameObject.AddComponent<AdmobManager>();
                    break;
                case "tapsell":
                    _tapsellManager = gameObject.AddComponent<TapsellManager>();
                    break;
                default:
                    Debug.Log(GameManager.GetInstance.adType);
                    Debug.Break();

                    break;
            }
        }

        public void RebuildAdManager()
        {
            switch (GameManager.GetInstance.adType)
            {
                case "admob":
                    Destroy(_tapsellManager);
//                    StartCoroutine(AdmobManagerBuild());
                    _admobManager = gameObject.AddComponent<AdmobManager>();

                    break;
                case "tapsell":
                    Destroy(_admobManager);
//                    StartCoroutine(TapsellManagerBuild());
                    _tapsellManager = gameObject.AddComponent<TapsellManager>();

                    break;
                default:
                    Debug.Log(GameManager.GetInstance.adType);
                    Debug.Break();

                    break;
            }
        }

        IEnumerator AdmobManagerBuild()
        {
            yield return new WaitForSeconds(0.5f);
            _admobManager = gameObject.AddComponent<AdmobManager>();
        }

        IEnumerator TapsellManagerBuild()
        {
            yield return new WaitForSeconds(0.5f);
            _tapsellManager = gameObject.AddComponent<TapsellManager>();
        }
    }
}
